---
layout: default
title: Contribute
permalink: /contribute/
order: 80
---

# Contribute

First off, welcome and thank you for your interest in getting involved in our cause!

Reproducible builds are a set of software development practices rather than being a single project, making our work broad and varied. There is still much to be done, and we require a diverse range of skills. Below, we describe the areas we focus on to give you an understanding of our efforts. We hope you find it interesting and see an opportunity to contribute!

## Contribute to tools

We work on our own set of tools that help us make builds reproducible. 

For example, we have:
- **[diffoscope](https://diffoscope.org/)**, a tool that recursively unpack archives and transform various binary formats into more human-readable forms for comparison. You can try it [live](https://try.diffoscope.org/). 
- **[strip-nondeterminism](https://salsa.debian.org/reproducible-builds/strip-nondeterminism)**, a tool that strips bits of nondeterministic information, such as timestamps and file system order, from files like gzipped files, ZIP archives, and JAR files. It can be used as a post-processing step to make a build reproducible.
- **[theunreproduciblepackage](https://github.com/bmwiedemann/theunreproduciblepackage)**, because sometimes it's more effective to show rather than tell, we maintain a collection of practices that make builds unreproducible.

And *many* more! We keep a list of them in the [tools]({{ "/tools/" | relative_url }}) section. We encourage you to take a look at them. We try to keep a list of issues updated for you to pick up. You can also take a look at [our repositories hosted in Salsa](http://salsa.debian.org/reproducible-builds).

## Contribute to distributions

Reproducible Builds is distro agnostic, which means we care about all the distributions and projects out there.

Various distributions have efforts to become more reproducible. We are involved in several of them and we've written guides on how you can contribute:

* [Arch Linux]({{ "/contribute/archlinux/" | relative_url }} )
* [Debian]({{ "/contribute/debian/" | relative_url }})
* [GNU Guix]({{ "/contribute/guix/" | relative_url }})
* [NixOS]({{ "/contribute/nixos/" | relative_url }})
* [openSUSE]({{ "/contribute/opensuse/" | relative_url }})

Please send patches to this page if your project is not listed here.

## Contribute to testing

There are many projects that are being tested within the reproducible test infrastructure. We keep a list of them in the [Continuous tests]({{ "/citests/" | relative_url }}) section. The primary repository for our testing efforts is available [here](https://salsa.debian.org/qa/jenkins.debian.net). You can learn more about this project in the [README](https://salsa.debian.org/qa/jenkins.debian.net/-/blob/master/README?ref_type=heads).

## Non-coding contributions

If you want to contribute but not in the form of coding for the different projects, we've compiled a list of other ways you can help out. Here are some ideas:

* Spread the word! Many people don't know about reproducible builds, you can help us change that:
  * Host demos: Show others how reproducible builds work in practice.
  * Give talks: Share your knowledge with other. Need inspiration? Check out [this list]({{ "/resources/" | relative_url }}) of our own talks for ideas
  *  Write blog posts
  *  Publish [academic publications]({{ "/docs/publications/" | relative_url }})
  *  ... and any other method you find suitable! And if you do, please let us know so we can feature them in our website
* Follow us on social media at <a href="https://twitter.com/ReproBuilds">@ReproBuilds</a>, Mastodon <a href="https://fosstodon.org/@reproducible_builds">@reproducible_builds@fosstodon.org</a> and <a href="https://reddit.com/r/reproduciblebuilds">Reddit</a> 
* You can promote our [talks]({{ "/resources/" | relative_url }}), [news]({{ "/news/" | relative_url }}) and [events]({{ "/events/" | relative_url }}) through social media
* You can help us with design related tasks
* Join us in our [events]({{ "/events/" | relative_url }})
* Make your own software reproducible, and tell us about it!
* Use our [tools]({{ "/tools/" | relative_url }}) in your projects
* Improve our [documentation](https://salsa.debian.org/reproducible-builds/reproducible-website/-/tree/master/_docs?ref_type=heads)
* Report reproducibility bugs
* Improve our website: we maintain a fairly updated [list of issues in our salsa repository](https://salsa.debian.org/reproducible-builds/reproducible-website/-/issues)

## Donate

Lastly, you can support us through monetary donations or by providing resources. We have an [extensive guide on our donate page]({{ "/donate/" | relative_url }}) on how both companies and individuals can contribute to our cause. Every contribution makes a difference! To see the impact of your support, check out our [news]({{ "/news/" | relative_url }}) section, where we publish a monthly report on our progress.

## Get in touch

Our preferred way of contact is to post to our public mailing list, rb-general. To do so, please join the [`rb-general` mailing-list](https://lists.reproducible-builds.org/listinfo/rb-general). However, there are other ways you can contact us:

* Our [Gitlab group](http://salsa.debian.org/reproducible-builds) is on [Salsa](https://salsa.debian.org/): submitting issues and opening merge requests is welcome! ([**Full signup instructions**]({{ "/contribute/salsa/" | relative_url }}))

* Subscribe to the [reproducible-builds@lists.alioth.debian.org mailing list](https://lists.alioth.debian.org/mailman/listinfo/reproducible-builds) and/or other [reproducible builds](https://lists.reproducible-builds.org/) oriented lists.

* Join the [#reproducible-builds IRC channel on irc.oftc.net](https://webchat.oftc.net/?channels=#reproducible-builds) and possibly [#reproducible-changes](https://webchat.oftc.net/?channels=#reproducible-changes) too. For Debian there is also [#debian-reproducible](https://webchat.oftc.net/?channels=#debian-reproducible) and [#debian-reproducible-changes](https://webchat.oftc.net/?channels=#debian-reproducible-changes), and for Arch Linux there is [#archlinux-reproducible on irc.libera.chat](https://web.libera.chat/?channels=#archlinux-reproducible)

* You can subscribe to [commit notifications](https://lists.reproducible-builds.org/listinfo/rb-commits) to keep track of contributions.

* If none of the above options work for you and you wish to contact the core team directly, please email contact@reproducible-builds.org.

You can learn more about our contributors and the core team at [involved People]({{ "/who/people/" | relative_url }}).

All that's left is to thank you for your time and interest. We hope you'll join us on our quest to achieve Reproducible Builds!
