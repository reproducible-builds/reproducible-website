---
layout: default
title: Contribute to reproducible builds in GNU Guix
permalink: /contribute/guix/
---

## Task suggestions for GNU Guix

This page explains how you can contribute to reproducible builds in
[GNU Guix](https://guix.gnu.org).

### Check unreproducible packages

You can monitor reproducibility issues by running [`guix
challenge`](https://guix.gnu.org/manual/en/html_node/Invoking-guix-challenge.html).
To further investigate, you can specify one or more packages on the
command line, and ask it to invoke Diffoscope upon failure:

```
guix challenge git \
  --diff=diffoscope \
  --substitute-urls="https://ci.guix.gnu.org https://guix.example.org"
```

Another option is to rebuild packages locally.  For example, assuming
you already installed Git on your machine, you can force a rebuild [with
`--check`](https://guix.gnu.org/manual/en/html_node/Additional-Build-Options.html#build_002dcheck):

```
guix build git --check --no-grafts --keep-failed
```

If the new build result differs from the one that was already in store,
the failed build is kept as `/gnu/store/…-git-1.2.3-check`, allowing you
to compare it with the original one (without the `-check` suffix).

### Reporting issues

Please report issues by sending email to `bug-guix@gnu.org`.  Each
message opens a new issues visible in the [bug
tracker](https://issues.guix.gnu.org).

When investigating, be sure to check [Debian’s reproducibility issue
inventory](https://tests.reproducible-builds.org/debian/index_issues.html)
for known problems and solutions.

### Join the discussion
You can also discuss these issues with other developers on [the Guix
development mailing list and on the IRC
channel](https://guix.gnu.org/en/contact).
