---
layout: default
title: Contribute to Arch Linux
permalink: /contribute/archlinux/
---

## Task suggestions for Arch Linux

### Join the discussion
   Join [#archlinux-reproducible on <em>irc.libera.chat</em>](https://web.libera.chat/?channels=#archlinux-reproducible).

### Check unreproducible packages
   Check the unreproducible packages on your system with `arch-repro-status -f BAD`.
   Additionally adding `-i` to that command allows you to interactively browse the build
   logs and diffoscope output per package. You can find additional options in [the repository](https://gitlab.archlinux.org/archlinux/arch-repro-status).

### For package maintainers
   If you maintain packages in Arch Linux the [Developer Dashboard](https://archlinux.org/devel/)
   has a report with your unreproducible packages.

### Reporting issues
   Report packaging issues which cause unreproducible packages on the [Arch Linux Gitlab's issues](https://gitlab.archlinux.org/groups/archlinux/packaging/packages/-/issues).

## External links

* [Arch Linux reproducible builds status](https://reproducible.archlinux.org/)
* [arch-repro-status](https://gitlab.archlinux.org/archlinux/arch-repro-status)
