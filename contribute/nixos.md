---
layout: default
title: Contribute to NixOS
permalink: /contribute/nixos/
---

## Task suggestions for NixOS

### Join the discussion

For real-time conversations, join the [`#reproducible-builds:nixos.org`](https://matrix.to/#/#reproducible-builds:nixos.org) channel on Matrix.

### Project Status

- **Website**: Check [reproducible.nixos.org](https://reproducible.nixos.org) for general information and status updates.
- **Project Board**: To keep track of ongoing tasks and open issues concerning
  reproducible builds, view our [GitHub Project Board](https://github.com/orgs/NixOS/projects/30).
- **Pull Requests & Issues**: Browse current [pull requests](https://github.com/NixOS/nixpkgs/pulls?q=is%3Aopen+is%3Apr+label%3A%226.topic%3A+reproducible+builds%22) and [issues](https://github.com/NixOS/nixpkgs/issues?q=is%3Aopen+is%3Aissue+label%3A%226.topic%3A+reproducible+builds%22) labeled with "reproducible builds."

### Reporting Issues

Use the [issue template](https://github.com/NixOS/nixpkgs/issues/new?template=10_unreproducible_package.yml) on GitHub to report your issues and hopefully, your solution.

### Additional Projects

- [lila](https://github.com/JulienMalka/lila)
- [trustix](https://github.com/nix-community/trustix)
