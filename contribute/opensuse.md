---
layout: default
title: Contribute to openSUSE
permalink: /contribute/opensuse/
---

## Task suggestions for openSUSE

### Join the discussion

For real-time conversations, join the
[`#reproducible-builds:opensuse.org`](https://matrix.to/#/#reproducible-builds:opensuse.org)
channel on Matrix.

### Project Status

See the tracking bug linked in the next section.

- [The openSUSE r-b CI graph has package
counts.](http://rb.zq1.de/compare.factory/graph.png)
- [Status reports of reproducible builds in openSUSE can be found on the openSUSE
Factory
mailinglist.](https://lists.opensuse.org/archives/search?q=reproducible-builds&page=1&mlist=factory%40lists.opensuse.org&sort=date-desc)

### Reporting issues

Report your issue on [the openSUSE
Bugzilla](https://bugzilla.opensuse.org/enter_bug.cgi?classification=openSUSE)
and ensure the Blocks field has a bug in the dependency tree of the [openSUSE
reproducible builds tracking
issue](https://bugzilla.opensuse.org/show_bug.cgi?id=1081754).

### Sending changes

The [overview of the openSUSE workflow of submitting changes links to detailed
documentation on how to submit
changes](https://en.opensuse.org/openSUSE:Fixing_bugs).

[The openSUSE specific r-b page has documentation on how to rebuild packages
reproducibly.](https://en.opensuse.org/openSUSE:Reproducible_Builds)

Patches can also be attached to Bugzilla issues.

