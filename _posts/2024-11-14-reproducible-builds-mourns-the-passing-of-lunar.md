---
layout: post
title: "Reproducible Builds mourns the passing of Lunar"
date: 2024-11-14 15:00:00
categories: org
---

The Reproducible Builds community sadly announces it has lost its founding member.
{: .lead}

Jérémy Bobbio *aka* 'Lunar' passed away on Friday November 8th in palliative care in Rennes, France.

Lunar was instrumental in starting the Reproducible Builds project in 2013 as a loose initiative within the [Debian](https://debian.org/) project. Many of [our earliest status reports](https://lists.debian.org/debian-devel-announce/2015/02/msg00007.html) were written by him and many of our [key tools in use today](https://diffoscope.org/) are based on his design.

Lunar was a resolute opponent of surveillance and censorship, and he possessed an unwavering energy that fueled his work on Reproducible Builds and [Tor](https://torproject.org). Without Lunar's far-sightedness, drive and commitment to enabling teams around him, Reproducible Builds and free software security would not be in the position it is in today. His contributions will not be forgotten, and his high standards and drive will continue to serve as an inspiration to us as well as for the other [high-impact projects he was involved in](https://linuxfr.org/news/deces-de-lunar-un-hacktiviste-pedagogue).

Lunar's creativity, insight and kindness were often noted. He will be greatly missed.

<br>

[![]({{ "/images/news/2024-11-14-reproducible-builds-mourns-passing-lunar/1.jpg" | relative_url }})](https://lunar.anargeek.net/)

Other tributes:
{: .small}

* [**Anargeek.net**](https://lunar.anargeek.net/) [FR]
* [LWN](https://lwn.net/Articles/997775/)
* [Debian](https://www.debian.org/News/2024/20241119)
* [Stefano Zacchiroli](https://upsilon.cc/~zack/blog/posts/2024/11/In_memory_of_Lunar/)
* [Linuxfr.org](https://linuxfr.org/news/deces-de-lunar-un-hacktiviste-pedagogue) [FR]
* [Software Heritage](https://www.softwareheritage.org/2024/11/15/remembering-lunar/)
* [A history of the Reproducible Builds project](https://reproducible-builds.org/docs/history/)
{: .small}
