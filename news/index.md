---
layout: default
title: News
permalink: /news/
order: 20
redirect_from:
 - /reports/
 - /blog/
---

# News

<ul class="list-unstyled">
    {% assign posts = site.posts | sort: 'year, month' | where: 'draft', 'false' %}
    {% for post in posts limit: 4 %}
    <li>
        <a href="{{ post.url | relative_url }}">{{ post.title }}</a>
        <small class="text-muted">{{ post.date | date: "%b %-d, %Y" }}</small>
    </li>
    {% endfor %}
</ul>

You can see all archived posts in [our news archive]({{ "/news/archive/" | relative_url }}).

## Reports

Every month we publish a report on what we have been up to.

<ul class="list-unstyled">
    {% assign reports = site.reports | sort: 'year, month' | where: 'draft', 'false' | reverse %}
    {% for x in reports %}
    <li>
        <a href="{{ x.url | relative_url }}">{{ x.title }}</a>
    </li>
    {% endfor %}
</ul>

(An [RSS/Atom feed]({{ "/blog/index.rss" | relative_url }}) of these reports is available.)

## Weekly reports

Previously, from May 2015 until April 2019, we published 206 weekly reports on what we had been up to:

{% assign posts = site.blog | sort: 'week' | reverse %}
{% for x in posts %}<a href="{{ x.url | relative_url }}" class="btn btn-outline-primary btn-sm mr-1 mb-1">{{ x.week }}</a>{% endfor %}
