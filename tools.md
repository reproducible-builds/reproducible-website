---
layout: default
title: Tools
permalink: /tools/
order: 30
---

# Tools

<p class="lead">Several tools are available to make your life easier when working on reproducible builds.</p>

- [Tools to compare build artifacts and/or detect nondeterminism](#tools-to-compare-build-artifacts-andor-detect-nondeterminism)
- [Tools to make builds reproducible (by removing nondeterminism)](#tools-to-make-builds-reproducible-by-removing-nondeterminism)
- [Tools for rebuilding](#tools-for-rebuilding)
  - [orchestrators](#orchestrators)
  - [rebuilder-backends](#rebuilder-backends)
  - [clients/frontends](#clientsfrontends)
- [verifiers?](#verifiers)
- [Misc](#misc)

## Tools to compare build artifacts and/or detect nondeterminism

#### diffoscope

[diffoscope](https://diffoscope.org/) will try to get to the bottom of what
makes files or directories different. It will recursively unpack archives of
many kinds and transform various binary formats into more human-readable forms
for comparison. It can compare two tarballs, ISO images, or PDFs just as
easily. See an example
[HTML](https://diffoscope.org/examples/https-everywhere-5.0.6_vs_5.0.7.html)
and [text](https://diffoscope.org/examples/igerman98_20131206-5.txt) output.

<a href="https://diffoscope.org/" class="btn btn-outline-primary btn-sm">Homepage</a>
<a href="https://salsa.debian.org/reproducible-builds/diffoscope" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://salsa.debian.org/reproducible-builds/diffoscope/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://salsa.debian.org/reproducible-builds/diffoscope/merge_requests" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://packages.debian.org/sid/diffoscope" class="btn btn-outline-primary btn-sm">Debian package</a>
<a href="https://pypi.python.org/pypi/diffoscope" class="btn btn-outline-primary btn-sm">PyPI</a>
<a href="https://archlinux.org/packages/extra/x86_64/diffoscope" class="btn btn-outline-primary btn-sm">Arch Linux package</a>

#### trydiffoscope

If you wish to just experiment with [diffoscope](https://diffoscope.org/)
without installing a large number of run-time dependencies, you can use the
[try.diffoscope.org](https://try.diffoscope.org/) service directly from your
web browser. A lightweight client is also available as the `trydiffoscope`
command-line utility.

<a href="https://try.diffoscope.org/" class="btn btn-outline-primary btn-sm">Homepage</a>
<a href="https://salsa.debian.org/reproducible-builds/trydiffoscope" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://salsa.debian.org/reproducible-builds/trydiffoscope/merge_requests" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://packages.debian.org/sid/trydiffoscope" class="btn btn-outline-primary btn-sm">Debian package</a>
<a href="https://pypi.python.org/pypi/trydiffoscope" class="btn btn-outline-primary btn-sm">PyPI</a>

#### reprotest

`reprotest` builds the same source code in different environments and then checks
the binaries produced by the builds to see if changing the environment, without
changing the source code, changed the generated binaries.

<a href="https://salsa.debian.org/reproducible-builds/reprotest" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://salsa.debian.org/reproducible-builds/reprotest/merge_requests" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://packages.debian.org/sid/reprotest" class="btn btn-outline-primary btn-sm">Debian package</a>
<a href="https://archlinux.org/packages/extra/any/reprotest" class="btn btn-outline-primary btn-sm">Arch Linux package</a>

#### disorderfs

Problems with [unstable order of inputs](/docs/stable-inputs/) or other
variations introduced by filesystems can sometimes be hard to track down.
`disorderfs` is an overlay FUSE filesystem that deliberately introduces
non-determinism into filesystem metadata. For example, it can randomize the
order in which directory entries are read.

<a href="https://salsa.debian.org/reproducible-builds/disorderfs" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://salsa.debian.org/reproducible-builds/disorderfs/merge_requests" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://packages.debian.org/sid/disorderfs" class="btn btn-outline-primary btn-sm">Debian package</a>
<a href="https://archlinux.org/packages/extra/x86_64/disorderfs" class="btn btn-outline-primary btn-sm">Arch Linux package</a>

#### The Unreproducible Package

`The Unreproducible Package` is meant as a practical way to demonstrate the
various ways that software can break *reproducible builds* using just low level
primitives without requiring external existing programs that implement these
primitives themselves.

It is structured so that one subdirectory demonstrates one class of
issues in some variants observed in the wild.

<a href="https://salsa.debian.org/reproducible-builds/theunreproduciblepackage" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://salsa.debian.org/reproducible-builds/theunreproduciblepackage/-/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://salsa.debian.org/reproducible-builds/theunreproduciblepackage/-/merge_requests" class="btn btn-outline-primary btn-sm">Merge Requests</a>

#### diffoci

`diffoci` compares Docker and OCI container images for helping reproducible builds.

<a href="https://github.com/reproducible-containers/diffoc" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/reproducible-containers/diffoci/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/reproducible-containers/diffoci/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>

#### reproducible-apk-tools

`reproducible-apk-tools` is a collection of scripts to help make
APKs reproducible, or find out why they are not.

<a href="https://github.com/obfusk/reproducible-apk-tools" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/obfusk/reproducible-apk-tools/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/obfusk/reproducible-apk-tools/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://pypi.python.org/pypi/repro-apk" class="btn btn-outline-primary btn-sm">PyPI</a>

#### ismypackagereproducibleyet

`ismypackagereproducibleyet` is a service for developers, packagers and users
to quickly find out if a package builds reproducibly in some Linux distribution.

<a href="https://salsa.debian.org/reproducible-builds/ismypackagereproducibleyet" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://salsa.debian.org/reproducible-builds/ismypackagereproducibleyet/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://salsa.debian.org/reproducible-builds/ismypackagereproducibleyet/merge_requests" class="btn btn-outline-primary btn-sm">Merge requests</a>

## Tools to make builds reproducible (by removing nondeterminism)

#### strip-nondeterminism

Some tools used in build systems might introduce non-determinism in ways
difficult to fix at the source, which requires post-processing.
`strip-nondeterminism` knows how to normalise various file formats such as
gzipped files, ZIP archives, and Jar files. It is written in Perl with
extensibility in mind.

<a href="https://salsa.debian.org/reproducible-builds/strip-nondeterminism" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://salsa.debian.org/reproducible-builds/strip-nondeterminism/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://salsa.debian.org/reproducible-builds/strip-nondeterminism/merge_requests" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://packages.debian.org/sid/strip-nondeterminism" class="btn btn-outline-primary btn-sm">Debian package</a>
<a href="https://archlinux.org/packages/extra/any/strip-nondeterminism" class="btn btn-outline-primary btn-sm">Arch Linux package</a>

#### disorderfs

Problems with [unstable order of inputs](/docs/stable-inputs/) or other
variations introduced by filesystems can sometimes be hard to track down.
`disorderfs` is an overlay FUSE filesystem that deliberately introduces
non-determinism into filesystem metadata. For example, it can randomize the
order in which directory entries are read.

<a href="https://salsa.debian.org/reproducible-builds/disorderfs" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://salsa.debian.org/reproducible-builds/disorderfs/merge_requests" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://packages.debian.org/sid/disorderfs" class="btn btn-outline-primary btn-sm">Debian package</a>
<a href="https://archlinux.org/packages/extra/x86_64/disorderfs" class="btn btn-outline-primary btn-sm">Arch Linux package</a>

#### Reproducible Build Maven Plugin

This Maven plugin tries to strip "non reproducible" data from the generated artifacts.
It follows the same goals as Debian's Reproducible Builds project but at the modest
scale of a Maven project.

<a href="https://zlika.github.io/reproducible-build-maven-plugin" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/Zlika/reproducible-build-maven-plugin/issues" class="btn btn-outline-primary btn-sm">Issues</a>

#### Reproducible Builds Gradle Plugin

Reproducibility settings applied to some of [Gradle's](https://gradle.org) built-in tasks, that should really be the default.

<a href="https://github.com/gradlex-org/reproducible-builds" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/gradlex-org/reproducible-builds/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/gradlex-org/reproducible-builds/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>

#### sbt-reproducible-builds

`sbt-reproducible-builds` is a [sbt](https://www.scala-sbt.org/) plugin
to make sbt builds more reproducible. Popular with
[Scala](https://www.scala-lang.org/) projects.

<a href="https://github.com/raboof/sbt-reproducible-builds" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/raboof/sbt-reproducible-builds/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/raboof/sbt-reproducible-builds/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>

#### reproducible-apk-tools

`reproducible-apk-tools` is a collection of scripts to help make
APKs reproducible, or find out why they are not.

<a href="https://github.com/obfusk/reproducible-apk-tools" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/obfusk/reproducible-apk-tools/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/obfusk/reproducible-apk-tools/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://pypi.python.org/pypi/repro-apk" class="btn btn-outline-primary btn-sm">PyPI</a>

#### apksigcopier

`apksigcopier` is a tool that enables using an android APK signature
as a build input, making it possible to create a reproducible build
from the source code without having access to the private key used
to create the signature. It can also be used to verify that two APKs
with different signatures are otherwise identical.

<a href="https://github.com/obfusk/apksigcopier" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/obfusk/apksigcopier/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/obfusk/apksigcopier/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://packages.debian.org/sid/apksigcopier" class="btn btn-outline-primary btn-sm">Debian package</a>
<a href="https://pypi.python.org/pypi/apksigcopier" class="btn btn-outline-primary btn-sm">PyPI</a>
<a href="https://archlinux.org/packages/extra/any/apksigcopier" class="btn btn-outline-primary btn-sm">Arch Linux package</a>


## Tools for rebuilding

### orchestrators

#### rebuilderd

Independent verification system of binary packages. `rebuilderd` monitors the
package repository of a linux distribution and uses rebuilder backends like
archlinux-repro to verify the provided binary packages can be reproduced from
the given source code.

<a href="https://github.com/kpcyrd/rebuilderd" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/kpcyrd/rebuilderd/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/kpcyrd/rebuilderd/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://wiki.archlinux.org/index.php/Rebuilderd" class="btn btn-outline-primary btn-sm">Arch Linux Documentation</a>
<a href="https://archlinux.org/packages/extra/x86_64/rebuilderd/" class="btn btn-outline-primary btn-sm">Arch Linux package</a>

#### PackageRebuilder

Standalone orchestrator for rebuilding Debian, Fedora and Qubes OS packages in
order to generate `in-toto` metadata which can be used with `apt-transport-in-toto`
or `dnf-plugin-in-toto` to validate reproducible status.

<a href="https://github.com/fepitre/package-rebuilder" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/fepitre/package-rebuilder/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/fepitre/package-rebuilder/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://archlinux.org/packages/extra/x86_64/rebuilderd/" class="btn btn-outline-primary btn-sm">Arch Linux package</a>

#### Reproducible Builds Transparency Log (rbtlog)

`rbtlog` is a Reproducible Builds transparency log for Android APKs. Its git repository contains:

- Scripts forming a rebuilder framework.
- Recipes to build various apps.
- Rebuild Logs forming a transparency log of reproduction attempts.
- GitHub Actions workflows to automate everything.

<a href="https://github.com/obfusk/rbtlog" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/obfusk/rbtlog/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/obfusk/rbtlog/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://codeberg.org/IzzyOnDroid/rbtlog" class="btn btn-outline-primary btn-sm">IzzyOnDroid rebuilder</a>

### rebuilder-backends

#### Arch Linux repro

`repro` is intended to be a tool for users to verify packages distributed by Arch
Linux. It uses the embedded `.BUILDINFO` file to reconstruct an identical build
environment and repeats the build from source, then compares the input package
with the package generated during the verification build.

<a href="https://github.com/archlinux/archlinux-repro" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/archlinux/archlinux-repro/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/archlinux/archlinux-repro/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://archlinux.org/packages/extra/any/archlinux-repro/" class="btn btn-outline-primary btn-sm">Arch Linux package</a>

#### Arch Linux makerepropkg

`makerepropkg` - rebuild a package to see if it is reproducible.

<a href="https://github.com/archlinux/devtools/blob/master/src/makerepropkg.in" class="btn btn-outline-primary btn-sm">Script</a>

#### Debian debrebuild.pl

Given a buildinfo file from a Debian package, generate instructions for
attempting to reproduce the binary packages built from the associated source
and build information.

<a href="https://salsa.debian.org/debian/devscripts/-/blob/master/scripts/debrebuild.pl" class="btn btn-outline-primary btn-sm">Script</a>

#### debrebuild

Given a buildinfo file from a Debian package, generate instructions for attempting
to reproduce the binary packages built from the associated source and build
information. `debrebuild` can parse buildinfo file having GPG signature and verify
its signature with provided key file.

Highly inspired from original Debian tool [debrebuild](https://salsa.debian.org/debian/devscripts)
and newer features from <https://salsa.debian.org/josch/devscripts/-/commits/debrebuild>
and <https://salsa.debian.org/fepitre/devscripts/-/commits/debrebuild>.

<a href="https://github.com/fepitre/debrebuild" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/fepitre/debrebuild/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/fepitre/debrebuild/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>

#### rpmreproduce

Given a buildinfo file from a RPM package, generate instructions for attempting
to reproduce the binary packages built from the associated source and build
information.

<a href="https://github.com/fepitre/rpmreproduce" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/fepitre/rpmreproduce/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/fepitre/rpmreproduce/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>

#### orb: check opam package reproducibility

This tool can check if an opam package build is reproducible.
It has two subcommands: build and rebuild.

`orb build` conducts a build of an opam package, and collects
the build result (and hashes thereof) and the build input.

`orb rebuild` takes this data as input and conducts a second build with
the same environment, and compares that the hashes of the produced
binaries are identical.

<a href="https://builds.robur.coop/" class="btn btn-outline-primary btn-sm">Homepage</a>
<a href="https://github.com/robur-coop/orb" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/robur-coop/orb/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/robur-coop/orb/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>


### clients/frontends

#### Rebuilderd Website

The [Rebuilderd Website](https://reproducible.archlinux.org) is simple status
display with the number of reproducible packages for Arch Linux. Uses
rebuilderd's API to fetch the current status of reproducibility.

<a href="https://reproducible.archlinux.org" class="btn btn-outline-primary btn-sm">Homepage</a>
<a href="https://gitlab.archlinux.org/archlinux/rebuilderd-website" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://gitlab.archlinux.org/archlinux/rebuilderd-website/-/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://gitlab.archlinux.org/archlinux/rebuilderd-website/-/merge_requests" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://archlinux.org/packages/extra/any/rebuilderd-website/" class="btn btn-outline-primary btn-sm">Arch Linux package</a>

#### ismyarchverifiedyet

Experimental python script to query results from rebuilderd instances and
compares it to your local arch system.

<a href="https://github.com/kpcyrd/ismyarchverifiedyet" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/kpcyrd/ismyarchverifiedyet/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/kpcyrd/ismyarchverifiedyet/pulls" class="btn btn-outline-primary btn-sm">Merge Requests</a>

#### Arch Repro Status

A CLI tool for querying the reproducibility status of the Arch Linux packages
using data from a rebuilderd instance such as reproducible.archlinux.org. It can
show the reproducibility status of packages that belong to an individual package
maintainer and currently installed packages on the system.

<a href="https://gitlab.archlinux.org/archlinux/arch-repro-status" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://gitlab.archlinux.org/archlinux/arch-repro-status/-/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://gitlab.archlinux.org/archlinux/arch-repro-status/-/merge_requests" class="btn btn-outline-primary btn-sm">Merge requests</a>
<a href="https://archlinux.org/packages/extra/x86_64/arch-repro-status/" class="btn btn-outline-primary btn-sm">Arch Linux package</a>

#### Builder-web - a web frontend for reproducible builds (robur)

`Builder-web` takes in submissions of builds, typically from builder,
and displays the produced artifacts in a way that makes it easy to compare
checksums and build status. Produced binaries can be downloaded and executed.
[builds.robur.coop](https://builds.robur.coop/) itself runs `builder-web`.

<a href="https://builds.robur.coop/" class="btn btn-outline-primary btn-sm">Homepage</a>
<a href="https://github.com/robur-coop/builder-web" class="btn btn-outline-primary btn-sm">Git</a>
<a href="https://github.com/robur-coop/builder-web/issues" class="btn btn-outline-primary btn-sm">Issues</a>
<a href="https://github.com/robur-coop/builder-web/pulls" class="btn btn-outline-primary btn-sm">Merge requests</a>


## verifiers?
 (I wonder how we can contrast a more thorough attestation verification vs regular client/frontends)
- <https://github.com/tweag/trustix>
- <https://github.com/in-toto/apt-transport-in-toto>
- <https://github.com/fepitre/dnf-plugin-in-toto>


## Misc

* [Style guide for reproducible-builds.org](https://reproducible-builds.org/style/) - this guide aims to provide standards, principles and components to design consistent views related to the Reproducible Builds project.

[Signed tarballs are available](https://reproducible-builds.org/_lfs/releases/) for our tools.
