# Source for the reproducible-builds.org website

The website for reproducible-builds.org is made with
[Jekyll](https://jekyllrb.com/), a static site generator. Markdown files
are automatically published on the
[Reproducible Builds](https://reproducible-builds.org/) website.


## Prerequisites

Before running the website, make sure you have
[installed and set up jekyll with all it's requirements](https://jekyllrb.com/docs/).

Install jekyll plugins
```sh
gem install jekyll-redirect-from jekyll-polyglot jekyll-sitemap
```

## Local development

Start the local development server with `jekyll serve`.

Pass the `--livereload` option to serve to automatically refresh the 
with each change you make to the source files:

```sh
jekyll serve --livereload
```

Open your web browser and visit <http://localhost:4000> to view the website.


## Build the website

Building the website is made by running:

```sh
jekyll build
```

The result will be available in the `_site` directory.


## Contribute

You want to help, great!

We have a guideline for you to read:
[our guide for contributors](https://reproducible-builds.org/contribute).

```console
sudo apt install jekyll git po4a python3-yaml ruby-jekyll-redirect-from ruby-jekyll-polyglot ruby-jekyll-sitemap
```

Basically, it is:

1. Fork the repository
2. Create your feature branch
3. Commit your changes
4. Push to your branch
5. Create a new Merge Request


## Nix

The website can also be built using [Nix](https://nixos.org). The exposed URL will be `http://127.0.0.1:4000`.

When the local website sources are available locally, the following commands are available:

- `nix develop`: Start a shell with all the required dependencies available (jekyll, pandoc, gnumake, which, po4a, python)
- `nix build .#reproducible-website`: Build the website completely. The result will be available in the `result` directory.
- `nix run .#default`: Start a webserver with Caddy to serve the website. This is not meant to be used for development.
- `nix run .#jekyll-serve`: Start a webserver with Jekyll to serve the website. This is meant to be used for development.
- `nix run .#jekyll-serve-livereload`: Start a webserver with Jekyll to serve the website with livereload. This is meant to be used for development.

In case the sources are not available locally, the following commands are available:

- `nix build git+https://salsa.debian.org/reproducible-builds/reproducible-website.git#reproducible-website`: Build a development snapshot of the website, available in the `result` directory.

- `nix run git+https://salsa.debian.org/reproducible-builds/reproducible-website.git#default`: Start a webserver with Caddy to serve the website. This is not meant to be used for development.
