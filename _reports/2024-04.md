---
layout: report
year: "2024"
month: "04"
title: "Reproducible Builds in April 2024"
draft: false
date: 2024-05-10 10:05:26
---

[![]({{ "/images/reports/2024-04/reproducible-builds.png#right" | relative_url }})]({{ "/" | relative_url }})

**Welcome to the April 2024 report from the [Reproducible Builds](https://reproducible-builds.org) project!** In our reports, we attempt to outline what we have been up to over the past month, as well as mentioning some of the important things happening more generally in software supply-chain security. As ever, if you are interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

**Table of contents:**

0. [New `backseat-signed` tool to validate distributions’ source inputs](#new-backseat-signed-tool-to-validate-distributions-source-inputs)
0. [‘NixOS is not reproducible’](#nixos-is-not-reproducible)
0. [Certificate vulnerabilities in F-Droid’s `fdroidserver`](#certificate-vulnerabilities-in-f-droids-fdroidserver)
0. [Website updates](#website-updates)
0. [‘Reproducible Builds and Insights from an Independent Verifier for Arch Linux’](#reproducible-builds-and-insights-from-an-independent-verifier-for-arch-linux)
0. [`libntlm` now releasing ‘minimal source-only tarballs’](#libntlm-now-releasing-minimal-source-only-tarballs)
0. [Distribution work](#distribution-work)
0. [Mailing list news](#mailing-list-news)
0. [diffoscope](#diffoscope)
0. [Upstream patches](#upstream-patches)
0. [reprotest](#reprotest)
0. [Reproducibility testing framework](#reproducibility-testing-framework)

---

### New `backseat-signed` tool to validate distributions' source inputs

*kpcyrd* announced a new tool called [`backseat-signed`](https://github.com/kpcyrd/backseat-signed), after:

> I figured out a somewhat straight-forward way to check if a given `git archive` output is cryptographically claimed to be the source input of a given binary package in either Arch Linux or Debian (or both).

Elaborating more [in their announcement post](https://lists.reproducible-builds.org/pipermail/rb-general/2024-April/003337.html), *kpcyrd* writes:

> I believe this to be the "reproducible source tarball" thing some people have been asking about. As explained in the README, I believe reproducing autotools-generated tarballs isn't worth everybody's time and instead a distribution that claims to build from source should operate on VCS snapshots instead of tarballs with 25k lines of pre-generated shell-script.

Indeed, many distributions' packages already build from VCS snapshots, and this trend is likely to accelerate in response to the xz incident. The announcement led to a [lengthy discussion on our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2024-April/thread.html#3337), as well as shorter [followup thread from *kpcyrd*](https://lists.reproducible-builds.org/pipermail/rb-general/2024-April/003376.html) about bootstrapping [Autotools](https://en.wikipedia.org/wiki/GNU_Autotools) projects.

<br>

### 'NixOS is not reproducible'

[![]({{ "/images/reports/2024-04/nixos.png#right" | relative_url }})](https://linderud.dev/blog/nixos-is-not-reproducible/)

[Morten Linderud](https://linderud.dev/) posted an post on his blog this month, provocatively titled, "[NixOS is not reproducible](https://linderud.dev/blog/nixos-is-not-reproducible/)". Although quickly admitting that his title is indeed "clickbait", Morten goes on to clarify the precise guarantees and promises that [NixOS](https://nixos.org/) provides its users.

Later in the most, Morten mentions that he was motivated to write the post because:

> I have heavily invested my free-time on this topic since 2017, and met some of the accomplishments we have had with “Doesn’t NixOS solve this?” for just as long… and I thought it would be of peoples interest to clarify[.]

<br>

### Certificate vulnerabilities in F-Droid's `fdroidserver`

In early April, Fay Stegerman [announced a certificate pinning bypass vulnerability and Proof of Concept (PoC)](https://www.openwall.com/lists/oss-security/2024/04/08/8) in the F-Droid `fdroidserver` tools for "managing builds, indexes, updates, and deployments for F-Droid repositories" to the [`oss-security`](https://www.openwall.com/lists/oss-security/) mailing list.

> We observed that embedding a v1 (JAR) signature file in an APK with `minSdk` >= 24 will be ignored by Android/apksigner, which only checks v2/v3 in that case. However, since `fdroidserver` checks v1 first, regardless of `minSdk`, and does not verify the signature, it will accept a "fake" certificate and see an incorrect certificate fingerprint. […] We also realised that the above mentioned discrepancy between `apksigner` and `androguard` (which `fdroidserver` uses to extract the v2/v3 certificates) can be abused here as well. […]

Later on in the month, Fay followed up with a second post detailing a third vulnerability and a script that [could be used to scan for potentially affected `.apk` files](https://www.openwall.com/lists/oss-security/2024/04/20/3) and mentioned that, whilst upstream had acknowledged the vulnerability, they had not yet applied any ameliorating fixes.

<br>

### Website updates

[![]({{ "/images/reports/2024-04/website.png#right" | relative_url }})]({{ "/" | relative_url }})

There were a number of improvements made to our website this month, including Chris Lamb updating the [archive page]({{ "/docs/archive/" | relative_url }}) to recommend `-X` and unzipping with `TZ=UTC`&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/d15f76b8)] and adding Maven, Gradle, JDK and Groovy examples to the [`SOURCE_DATE_EPOCH` page]({{ "/docs/source-date-epoch/" | relative_url }})&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/bfcbb9a2)]. In addition Jan Zerebecki added a new [`/contribute/opensuse/`]({{ "/contribute/opensuse/" | relative_url }}) page&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/4901c9ae)] and *Sertonix* fixed the automatic RSS feed detection&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/5f311583)][[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/54c80767)].

<br>

### [*Reproducible Builds and Insights from an Independent Verifier for Arch Linux*](https://doi.org/10.18420/sicherheit2024_016)

[![]({{ "/images/reports/2024-04/sicherheit2024_016.png#right" | relative_url }})](https://doi.org/10.18420/sicherheit2024_016)

Joshua Drexel, Esther Hänggi and Iyán Méndez Veiga of the School of Computer Science and Information Technology, Hochschule Luzern (HSLU) in Switzerland published a paper this month entitled [*Reproducible Builds and Insights from an Independent Verifier for Arch Linux*](https://doi.org/10.18420/sicherheit2024_016). The paper establishes the context as follows:

> Supply chain attacks have emerged as a prominent cybersecurity threat in recent years. Reproducible and bootstrappable builds have the potential to reduce such attacks significantly. In combination with independent, exhaustive and periodic source code audits, these measures can effectively eradicate compromises in the building process. In this paper we introduce both concepts, we analyze the achievements over the last ten years and explain the remaining challenges.

What is more, the paper aims to:

> … contribute to the reproducible builds effort by **setting up a rebuilder and verifier instance to test the reproducibility of Arch Linux packages**. Using the results from this instance, we uncover an unnoticed and security-relevant packaging issue affecting 16 packages related to Certbot […].

A [PDF](https://dl.gi.de/server/api/core/bitstreams/f8685808-2e51-4a53-acc0-2b45fa240e3b/content) of the paper is available.

<br>

### [`libntlm` now releasing 'minimal source-only tarballs'](https://blog.josefsson.org/2024/04/13/reproducible-and-minimal-source-only-tarballs/)

[Simon Josefsson](https://blog.josefsson.org/) wrote on his blog this month that, going forward, the [`libntlm`](https://gitlab.com/gsasl/libntlm/) project will now be releasing what they call "[minimal source-only tarballs](https://blog.josefsson.org/2024/04/13/reproducible-and-minimal-source-only-tarballs/)":

> The [XZUtils incident](https://en.wikipedia.org/wiki/XZ_Utils_backdoor) illustrate that tarballs with files that are not included in the git archive offer an opportunity to disguise malicious backdoors. [The] risk of hiding malware is not the only motivation to publish signed minimal source-only tarballs. With pre-generated content in tarballs, there is a risk that GNU/Linux distributions [ship] generated files coming from the tarball into the binary `*.deb` or `*.rpm` package file. Typically the person packaging the upstream project never realized that some installed artifacts was not re-built[.]

Simon's [post](https://blog.josefsson.org/2024/04/13/reproducible-and-minimal-source-only-tarballs/) goes into further details how this was achieved, and describes some potential caveats and counters some expected responses as well. A shorter version can be found in the announcement for the [1.8 release of `libntlm`](https://lists.nongnu.org/archive/html/libntlm/2024-04/msg00000.html).

<br>

### Distribution work

[![]({{ "/images/reports/2024-04/debian.png#right" | relative_url }})](https://debian.org/)

In Debian this month, Helmut Grohne [filed a bug](https://bugs.debian.org/1068809) suggesting the removal of `dh-buildinfo`, a tool to generate and distribute `.buildinfo`-like files within binary packages. Note that this is distinct from the `.buildinfo` generation performed by `dpkg-genbuildinfo`. By contrast, the entirely optional `dh-buildinfo` generated a `debian/buildinfo` file that would be shipped within binary packages as `/usr/share/doc/package/buildinfo_$arch.gz`.

Adrian Bunk recently asked about [including source hashes in Debian's .buildinfo files](https://bugs.debian.org/1068483), which prompted Guillem Jover to refresh some old patches to dpkg to make this possible, which revealed some quirks Vagrant Cascadian discovered when testing.

In addition, 21 reviews of Debian packages were added, 22 were updated and 16 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). A number issue types have been added, such as new [`random_temporary_filenames_embedded_by_mesonpy`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/67f129bc) and [`timestamps_added_by_librime`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/3675debb) toolchain issues.

[![]({{ "/images/reports/2024-04/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

In openSUSE, it was announced that their Factory distribution [enabled bit-by-bit reproducible builds](https://news.opensuse.org/2024/04/18/factory-bit-reproducible-builds/) for almost all parts of the package. Previously, more parts needed to be ignored when comparing package files, but now only the signature needs to be deleted.

In addition, Bernhard M. Wiedemann published [`theunreproduciblepackage`](https://build.opensuse.org/package/show/home:bmwiedemann:reproducible/theunreproduciblepackage) as a proper `.rpm` package which it allows to better test tools intended to debug reproducibility. Furthermore, it was announced that Bernhard's work on a [100% reproducible openSUSE-based distribution](https://nlnet.nl/project/Reproducible-openSUSE/) will be funded by [NLnet](https://nlnet.nl).
He also posted another [monthly report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/WJZMFH4QJHOJ7MMGKWBXSB7M3HOH5XOC/) for his reproducibility work in openSUSE.

[![]({{ "/images/reports/2024-04/guix.png#right" | relative_url }})](https://www.gnu.org/software/guix/)

In GNU Guix, Janneke Nieuwenhuizen submitted a patch set for creating a reproducible source tarball for Guix. That is to say, ensuring that `make dist` is reproducible when run from Git.&nbsp;[[…](https://issues.guix.gnu.org/70169/)]

[![]({{ "/images/reports/2024-04/fedora.png#right" | relative_url }})](https://fedoraproject.org/)

Lastly, in Fedora, a new wiki page was created to propose a change to the distribution. Titled "[*Changes/ReproduciblePackageBuilds*](https://fedoraproject.org/wiki/Changes/ReproduciblePackageBuilds)", the page summarises itself as a proposal whereby "A post-build cleanup is integrated into the RPM build process so that common causes of build irreproducibility in packages are removed, making most of Fedora packages reproducible."

<br>

### Mailing list news

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month:

* Continuing a [thread started in March 2024](https://lists.reproducible-builds.org/pipermail/rb-general/2024-March/thread.html#3301) about the [Arch Linux minimal container now being 100% reproducible]({{ "/reports/2024-03/#arch-linux-minimal-container-userland-now-100-reproducible" | relative_url }}), John Gilmore [followed up with a post](https://lists.reproducible-builds.org/pipermail/rb-general/2024-April/003336.html) about the practical and philosophical distinctions of local vs. remote storage of the various artifacts needed to build packages.

* Chris Lamb asked the list which conferences readers are attending these days: "After peak Covid and other industry-wide changes, conferences are no longer the 'must attend' events they previously were… especially in the area of software supply-chain security. In rough, practical terms, it seems harder to justify conference travel today than it did in mid-2019." The thread generated a [number of responses](https://lists.reproducible-builds.org/pipermail/rb-general/2024-April/thread.html#3370) which would be of interest to anyone planning travel in Q3 and Q4 of 2024.

* James Addison wrote to the list about a ["quirk" in Git related to its `core.autocrlf` functionality](https://lists.reproducible-builds.org/pipermail/rb-general/2024-April/003385.html), thus helpfully passing on a "slightly off-topic and perhaps not of direct relevance to anyone on the list today" note that might still be "the kind of issue that is useful to be aware of if-and-when puzzling over unexpected git content / checksum issues (situations that I _do_ expect people on this list encounter from time-to-time)".

<br>

### [*diffoscope*](https://diffoscope.org)

[![]({{ "/images/reports/2024-04/diffoscope.png#right" | relative_url }})](https://diffoscope.org/)

[diffoscope](https://diffoscope.org) is our in-depth and content-aware diff utility that can locate and diagnose reproducibility issues. This month, Chris Lamb made a number of changes such as uploading versions `263`, `264` and `265` to Debian and made the following additional changes:

* Don't crash on invalid `.zip` files, even if we encounter their 'badness' halfway through the file and not at the time of their initial opening.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/9c7e817c)]
* Prevent `odt2txt` tests from always being skipped due to an (impossibly) new version requirement.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8e6f778c)]
* Avoid parens-in-parens in test 'skipping' messages.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/99afaf60)]
* Ensure that tests with `>=`-style version constraints actually print the tool name.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e52eef5a)]

In addition, Fay Stegerman fixed a crash when there are (invalid) duplicate entries in `.zip` which was originally reported in Debian bug [#1068705](https://bugs.debian.org/1068705)).&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/945fd9fa)] Fay also added a user-visible 'note' to a diff when there are duplicate entries in ZIP files&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/607094a5)]. Lastly, Vagrant Cascadian added an external tool pointer for the `zipdetails` tool under [GNU Guix](https://guix.gnu.org/)&nbsp;[[…](https://salsa.debian.org/reproducible-builds/diffoscope/commit/90dd1883)] and proposed updates to diffoscope in Guix as well [[...](https://issues.guix.gnu.org/70428)] which were merged as [[264](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=af969a594a2972b230caa9666fb83a2011b5d464)] [[265](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=10d0e2d3110e4be2bc6cfecb9a3abb83d8e1ddd6)], fixed a [regression in test coverage](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=c1c9d6b3cdf5955f1bf5fded2a0c496ce2e631f1) and [increased verbosity of the test suite](https://issues.guix.gnu.org/70344)[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=4dec6e9fb74a688e0297b127773b88a699531785)].

<br>

### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Chris Lamb:

    * [#1068173](https://bugs.debian.org/1068173) filed against [`pg-gvm`](https://tracker.debian.org/pkg/pg-gvm).
    * [#1068176](https://bugs.debian.org/1068176) filed against [`goldendict-ng`](https://tracker.debian.org/pkg/goldendict-ng).
    * [#1068372](https://bugs.debian.org/1068372) filed against [`grokevt`](https://tracker.debian.org/pkg/grokevt).
    * [#1068374](https://bugs.debian.org/1068374) filed against [`ttconv`](https://tracker.debian.org/pkg/ttconv).
    * [#1068375](https://bugs.debian.org/1068375) filed against [`ludevit`](https://tracker.debian.org/pkg/ludevit).
    * [#1068795](https://bugs.debian.org/1068795) filed against [`pympress`](https://tracker.debian.org/pkg/pympress).
    * [#1069168](https://bugs.debian.org/1069168) filed against [`sagemath-database-conway-polynomials`](https://tracker.debian.org/pkg/sagemath-database-conway-polynomials).
    * [#1069169](https://bugs.debian.org/1069169) filed against [`gap-polymaking`](https://tracker.debian.org/pkg/gap-polymaking).
    * [#1069663](https://bugs.debian.org/1069663) filed against [`dub`](https://tracker.debian.org/pkg/dub).
    * [#1069709](https://bugs.debian.org/1069709) filed against [`dpb`](https://tracker.debian.org/pkg/dpb).
    * [#1069784](https://bugs.debian.org/1069784) filed against [`python-itemloaders`](https://tracker.debian.org/pkg/python-itemloaders).
    * [#1069822](https://bugs.debian.org/1069822) filed against [`python-gvm`](https://tracker.debian.org/pkg/python-gvm).

* Bernhard M. Wiedemann:

    * [`metis`](https://build.opensuse.org/request/show/1164284) (fix build with --nocheck)
    * [`musique`](https://build.opensuse.org/request/show/1163266) (fix a date-related issue)
    * [`orthanc-volview`](https://build.opensuse.org/request/show/1160385) (fix an issue with mtimes and sorting)
    * [`go1.13`](https://build.opensuse.org/request/show/1168496), [`go1.14`](https://build.opensuse.org/request/show/1167936), [`go1.15`](https://build.opensuse.org/request/show/1168495) (fix a parallelism-related issue)
    * [`postfish`](https://build.opensuse.org/request/show/1168486) (disable compile-time benchmarking)
    * [`geany/glfw`](https://build.opensuse.org/request/show/1169977) (toolchain, random)
    * [`edk2/ovmf/tianocore`](https://github.com/tianocore/edk2/pull/5550) (with Joey Li: fix a date-related issue)
    * [`dlib`](https://bugzilla.opensuse.org/show_bug.cgi?id=1223168) (report an issue with compile-time-CPU-detection)
    * [`lua-lmod`](https://github.com/TACC/Lmod/pull/702) (fix a date-related issue)
    * [`gitui`](https://github.com/extrawurst/gitui/pull/2202) (fix a date-related issue)
    * [`openssl-3`](https://bugzilla.opensuse.org/show_bug.cgi?id=1223336) (report an issue with random output)
    * [`gcc14`](https://bugzilla.suse.com/show_bug.cgi?id=1223169) (FTBFS-2038)
    * [`nebula`](https://github.com/slackhq/nebula/issues/1124) (FTBFS-2027-11-11)

* Jan Zerebecki:

    * [rpm](https://github.com/rpm-software-management/rpm/pull/2880) (Support reproducible automatic rebuilds, etc.)
    * [openSUSE-release-tools](https://github.com/openSUSE/openSUSE-release-tools/pull/3064) (Create changelog for generated package sources for `SOURCE_DATE_EPOCH`)
    * [pesign-obs-integration](https://github.com/openSUSE/pesign-obs-integration/pull/48) (Create changelog for generated package sources for `SOURCE_DATE_EPOCH`)
    * [openSUSE post-build-checks](https://github.com/openSUSE/post-build-checks/pull/62) (Set `SOURCE_DATE_EPOCH`)
    * [obs-build](https://github.com/openSUSE/obs-build/pull/977) (Fix changelog timezone handling)
    * [obs-service-tar_scm](https://github.com/openSUSE/obs-service-tar_scm/pull/484) (When generating changelog from Git, create the file if it does not exist.)

* Thomas Goirand:

    * [`oslo.messaging`](https://github.com/openstack/oslo.messaging/commit/dc55d64df989bdb5161ca8ad8d74115cc2959174) (fix a hostname-related issue)

<br>

### [*reprotest*](https://salsa.debian.org/reproducible-builds/reprotest)

[*reprotest*](https://salsa.debian.org/reproducible-builds/reprotest) is our tool for building the same source code twice in different environments and then checking the binaries produced by each build for any differences. This month, *reprotest* version `0.7.27` was uploaded to Debian unstable) by Vagrant Cascadian who made the following additional changes:

* Enable specific number of CPUs using `--vary=num_cpus.cpus=X`.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reprotest/commit/cdabc07)]
* Consistently use 398 days for time variation, rather than choosing randomly each time.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reprotest/commit/42a53ed)]
* Disable builds of `arch:any` packages.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reprotest/commit/3270c94)]
* Update the description for the `build_path.path` option in `README.rst`.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reprotest/commit/9235862)]
* Update escape sequences for compatibility with Python 3.12. ([#1068853](https://bugs.debian.org/1068853)).&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reprotest/commit/cf65735)]
* Remove the generic 'upstream' signing-key&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reprotest/commit/7400030)] and update the packages' signing key with the currently active team members&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reprotest/commit/d11398f)].
* Update the packaging `Standards-Version` to 4.7.0.&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reprotest/commit/82777f9)]

In addition, Holger Levsen fixed some spelling errors detected by the `spellintian` tool&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reprotest/commit/96e324a)] and Vagrant Cascadian updated reprotest in [GNU Guix](https://guix.gnu.org/) to [0.7.27](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=f006dc5d74c6cc613dfab1a91e67e8e8d806f2d1).

<br>

### Reproducibility testing framework

[![]({{ "/images/reports/2024-04/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a comprehensive testing framework running primarily at [*tests.reproducible-builds.org*](https://tests.reproducible-builds.org) in order to check packages and other artifacts for reproducibility.

In April, an enormous number of changes were made by Holger Levsen:

* [Debian](https://debian.org/)-related changes:

    * Adjust for changed internal IP addresses at Codethink.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/d202c0449)]
    * Automatically cleanup failed *diffoscope* user services if there are too many failures.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/e829e6e71)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/b2401650a)]
    * Configure two new nodes at infomanik.cloud.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/cc1ed0063)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/3709b0f1c)]
    * Schedule Debian *experimental* even less.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/73013d6f6)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/4b5f4cb09)]

* Breakage detection:

    * Exclude currently building packages from breakage detection.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/92078b002)]
    * Be more noisy if *diffoscope* crashes.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/9997af327)]
    * Health check: provide clickable URLs in jenkins job log for failed pkg builds due to diffoscope crashes.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/386ec0aa4)]
    * Limit graph to about the last 100 days of breakages only.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/c88e08dfd)]
    * Fix all found files with bad permissions.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/6d0c66f1e)]
    * Prepare dealing with *diffoscope* timeouts.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/98ba4fe38)]
    * Detect more cases of failure to *debootstrap* base system.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/53865a60c)]
    * Include timestamps of failed job runs.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/00cca3c93)]

* Documentation updates:

    * Document how to access arm64 nodes at Codethink.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/a247125b0)]
    * Document how to use infomaniak.cloud.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/cd004dd6c)]
    * Drop notes about long stalled LeMaker HiKey960 boards sponsored by HPE and hosted at ETH.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/0a31a1fe9)]
    * Mention `osuosl4` and `osuosl5` and explain their usage.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/3b390f7e7)]
    * Mention that some packages are built differently.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/d68086a4b)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/6067b5612)]
    * Improve language in a comment.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/77dbf257b)]
    * Add more notes how to query resource usage from `infomaniak.cloud`.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/ea1035e7b)]

* Node maintenance:

    * Add `ionos4` and `ionos14` to THANKS.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/be7d08960)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/699b5554c)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/2e3bcbada)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/09fccba39)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/da9063ad4)]
    * Deprecate Squid on `ionos1` and `ionos10`.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/674f55d6d)]
    * Drop obsolete script to powercycle `arm64` architecture nodes.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/b4d37b5b3)]
    * Update `system_health_check` for new proxy nodes.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/148d252d0)]

* Misc changes:

    * Make the `update_jdn.sh` script more robust.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/ef3de23bd)][[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/2c1d7272f)]
    * Update my SSH public key.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/23ab1af4f)]

In addition, Mattia Rizzolo added some new host details.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/faddf9eaa)]

<br>

---

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mastodon: [@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
