---
layout: event_detail
title: Collaborative Working Sessions - Verification Use Cases
event: hamburg2023
order: 211
permalink: /events/hamburg2023/verification2/
---

- have some central place for people to upload attestations?

    - put everything into one database?

    - disks are cheap, but querying data is complicated

- how do we display data?

    - have a website?

    - a graph?

- maybe collect them in git repos?

    - every entity runs their own repo

- we need to be able to tell which entity did the rebuild
- do we need additional data for easier triage?

    - cpu features?

- maybe she should keep track of the cpu features of the rebuilder?
- the buildinfo file should canonically describe a "blessed" environment
- each language package manager (cargo, npm, composer, ...) is their own "distro", from a r-b point of view
- do we want to match results between distros?

    - is this doable/useful?

    - in Arch Linux, we often know what the issue is based on a single diffoscope, the challenge is triage/fixing all root causes

- maybe something similiar to crev? https://github.com/crev-dev/crev/
