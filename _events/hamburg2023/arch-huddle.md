---
layout: event_detail
title: Collaborative Working Sessions - Arch huddle
event: hamburg2023
order: 307
permalink: /events/hamburg2023/arch-huddle/
---
# Making Arch Linux Debug Packages Reproducible

This should be handled in three different steps. There are questions remaining to be answered before proceeding with the integration.

## `debuginfod`

- Is `debuginfod` secure?
  - i.e. Is there authentication between `gdb` and `debuginfod`?

⚠️ It is theoretically possible to perform code execution through debug symbols.

## Mirrors

- Right now the debug packages live in a single server. We should start distributing them through mirrors and potentially have them in our archives as well.
  - There is a question about storage since debug packages might take a good amount of disk space.
  - "We shouldn't let the limitations of mirrors affect our design choices".

## Integration

Here are the tools that needs integration:

- [rebuilderd](https://github.com/kpcyrd/rebuilderd)
- [devtools](https://gitlab.archlinux.org/archlinux/devtools)
- [repro](https://github.com/archlinux/archlinux-repro)
  - We need to check hashes etc.
