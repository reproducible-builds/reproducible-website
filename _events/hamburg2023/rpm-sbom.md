---
layout: event_detail
title: Collaborative Working Sessions - SBOM for rpm
event: hamburg2023
order: 300
permalink: /events/hamburg2023/rpm-sbom/
---

SBOM discussion led by Marek

rpmbuild should produce buildinfo file during package-build

currently fragmented: OBS, koji, others reinvent their own formats

There was previous discussion with rpm maintainers.
Idea: produce separate sub-package with that buildinfo file.
format was too Debian-ish and therefore disliked by rpm maintainers.

buildinfo-rpm can be signed the normal way
can be published to separate repo (similar to debuginfo)


Prior work:
* https://github.com/rpm-software-management/rpm/pull/1532 + rpmrebuild
* https://github.com/rpm-software-management/rpm/issues/2389
* http://download.opensuse.org/update/leap/15.5/sle/x86_64/ has slsa_provenance.json in-toto format
* https://github.com/opensbom-generator/spdx-sbom-generator#module-json-example
* https://cyclonedx.org/
* some Yocto-based medical device collects plenty data from build

goal:
 * be able to independently verify rpms / containers
 * common tool for reproducing rpm packages - no matter from which distribution
 * also for 3rd-party packages such as google-chrome

Ideas:
 * discuss more with upstream: what value it would provide
 * let upstream come up with a PR
 * have prepared shared zstd dict for efficient SBOM compression

result/output-SBOM vs input/build-SBOM
  => see also notes on Wed discussion on SBOM
  SPDX + CycloneDX + in-toto file format

consumers for SBOM files:
* CVE-scanners
* License-scanners


missing link for publishing required buildrequires rpm + fetching via name|shasum
* URL for provider service
* archive.org
* IPFS
* other


