---
layout: event_detail
title: Practical outcomes
event: hamburg2024
order: 307
permalink: /events/hamburg2024/outcomes/
---

# Things being worked on

- [meson reprotest command](https://github.com/mesonbuild/meson/pull/13689)
- Add [meson reprotest CI job definition](https://gitlab.gnome.org/guidog/meta-phosh/-/merge_requests/13)
  (multiple projects associated with [phosh](https://phosh.mobi))
- Ensure stable sorting in [phosh-osk-stub](https://gitlab.gnome.org/guidog/phosh-osk-stub/-/merge_requests/154)

# Things that are done (e.g. have landed upstream)

- [TOPIC](LINK)

