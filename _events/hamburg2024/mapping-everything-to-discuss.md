---
layout: event_detail
title: Mapping everything to discuss
event: hamburg2024
order: 11
permalink: /events/hamburg2024/mapping-everything-to-discuss/
---

# Everything we need to talk about

## (no label)

 * Exgend R-B to quantum computers

## (no label)

 * The use of transparency logs

## Tooling

* Creating underhanded RB contest
* Easy actionables to do in a team to facilitate R13Y
* Make reproducible builds by default in build tools
* Make repro builds default in compilers
* Bridging CI/CD with package builds
* Improve/add heuristics to reproduce Maven artifacts
* Produce an easy way to check reproducibility of signed binaries
* Automating reproducibility checks

## Rebuilders

* Consensus implementation for rebuilders
* How to share rebuild results between rebuilders
* Rebuild definition formats
* Rebuilder network design
* Interchange format for rebuild info
* Do we worry about reproducible package version selection? (e.g., repro lockfile generation)
* Trust model for human contributed rebuild definitions
* Encouraging diversity of rebuilders

## Languages

* Python R-B issues
* Rebuilding Python packages that are available on PyPI
* Lisp+Scheme R-B issues
* Rust R-B issues

## Distros/OS

* How to make Debian-based docker images reproducible?
* macOS codesigning and reproducible builds
* Immutable operating systems are the solution?
* BOotstrapping reproducibility

## Docs

* What are the milestones for this year?
* Mentor programs like GSoC/Outreachy?
* How to contribute to the cause?
* Carrot/sticks to get pkg maintainers to be reproducible
* Trigger end user interest

## Source code

* Establishing canonical source repos
* Source tarball reproducibility
* How to avoid another "snapshot is broken" situation
