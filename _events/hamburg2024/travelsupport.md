---
layout: default
title: Hamburg 2024 - Travel Bursary
permalink: /events/hamburg2024/travelsupport
event_hide: true
event_date: 2024-09-17
event_date_string: September 17-19 2024
event_location: Hamburg, Germany
---

# {{ page.title }}

**<a href="{{ "/events/hamburg2024" | relative_url }}">← Main event page</a>**

### Instructions for requesting travel and/or accommodation sponsorship

If you are seeking sponsorship to attend this event, please email the organizers with these details:

    I'm seeking sponsorship for:
    [ ] accommodation
    [ ] travel
        * I'm travelling from ____ via _____ (main mode of transportation)
        * When going back I'll go to _____ via ____ (main mode of transportation)
        * I foresee the expense will be ______ €

You will need to pay the travel from your own pocket and you will be reimbursed *after* the event; in case you won't be able to attend the event we won't be providing reimbursement.

The budget for sponsored attendance is limited, so please do send your request early.


If you'd rather not have such information recorded on a mailing list, please send the email directly to Mattia instead.
