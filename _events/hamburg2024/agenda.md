---
layout: event_detail
title: Agenda
event: hamburg2024
order: 10
permalink: /events/hamburg2024/agenda/
---


# Reproducible Builds Summit 2024

The following was the schedule for the 2024 Reproducible Builds Summit in Hamburg, Germany.


# Agenda Overview 

**The following is the working schedule for the 2024 Reproducible Builds Summit.**

The agenda is being designed as a combination of planned sessions and participant-driven discussions, and specific topics will be placed into time slots based on pre-event input from those who will be in attendance.

**The following are some general guidelines regarding how we'll run the workshop:**

- The agenda will be flexible and dynamic. Sessions will be adjusted and added based on suggestions and requests by both participants and organisers. Stated session and break times are approximate and may shift. But we plan to start each day on time and end on time or early, and we shall not be late to lunch :)

- Session topics and outcomes have been sourced from pre-event input from participants. We *welcome and encourage* additional session requests and proposals, as long as they focus on collaboration, co-creation and sharing of experiences, goals or visions, rather than being lecture-oriented or slideware-based.

- Full participation in the program is requested; part-time participation will work against the overall event goals. In particular, we ask all participants to refrain from scheduling side meetings during stated agenda times.

- This is a "**devices** at ease" event: use of laptops and cellphones during sessions is strongly discouraged with the exception of designated note takers. We invite participants to use the morning and afternoon breaks, as well as the lunch hour, to check in with external realities.

- We will follow Aspiration’s Participation Guidelines (https://facilitation.aspirationtech.org/index.php?title=Participants:Guidelines) and Event Expectations (https://aspirationtech.org/events/expectations) during the meeting.

- Overall, sessions are intended to be highly interactive. Facilitators' primary goal will be to enable collaborative and co-created outputs, constructively address questions, and support peer sharing. We invite all participants to bring your ideas, your questions, and your knowledge to contribute.


# Day 1 - Tuesday 17 September 2024

## 8.00 Breakfast at venue

## 9.30 Opening Circle

The 2024 Reproducible Builds Summit will be called to order with a fast-paced kickoff that includes words of welcome from the organizers, brief participant introductions, along with overviews of the agenda, participation guidelines and meeting logistics.

## 10.00 Interactive Project Updates

We'll start the program with short question-driven updates from a range of participants on a range of projects from across the Reproducible Builds ecosystem.

### Round I

* Maven and Java ecosystem

* [growing reproducible-central activity and contributions](https://github.com/jvm-repo-rebuild/reproducible-central/graphs/contributors)

* output-file level GitHub badge, [for example](https://github.com/jvm-repo-rebuild/reproducible-central/blob/master/content/io/quarkus/README.md)

* feedback: interest in other repositories than Maven Central: Android, Kotlin/Jetbrains

* whatsrc

  - [Homepage](https://whatsrc.org)

* binsider

  - [Homepage](https://binsider.dev/)
  - [Github](https://github.com/orhun/binsider)

* reproducible nikita / prefix

FIXME: Add links here...

* Debian status

  - 97% reproducible in CI
  - reproducible docker images
  - reproducible live-images
  - debian-policy: we are very close to enforce no regressions and no new packages which are unreproducible
  - snapshot.debian.org got fixed (required for rebuilders)
  - now: setting up rebuilders...


### Round II

* OpenSUSE

FIXME: Add links / description here...

* Tor Browser

  - [Framework for building Tor Browser reproducibly](https://gitlab.torproject.org/tpo/applications/tor-browser-build/)
  - We build a lot of stuff, see the projects directory. We have to interact with a lot of different build systems (Firefox's, CMake+Ninja, Go, Cargo, autotools...).
  - We we have a series of bash scripts that are customized in real time with a tool Nicolas wrote ([RBM](https://gitlab.torproject.org/tpo/applications/rbm/))
  - Firefox ESR 115 -> 128 migration
  - Linux + macOS: no reproducibility problems
  - Windows: problems with the Rust toolchain, due to the GCC/mingw toolchain we used to build Rust std. We migrated to the arch-pc-windows-gnullvm targets (See the [documentation](https://doc.rust-lang.org/rustc/platform-support/pc-windows-gnullvm.html))
  - Android: strange regression on the Gradle open source license generator. The patch is easy, but we hope to fix it upstream

* OSS Rebuild

  - [GitHub](https://github.com/google/oss-build)

* Aroma

  - AROMA stands for Automatic Reproduction of Maven Artifacts. We investigated how far we can go with simple heuristics to help Reproducible Central.

  - [Paper @ https://doi.org/10.1145/3643764](https://dl.acm.org/doi/pdf/10.1145/3643764)

* System Transparency

FIXME

Add links / description here...

* Reproducible development environments at work... Made easy

FIXME: 

Add links / description here...


## 11.45 Break

Please note that all break times are approximate :)

## 11.30 Mapping the Big Picture

Building on the mappings we did at the 2023 Reproducible Builds Summit, the group will use this time to take stock of where things stand for Reproducible Builds across a range of context, as of the Summit. We'll identify success stories, exemplars and case studies to be celebrated and amplified, while also mapping challenges, needs and unsolved problems.

Topics, issues and ideas that surface during this session will inform how we structure the rest of the agenda.

* [Everything we need to talk about]({{ "/events/hamburg2024/mapping-everything-to-discuss/" | relative_url }})
* [Success Stories and Unsolved Problems]({{ "/events/hamburg/2024/mapping-success-and-unsolved" | relative_url }})
* [Missing maps/lists/documentation/visualizations]({{ "/events/hamburg/2024/mapping-missing-documentation" | relative_url }})

## 12.30 Lunch in Cantina

Participants are encouraged to sit with those who they have not yet met or engaged.

## 14.00 Collaborative Working Sessions

* Getting started with reproducible investigations

https://pad.riseup.net/p/rbsummmit2024-d1a-gettingstarted-keep

* Making the business case for reproducible builds

https://pad.riseup.net/p/rbsummmit2024-d1a-making-case-keep

* Rebuilder information exchange

https://pad.riseup.net/p/rbsummmit2024-d1a-rebuilder-exchange-keep

* Debian supply chain

https://pad.riseup.net/p/rbsummmit2024-d1a-debian-supply-chain-keep

* RB problem prioritization

https://pad.riseup.net/p/rbsummmit2024-d1a-problem-prioritization-keep


## 15.45 Closing Circle

The closing session will invite participants to weigh in on what has been most useful during the course of Day 1, and refine their goals and priorities for the agenda of Day 2.

## 16.00 Break

Please note that all break times are approximate :)

## 16.30 Hack Time

The group will decide how to allocate remaining Day 1 time between working sessions and hack time.

## 18.00 Dinner at venue

# Day 2 - Wednesday 18 September 2024

## 8.00 Breakfast at venue

## 9.30 Opening Circle

The day will start with a summary of Day 1 outcomes and a Day 2 Agenda Overview.

## 9.45 Collaborative Working Sessionsd

* Rebuilders II

https://pad.riseup.net/p/rbsummmit2024-d2m-rebuilders-keep

* Kernel reproducibility

https://pad.riseup.net/p/rbsummmit2024-d2m-kernel-keep

* Getting started with reproducible investigations II

https://pad.riseup.net/p/rbsummmit2024-d2m-gettingstarted-keep

* Making the business case for reproducible builds II

https://pad.riseup.net/p/rbsummmit2024-d2m-making-case-keep

* TOPIC

https://pad.riseup.net/p/rbsummmit2024-d2m-TOPIC-keep


## 11.15 Break

Please note that all break times are approximate :)

## 11.30 Peer skill and knowledge share

Participants will be invited and encouraged to request or share any skill they consider relevant to the meeting scope. The session will be structured so as to minimize group size and maximize 1-on-1 sharing opportunities.

## 12.30 Lunch in Cantina

Participants are encouraged to sit with those who they have not yet met or engaged.

## 14.00 Collaborative Working Sessions

* Rebuilders III

https://pad.riseup.net/p/rbsummmit2024-d2m-rebuilders-keep

* Kernel reproducibility

https://pad.riseup.net/p/rbsummmit2024-d2m-kernel-keep

* Getting started with reproducible investigations III

https://pad.riseup.net/p/rbsummmit2024-d2m-gettingstarted-keep

* Making the business case for reproducible builds III

https://pad.riseup.net/p/rbsummmit2024-d2m-making-case-keep


## 15.45 Closing Circle

The closing session will invite participants to weigh in on what has been most useful during the course of Day 1, and refine their goals and priorities for the agenda of Day 2.

## 16.00 Break

Please note that all break times are approximate :)

## 16.30 Hack Time

The group will decide how to allocate remaining Day 1 time between working sessions and hack time.

## 18.00 Dinner at venue

# Day 3 - Thursday 19 September 2024

## 8.30 Breakfast at venue

## 9.30 Opening Circle

The day will start with a summary of Day 2 outcomes and a Day 3 Agenda Overview.

## 9.45 Collaborative Working Sessions

Working sessions continue.

* TOPIC

https://pad.riseup.net/p/rbsummmit2024-d3m-TOPIC-keep

* TOPIC

https://pad.riseup.net/p/rbsummmit2024-d3m-TOPIC-keep

* TOPIC

https://pad.riseup.net/p/rbsummmit2024-d3m-TOPIC-keep

* Getting Started with Reproducible Builds

https://pad.riseup.net/p/rbsummmit2024-d3m-gettingstarted-keep

* Making the business case for reproducible builds IV

https://pad.riseup.net/p/rbsummmit2024-d3m-TOPIC-keep


## 11.15 Break

Please note that all break times are approximate :)

## 11.30 Collaborative Working Sessions

* Teaching RB at universities

https://pad.riseup.net/p/rbsummmit2024-d3m-university-curriculum-keep

* TOPIC

https://pad.riseup.net/p/rbsummmit2024-d3m-TOPIC-keep

* TOPIC

https://pad.riseup.net/p/rbsummmit2024-d3m-TOPIC-keep

* TOPIC

https://pad.riseup.net/p/rbsummmit2024-d3m-TOPIC-keep

* TOPIC

https://pad.riseup.net/p/rbsummmit2024-d3m-TOPIC-keep


## 13.00 Lunch in Cantina

Participants are encouraged to sit with those who they have not yet met or engaged.

## 14.00 Mapping next conversations and next steps

The group will pause before the final session to take stock of the progress made to this point in the week and to inventory action items, next steps and other bridges to post-event collaboration.

## 15.30 Closing Circle

Participants will summarize key outcomes from the event, and discuss next steps for continuing collaboration after the meeting.

## 16.00 Adjourn / Hack Time

## 19.00 Dinner at venue



