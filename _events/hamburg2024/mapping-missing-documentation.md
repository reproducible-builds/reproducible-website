---
layout: event_detail
title: Mapping missing Documentation
event: hamburg2024
order: 13
permalink: /events/hamburg2024/mapping-missing-documentation/
---

# Missing maps/lists/documentation/visualization

* what is blocking progress
* open issues by programming language
* A non-distro/all-distry list of unreproducible software
* RB issues with doxygen: common problems&solutions
* I want a list of all pypi pythron reproducible packages
* CVEs but for reproducibility
* List Security bugs in RB processes and tools (diffoscope, verifification scripts, etc)
* list services and/or infrastructure we depend on
* different types of attestations, pros and cons of each of them
* homepage/doc: other classes of repo issues besides what is already documented?
* to have some guidelines about how to be a reproducile build tools
* review docs website for direct links to targeted actionable headings
* cyber resiliance act requires SBOM, what are tbe best practices to implement them?
* best practices to investigate and resolve reproducibility bugs
* how to properly introduce reproducibility in a team? why would we do that?
* doc explaining how to set up and run rebuilders
* easy TODOs for student projects (3-6 month wokring 3-4h/week)
* format to communicate rebuilder capabilities
* "service [name] only has reproducible deps" green checkmarks
* rewrite strip-nondeterminism in <not perl>
* tools to help building reproducibly
* divergence between differnt packaging reproducibility for same upstreams
* what is the standard way to make go binaries reproducible
* how to properly sign APKs to then run APKSIG copier
* how can I gather gradle dependencies in the proper way
* source packages to upstream repositories
