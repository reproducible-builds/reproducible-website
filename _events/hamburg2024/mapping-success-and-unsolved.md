---
layout: event_detail
title: Mapping sucess stories and unsolved problems
event: hamburg2024
order: 12
permalink: /events/hamburg2024/mapping-success-and-unsolved/
---

# Success stories

* Bernhard's RB-OS ring 0 reproducible
* Independent rebuild check is part of release workflow of Apache Airflow (data science)
* R-B mentioned in SLSA 4
* Doing the 8th R-B summit
* Timestamp issues solved by SOURCE_DATE_EPOCH
* A network of rebuilders exists
* Independent Arch Linux Rebuilderd in an Applied University :)
* Arch Linux has independent rebuilders for real-world binaries
* Projects are happy to take patches to ensure reproducibility (in my experience)
* Practice of R-B is known and accepted by many developers
* [Meta] R-B website getting an update about success stories
* Debian containers are reproducible
* Conda-packages are reproducible using rattler-build
* apt.vulns.xyz documents how to do reproducible 3rd party apt repos
* repro-env tracks "traditional" Linux build environments
* Using reproducible development env. is an amazing experience
* apt-swarm implements an authority-less p2p transparency log
* RB + SBOM permitted to find broken dependencies in releases
* Finding bugs: libxslt issue 37 "puzzled why it took so long to discover this issue"


# Unsolved problems

* Establish canonical source repos
* Agreeing on source code consensus
* How to systematically detect toolchain reproducibility regressions
* Reproducible day to day dev builds
* Document format and protocols for rebuilder network(s) missing
* How do we build a system of attesters for proving reproducibility?
* Still not a good enough final user (regular, simple human) motivation and publicity
* Motivate Maven devs to add timestamp to their pom files
* How to make the world benefit from R-B
* Filesystem/VM image reproducibility
* Awareness in IT, crypto, and cybersecurity fields
* Linux secure boot and reproducible builds are incompatible
* Deriving build instructions
* No contact with proprietary tool vendors (e.g., Apple)
* How do we create fully reproducible infrastructure? Is Terraform enough?
* Reproducibility requirements in cryptographic standards (e.g., NIST, BSI, ...)
* Embedded signatures making build non-reproducible
* Many open source devs I talked with don't know about reproducible builds (but they agree it's a good idea once you explain it to them)
* I can't find a way to sell r13y to people that are not aware of it :-(
* How a maintainer can declare/communicate intended non-reproducible parts of binaries
* We need a serious marketing effort for R B adoption
* People afraid of learning new tools/tech
* Transparency logs, how to do them, how to use them
* Haskell's GHC has non-deterministic output with concurrency enabled
* Solve RB for iOS ecosystem (Apple modifies .ipa uploads)
