---
layout: event_detail
title: Collaborative Working Sessions - Firmware
event: venice2022
order: 130
permalink: /events/venice2022/firmware/
---

Reproducible Builds Summit 2022

- What is Firmware
  - Integrated software components
  - Software w/o user interaction
  - Usually means cross-compilation
  - Usually "closed" systems with minimal user control
  - Often limited resources (memory, cpu)
- Build processes
  - Open vs. closed toolchains
    - Some are closed source
    - Some are GCC (sometimes with patches)
    - Some are clang
    - Some are "something else" (sdcc?)
- UEFI
  - many laptops won't allow changing the firmware
    - intel bootguard
    - in theory, you could reproduce it and verify, but this is hard due to closed-sourced components and unknown contents
  - should be reproducable if you build coreboot
  - more of a political challenge than technical
    - need access to source, toolchains, etc...
- projects/companies that are involved w/ reproducable builds
  - coreboot
  - openwrt
  - yocto
  - openembedded
  - trezor (crypto wallet)
  - mullvad (vpn provider)
- Benefits to end-users
  - users can't trust their devices without knowing where the firmware came from
  - companies purchasing equipment (network gear, etc...) have a strong need to trust the devices
  - potential for "software escrow"
- Some devices have higher trust needs
  - 2FA tokens
  - network hardware
  - crypto wallets
- Legal stuff
  - Stricter GPL version that requires reproducability?
- Why don't people do RB firmware now?
  - they don't know about it
  - they don't see benefit in it
  - cost sensitivity
