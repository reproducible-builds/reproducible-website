---
layout: default
title: CI tests
permalink: /citests/
order: 70
---

# Continuous tests

<div markdown="1">
The following projects are tested within the reproducible test infrastructure.
Click the different links to see current tests and results.
</div>

<div class="projects row bg-light p-md-5 p-sm-3 py-5">
    {% for project in site.data.projects %}
    {% if project.tests and project.tests_disabled != true and project.tests_external != true %}
    <div class="col-xs-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
        <div class="card text-center" name="{{ project.name }}">
            <a href="{{ project.tests }}">
                <img class="project-img" src="{{ project.logo | prepend: "/images/logos/" | relative_url }}" alt="{{ project.name }}">
                <h4 class="mt-0">{{ project.name }}</h4>
            </a>
        </div>
    </div>
    {% endif %}
    {% endfor %}
</div>

# External tests

<div markdown="1">
The following projects are being tested outside the reproducible test infrastructure.
</div>

<div class="projects row bg-light p-md-5 p-sm-3 py-5">
    {% for project in site.data.projects %}
    {% if project.tests and project.tests_external == true %}
    <div class="col-xs-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
        <div class="card text-center" name="{{ project.name }}">
            <a href="{{ project.tests }}">
                <img class="project-img" src="{{ project.logo | prepend: "/images/logos/" | relative_url }}" alt="{{ project.name }}">
                <h4 class="mt-0">{{ project.name }}</h4>
            </a>
        </div>
    </div>
    {% endif %}
    {% endfor %}
</div>

# Disabled tests

<div markdown="1">
The following projects were once tested within the reproducible test infrastructure, though now this tests
are disabled as they became unmaintained. Please contact us if you want to revive them.
</div>

<div class="projects row bg-light p-md-5 p-sm-3 py-5">
    {% for project in site.data.projects %}
    {% if project.tests and project.tests_disabled == true %}
    <div class="col-xs-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
        <div class="card text-center" name="{{ project.name }}">
            <a href="{{ project.tests }}">
                <img class="project-img" src="{{ project.logo | prepend: "/images/logos/" | relative_url }}" alt="{{ project.name }}">
                <h4 class="mt-0">{{ project.name }}</h4>
            </a>
        </div>
    </div>
    {% endif %}
    {% endfor %}
</div>
