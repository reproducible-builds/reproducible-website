---
title: Formal definition
layout: docs
permalink: /docs/formal-definition/
---

Most free software distributions are self-contained: all tools required
to build their components are part of the distribution. In such cases, it's
possible to specify the build environment in a machine readable format
that can be later used to reinstall the environment.

It is also useful for the relationship of these parts to be discoverable. This
currently only works within each software distributions. Even across
distributions that use the same formats (e.g. Debian and Ubuntu) it does not
yet work out of the box. But builds often use other build artefacts as source
inputs.

E.g. a container image was created by installing Debian packages, those
Packages often use tar archives, build from some git source repositiry, and
some git repos contain code developers generated with a build step. So to
recursively verify a specific artefact reproduces one needs to indentify all
the sources, mirror them and run some build jobs, across distributions. For
container images it is currently not specified how to look up the source from
an artefact or embed a identifier for the source in the artifact. And for many
artefact types looking up the source control revision is guess work instead of
a specification that can be followed, much less it being verified when a change
is submitted to a distribution.

Files that serve this goal are sometimes called a software bill of materials
(SBOM). Various formats are in use or were proposed for this:

* [Debian Buildinfo
  specification](https://wiki.debian.org/ReproducibleBuilds/BuildinfoSpecification),
  ([example Debian Buildinfo
  file](https://buildinfo.debian.net/d6a4da6e62bf21c9459197a4bf22d45725dc40f3/0ad_0.0.23.1-5_amd64))
* [openSUSE OBS Buildinfo documentation with
  example](https://api.opensuse.org/apidocs/#/Build/get_build__project_name___repository_name___architecture_name___package_name___buildinfo)
* [slsa provenance specification](https://slsa.dev/spec/v1.0/provenance),
  ([example of slsa povenance file produced by openSUSE
OBS](https://download.opensuse.org/update/leap/15.5/sle/x86_64/389-ds-2.2.8~git17.48834f1-150500.3.5.1.x86_64.slsa_provenance.json))
* [in-toto Attestation specification can include multiple formats that are
  related](https://github.com/in-toto/attestation/tree/main/spec/predicates)

As example, the .buildinfo control files used by Debian specifies the sources,
the generated binaries, and all packages used to perform this build (with the
exact version number). This is signed by reproducers to attest what their build
result was. (This alone does not indicate if it is reproducible, as every repeated
build result could be different.)

A Debian binary package containes the name and version of the source used. A
[Debian package repository contains a file with the hashes, names and versions
of included
packages](https://wiki.debian.org/DebianRepository/Format#A.22Packages.22_Indices).
[Each update of the Debian repositories is
archived](https://snapshot.debian.org/) to be able to reproduce package builds.

This ensures that from the artifact itself, or the hash of an artifact, or the
repository identifier and name and version of a binary, or the Buildinfo file
one can find the exact source code and and build environment that was used for
building it.

Distributions that are built in a rolling way are more complicated to
reproduce, than a distribution that is built only with the package versions
that it itself contains. For the later the build dependencies can just be
installed from itself. That it is self contained (though with self-referential
dependency loops) and reproducible can be ensured by rebuilding again after
bootsrapping from outside binaries (and only keeping the rebuilt binaries). But
for rolling distributions the dependencies are in some past version of it, but
not necessarly in the current one. So to install the build dependencies one
needs to either search an archive of them for which repo version included all
their versions or install the neceassry binaries directly or assemble a new
package repo from just these versions taken from the archive.
