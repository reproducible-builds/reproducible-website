---
title: Academic publications
layout: docs
permalink: /docs/publications/
---

### Citing reproducible-builds.org

The
[CITATION.cff](https://salsa.debian.org/reproducible-builds/reproducible-website/-/blob/master/CITATION.cff)
file is available at the root of the repository. It can be used to
generate citations in various formats using
[`cffconvert`](https://github.com/citation-file-format/cffconvert).

If you are preparing a paper or article and wish to reference the
[reproducible-builds.org](https://reproducible-builds.org) project, the
following BibTeX entry is recommended:

{% raw %}

    @misc{ReproducibleBuildsOrg,
      author = {{Reproducible Builds}},
      title = {Reproducible Builds Website},
      url = {https://reproducible-builds.org/}
    }

{% endraw %}

### Academic publications

In addition to the resources mentioned, our repository also includes a
[bibliography.bib](https://salsa.debian.org/reproducible-builds/reproducible-website/-/blob/master/bibliography.bib)
file, which contains BibTeX entries for all the academic publications listed
here. This file is continuously updated to reflect the most recent scholarly
works related to reproducible builds. It serves as a comprehensive source for
researchers and practitioners looking to cite relevant literature in their work.
The file can be found within the repository, making it easy for anyone to access
and utilize in their own scholarly writings.

{% for pub in site.data.publications %}
{{ pub.title }}
 : {% if pub.source %}_{{pub.source}}_<br>{% endif %}
   {{ pub.authors }} ({{ pub.date }})<br><{{ pub.url }}>{:target="_blank"}
{% endfor %}
