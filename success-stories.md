---
layout: default
title: Success stories
permalink: /success-stories/
order: 26
---

<h1>Success stories</h1>

This page highlights the success stories of Reproducible Builds, showcasing real-world examples of projects shipping with verifiable, reproducible builds. These stories aim to enhance the technical resilience of the initiative by encouraging community involvement and inspiring new contributions.

Please note this list includes projects with both 100% reproducible and partially reproducible builds.

<h2>Featured success stories</h2>

{% for section in site.data.success_stories %}
  <h2>{{ section.year }}</h2>
  {% if section.featured == true %}
  <div class="row">
  {% for item in section.success_stories %}
  <div class="d-flex align-items-stretch col-sm-4">
    <div class="card">
      {% if item.slides == true %}
      <div class="card-img-top">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="{{ item.url }}" allowfullscreen></iframe>
        </div>
      </div>
      {% else %}
        <a href="{{ item.url }}" class="flex-style">
          <img class="feature-image card-img-top width-style"
            src="{{ "/images/success-stories/" | relative_url }}{{ item.image }}"
          >
        </a>
      {% endif %}
      <div class="card-body">
        <h3 class="card-title">{{ item.title }}</h3>
        <p class="card-text">{{ item.summary }}</p>
        {% if item.slides == true %}
        <a class="btn btn-sm btn-outline-primary mt-2" href="{{ item.url }}">Slides</a>
        {% endif %}
        <a class="btn btn-sm btn-outline-primary mt-2" href="{{ item.blog | relative_url }}">Read more</a>
      </div>
    </div>
  </div>
  {% endfor %}
  </div>

  <h2>Other success stories</h2>
  {% else %}
  <div class="container">
  {% for item in section.success_stories %}
    <div class="row mb-5">
      {% if item.slides == true %}
      <div class="col-sm d-flex justify-content-end">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="{{ item.url }}" allowfullscreen></iframe>
        </div>
      </div>
      {% else %}
      <div class="col-sm d-flex justify-content-end">
        <a href="{{ item.url }}" >
          <img src="{{ "/images/success-stories/" | relative_url }}{{ item.image }}">
        </a>
      </div>
      {% endif %}
      <div class="col-sm">
        <h3 class="mt-0">{{ item.title }}</h3>
        {{ item.summary }}
        <br>
        {% if item.slides == true %}
        <a class="btn btn-sm btn-outline-primary mt-2" href="{{ item.url }}">Slides</a>
        {% endif %}
        <a class="btn btn-sm btn-outline-primary mt-2" href="{{ item.blog | relative_url }}">Read more</a>
      </div>
    </div>
  {% endfor %}
  </div>
  {% endif %}
{% endfor %}

<style>
  .flex-style {
    display: flex;
    justify-content: center;
    height: 4rem;
    margin-top: 3rem;
  }
  .width-style {
    width: auto;
  }
</style>
