LANGUAGES := $(shell ruby -ryaml -e "data = YAML::load(open('_config.yml')); puts data['languages']")

all:
	./bin/i18n.sh
	./bin/contributors.py
ifeq ($(CI_JOB_ID),)
	jekyll build --verbose --trace
else
	sed -i -e 's@^\(url:\).*@\1 https://$(CI_PROJECT_NAMESPACE).pages.debian.net@g' _config.yml
	jekyll build --verbose --trace --baseurl="/-/$(CI_PROJECT_NAME)/-/jobs/$(CI_JOB_ID)/artifacts/_site/"
endif

clean:
	rm -rf _site
	for lang in $(LANGUAGES); do \
	    rm -f $$lang/*.md $$lang/contribute/*.md _docs/$$lang/*.md; \
	    rmdir $$lang/contribute $$lang _docs/$$lang; \
	done

lint:
	@find \
		-type f \
		\( -name "*.md" -or -name "*.html" \) \
		-not -wholename '*/_site/*' \
		-print0 | xargs -0r grep -rl "href=[\"']/" | while read X; do \
		echo "W: $$X is using URIs that are not using '{{ \"/foo\" | relative_url }}'"; \
	done
