---
bibliography: ../../bibliography.bib
nocite: '@*'
csl: apa-cv.csl
---

### Citing reproducible-builds.org

The
[CITATION.cff](https://salsa.debian.org/reproducible-builds/reproducible-website/-/blob/master/CITATION.cff)
file is available at the root of the repository. It can be used to
generate citations in various formats using
[`cffconvert`](https://github.com/citation-file-format/cffconvert).

If you are preparing a paper or article and wish to reference the
[reproducible-builds.org](https://reproducible-builds.org) project, the
following BibTeX entry is recommended:

{% raw %}

    @misc{ReproducibleBuildsOrg,
      author = {{Reproducible Builds}},
      title = {Reproducible Builds Website},
      url = {https://reproducible-builds.org/}
    }

{% endraw %}

### Academic publications

In addition to the resources mentioned, our repository also includes a
[bibliography.bib](https://salsa.debian.org/reproducible-builds/reproducible-website/-/blob/master/bibliography.bib)
file, which contains BibTeX entries for all the academic publications listed
here. This file is continuously updated to reflect the most recent scholarly
works related to reproducible builds. It serves as a comprehensive source for
researchers and practitioners looking to cite relevant literature in their work.
The file can be found within the repository, making it easy for anyone to access
and utilize in their own scholarly writings.

- [@Thompson84]
- [@wheeler2010fully]
- [@courts2013functional]
- [@courtes:hal-01161771]
- [@Ren_2018]
- [@8935014]
- [@10.5555/3361338.3361435]
- [@Ohm_2020]
- [@10.1145/3373376.3378519]
- [@10.1145/3407023.3409183]
- [@Akhlaghi_2021]
- [@9403390]
- [@9465650]
- [@10.1145/3510003.3510102]
- [@9740718]
- [@strangfeld_2022]
- [@Butler2023]
- [@10179320]
- [@schorlemmer2024signing]
- [@malka2024reproducibility]
- [@randrianaina:hal-04441579]
- [@dellaiera_2024_12666898]
