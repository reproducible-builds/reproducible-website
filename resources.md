---
layout: default
title: Talks
permalink: /resources/
order: 50
---

# Talks & Resources

## Slides

We store all of our presentations in [PDF or HTML form]([/_lfs/presentations](https://reproducible-builds.org/_lfs/presentations)).
We also have [the original sources available in Git](https://salsa.debian.org/reproducible-builds/reproducible-presentations).


## Talks

{% for x in site.data.presentations %}

{% assign slug = x.title | append: "-" | append: x.event.date | slugify %}

<article>
<h3 id="{{ slug }}">{{ x.title }}</h3>

<a class="font-weight-bold text-secondary" href="{{ x.event.url }}">{{ x.event.name }} • {{ x.event.location }}</a>

<p class="my-1">by {{ x.presented_by }}
{% if x.event.date %}
 on {{ x.event.date }}
{% endif %}
</p>


<nav class="nav">
{% if x.video.youtube %}
<a class="nav-link pl-0" href="https://www.youtube.com/watch?v={{ x.video.youtube }}">YouTube</a>
{% endif %}

{% if x.video.url %}
<a class="nav-link pl-0" href="{{ x.video.url }}">Video</a>
{% endif %}

{% if x.video.subtitles %}
<a class="nav-link pl-0" href="{{ x.video.subtitles }}">Subtitles</a>
{% endif %}

{% if x.slides %}
<a class="nav-link pl-0" href="{{ x.slides }}">Slides</a>
{% endif %}
</nav>

</article>
{% endfor %}
