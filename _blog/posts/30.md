---
layout: blog
week: 30
published: 2015-11-23 17:43:07
---

What happened in the [reproducible
builds](https://wiki.debian.org/ReproducibleBuilds) effort this week:

## Toolchain fixes

  * Markus Koschany uploaded [antlr3](https://tracker.debian.org/antlr3)/3.5.2-3 which includes a fix by Emmanuel Bourg to make the generated parser reproducible.
  * Markus Koschany uploaded [maven-bundle-plugin](https://tracker.debian.org/maven-bundle-plugin)/2.4.0-2 which includes a fix by Emmanuel Bourg to use the date in the `DEB_CHANGELOG_DATETIME` variable in the `pom.properties` file embedded in the jar files.
  * Niels Thykier uploaded [debhelper](https://tracker.debian.org/debhelper)/9.20151116 which makes the timestamp of directories created by `dh_install`, `dh_installdocs`, and `dh_installexamples` reproducible. [Patch](https://bugs.debian.org/802005) by Niko Tyni.

Mattia Rizzolo uploaded a version of [perl](https://tracker.debian.org/perl)
to the [“reproducible”
repository](https://wiki.debian.org/ReprodcibleBuilds/ExperimentalToolchain)
including the patch written by Niko Tyni to add [support for SOURCE_DATE_EPOCH
in Pod::Man](https://bugs.debian.org/801621).

Dhole sent an [updated version](https://gcc.gnu.org/ml/gcc-
patches/2015-11/msg01890.html) of his patch adding support for
[SOURCE_DATE_EPOCH](https://wiki.debian.org/ReproducibleBuilds/TimestampsProposal)
in GCC to the upstream mailing list. Several comments have been made in
response which have been quickly addressed by Dhole.

Dhole also [forwarded](https://bugzilla.gnome.org/show_bug.cgi?id=758148) his
patch [adding support for `SOURCE_DATE_EPOCH` in
libxslt](https://bugs.debian.org/791815) upstream.

## Packages fixed

The following packages have become reproducible due to changes in their build
dependencies: [antlr3](https://tracker.debian.org/antlr3)/3.5.2-3,
[clusterssh](https://tracker.debian.org/clusterssh),
[cme](https://tracker.debian.org/cme), [libdatetime-set-
perl](https://tracker.debian.org/libdatetime-set-perl), [libgraphviz-
perl](https://tracker.debian.org/libgraphviz-perl), [liblingua-translit-
perl](https://tracker.debian.org/liblingua-translit-perl), [libparse-cpan-
packages-perl](https://tracker.debian.org/libparse-cpan-packages-perl),
[libsgmls-perl](https://tracker.debian.org/libsgmls-perl), [license-
reconcile](https://tracker.debian.org/license-reconcile), [maven-bundle-
plugin](https://tracker.debian.org/maven-bundle-plugin)/2.4.0-2,
[siggen](https://tracker.debian.org/siggen),
[stunnel4](https://tracker.debian.org/stunnel4),
[systemd](https://tracker.debian.org/systemd), [x11proto-
kb](https://tracker.debian.org/x11proto-kb).

The following packages became reproducible after getting fixed:

  * [bindex](https://tracker.debian.org/bindex)/2.2+svn101-3 by Markus Koschany.
  * [glyr](https://tracker.debian.org/glyr)/1.0.8-2 by Etienne Millon.
  * [jenkins-json](https://tracker.debian.org/jenkins-json)/2.4-jenkins-3-4 by Emmanuel Bourg.
  * [pkg-config](https://tracker.debian.org/pkg-config)/0.29-1 uploaded by Tollef Fog Heen, [original patch](https://bugs.debian.org/792821) by Juan Picca.
  * [plexus-containers1.5](https://tracker.debian.org/plexus-containers1.5)/1.6-1 by Emmanuel Bourg.
  * [polyglot-maven](https://tracker.debian.org/polyglot-maven)/0.8~tobrien+git20120905-5 by Emmanuel Bourg.
  * [sigil](https://tracker.debian.org/sigil)/0.9.0+dfsg-3 uploaded by Mattia Rizzolo, [original patch](https://bugs.debian.org/805500) by Reiner Herrmann.
  * [simutrans](https://tracker.debian.org/simutrans)/120.1.1+repack-2 uploaded by Jörg Frings-Fürst, fix by Markus Koschany.
  * [torrus](https://tracker.debian.org/torrus)/2.08-4 by Bernhard Schmidt.
  * [trigger-rally-data](https://tracker.debian.org/trigger-rally-data)/0.6.1-2 uploaded by Bertrand Marc, [patch](https://bugs.debian.org/789308) by Mattia Rizzolo.

Some uploads fixed some reproducibility issues, but not all of them:

  * [castle-game-engine](https://tracker.debian.org/castle-game-engine)/5.2.0-1 by Paul Gevers.
  * [libam7xxx](https://tracker.debian.org/libam7xxx)/0.1.6-2 by Antonio Ospite.
  * [libpdfbox-java](https://tracker.debian.org/libpdfbox-java)/1:1.8.10-1 by Emmanuel Bourg.
  * [xfaces](https://tracker.debian.org/xfaces)/3.3-29 uploaded by Hakan Ardo, [original patch](https://bugs.debian.org/777063) by Chris Lamb.

## reproducible.debian.net

Vagrant Cascadian has [set up a new `armhf` node using a Raspberry Pi
2](https://lists.alioth.debian.org/pipermail/reproducible-builds/Week-of-
Mon-20151116/003943.html). It should soon be added to the Jenkins
infrastructure.

## diffoscope development

diffoscope [version 42](https://tracker.debian.org/news/727245) was release on
November 20th. It adds a missing dependency on [python3-pkg-
resources](https://tracker.debian.org/python-setuptools) and to prevent
similar regression another
[autopkgtest](https://people.debian.org/~mpitt/autopkgtest/README.package-
tests.html) to ensure that the command line is functional when _Recommends_
are not installed. Two more encoding related problems have been fixed
([#804061](https://bugs.debian.org/804061),
[#805418](https://bugs.debian.org/805418)). A missing _Build-Depends_ has also
been added on [binutils-multiarch](https://tracker.debian.org/binutils) to
make the test suite pass on architectures other than `amd64`.

## Package reviews

180 [reviews](https://reproducible.debian.net/unstable/amd64/index_notes.html)
have been removed, 268 added and 59 updated this week.

70 new “fail to build from source” bugs have been reported by Chris West,
Chris Lamb and Niko Tyni.

New issue this week:
[randomness_in_ocaml_preprocessed_files](https://reproducible.debian.net/issues/unstable/randomness_in_ocaml_preprocessed_files_issue.html).

## Misc.

Jim MacArthur started to work on [a system to rebuild and
compare](https://github.com/CodethinkLabs/debian-reproducible-helper) packages
built on [reproducible.debian.net](https://reproducible.debian.net/) using
`.buildinfo` and [snapshot.debian.org](https://snapshot.debian.org/).

On December 1-3rd 2015, a [meeting](https://reproducible-
builds.org/events/athens2015/) of about 40 participants from 18 different free
software projects will be held in Athens, Greece with the intent of improving
the collaboration between projects, helping new efforts to be started, and
brainstorming on end-user aspects of reproducible builds.

