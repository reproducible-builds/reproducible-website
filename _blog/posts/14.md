---
layout: blog
week: 14
published: 2015-08-03 13:03:08
---

What happened in the [reproducible
builds](https://wiki.debian.org/ReproducibleBuilds) effort this week:

## Toolchain fixes

  * Joachim Breitner uploaded [haskell-devscripts](https://tracker.debian.org/haskell-devscripts)/0.9.11 which ads support for UTF-8 encoded `debian/control` file with all locales. [Original patch](https://bugs.debian.org/793944) by Chris Lamb.
  * Jonas Smedegaard uploaded [ghostscript](https://tracker.debian.org/9.16~dfsg-1) which adds support for [SOURCE_DATE_EPOCH](https://wiki.debian.org/ReproducibleBuilds/TimestampsProposal). [Original patch](https://bugs.debian.org/794004) by Dhole.

akira submitted a patch to make [cdbs](https://tracker.debian.org/cdbs) export
`SOURCE_DATE_EPOCH`. She uploded a package with the enhancement to the
[experimental “reproducible”
repository](https://wiki.debian.org/ReproducibleBuilds/ExperimentalToolchain).

## Packages fixed

The following 15 packages became reproducible due to changes in their build
dependencies: [dracut](https://tracker.debian.org/dracut), [editorconfig-
core](https://tracker.debian.org/editorconfig-core),
[elasticsearch](https://tracker.debian.org/elasticsearch),
[fish](https://tracker.debian.org/fish),
[libftdi1](https://tracker.debian.org/libftdi1),
[liblouisxml](https://tracker.debian.org/liblouisxml), [mk-
configure](https://tracker.debian.org/mk-configure),
[nanoc](https://tracker.debian.org/nanoc), [octave-
bim](https://tracker.debian.org/octave-bim), [octave-data-
smoothing](https://tracker.debian.org/octave-data-smoothing), [octave-
financial](https://tracker.debian.org/octave-financial), [octave-
ga](https://tracker.debian.org/octave-ga), [octave-missing-
functions](https://tracker.debian.org/octave-missing-functions), [octave-
secs1d](https://tracker.debian.org/octave-secs1d), [octave-
splines](https://tracker.debian.org/octave-splines),
[valgrind](https://tracker.debian.org/valgrind).

The following packages became reproducible after getting fixed:

  * [ant-contrib](https://tracker.debian.org/ant-contrib)/1.0~b3+svn177-7 by Emmanuel Bourg.
  * [authbind](https://tracker.debian.org/authbind)/2.1.1+nmu1 uploaded by Johannes Schauer, [original patch](https://bugs.debian.org/792945) by akira.
  * [elektra](https://tracker.debian.org/elektra)/0.8.12-1 uploaded by Pino Toscano, [original patch](https://bugs.debian.org/788879) by akira.
  * [glance](https://tracker.debian.org/glance)/2015.1.0-3 by Thomas Goirand.
  * [gnumeric](https://tracker.debian.org/gnumeric)/1.12.23-1 uploaded by Dmitry Smirnov, [original patch](https://bugs.debian.org/784672) by Daniel Kahn Gillmor.
  * [libdevice-cdio-perl](https://tracker.debian.org/libdevice-cdio-perl)/0.3.0-5 uploaded by gregor herrmann, [report](https://bugs.debian.org/793680) by Chris Lamb.
  * [libxray-scattering-perl](https://tracker.debian.org/libxray-scattering-perl)/3.0.1-1.1 uploaded by gregor herrmann, [original patch](https://bugs.debian.org/786634) by Reiner Herrmann.
  * [libxray-spacegroup-perl](https://tracker.debian.org/libxray-spacegroup-perl)/0.1.1-2.1 uploaded by gregor herrmann, [original patch](https://bugs.debian.org/790072) by Chris Lamb.
  * [ryu](https://tracker.debian.org/ryu)/3.23-2 by Dariusz Dwornikowski.

Some uploads fixed some reproducibility issues but not all of them:

  * [apophenia](https://tracker.debian.org/apophenia)/0.999e+ds-1 uploaded by Jerome Benoit, [original patch](https://bugs.debian.org/788868) by akira.
  * [xbs](https://tracker.debian.org/xbs)/0-10 uploaded by Matthew Vernon, [original patch](https://bugs.debian.org/792988) by Colin Watson.
  * [bbswitch](https://tracker.debian.org/bbswitch)/0.8-2 uploaded by Vincent Cheng, [original patch](https://bugs.debian.org/778455) by Chris Lamb.
  * [mariadb-10.0](https://tracker.debian.org/mariadb-10.0)/10.0.20-3 by Otto Kekäläinen.
  * [icedove](https://tracker.debian.org/icedove)/38.0.1-1 uploaded by Christoph Goehre, improvements by Carsten Schoenert.
  * [apache2](https://tracker.debian.org/apache2)/2.4.16-1 uploaded by Stefan Fritsch, improvements by Jean-Michel Vourgère.

In contrib, Dmitry Smirnov improved [libdvd-
pkg](https://tracker.debian.org/libdvd-pkg) with 1.3.99-1-1.

Patches submitted which have not made their way to the archive yet:

  * [#793705](https://bugs.debian.org/793705) on [mime-support](https://tracker.debian.org/mime-support) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#793922](https://bugs.debian.org/793922) on [polymake](https://tracker.debian.org/polymake) by Chris Lamb: sort Perl hash keys.
  * [#793980](https://bugs.debian.org/793980) on [lsof](https://tracker.debian.org/lsof) by Valentin Lorentz: removes extra informations from the build system..
  * [#793996](https://bugs.debian.org/793996) on [at](https://tracker.debian.org/at) by Valentin Lorentz: use absolute values for chmod.
  * [#794005](https://bugs.debian.org/794005) on [python-dtcwt](https://tracker.debian.org/python-dtcwt) by Chris Lamb: use a constant seed for the random number generator.
  * [#794011](https://bugs.debian.org/794011) on [gzip](https://tracker.debian.org/gzip) by Valentin Lorentz: remove date from texinfo header.
  * [#794014](https://bugs.debian.org/794014) on [moin](https://tracker.debian.org/moin) by Dhole: normalizes the timezone and fixes timestamps from the files compressed with zip.
  * [#794106](https://bugs.debian.org/794106) on [cryptsetup](https://tracker.debian.org/cryptsetup) by Valentin Lorentz: use date from latest `debian/changelog` entry in manpage.
  * [#794130](https://bugs.debian.org/794130) on [coq](https://tracker.debian.org/coq) by Valentin Lorentz: use C locale and UTC timezone when generating the build date.
  * [#794225](https://bugs.debian.org/794225) on [libsyncml](https://tracker.debian.org/libsyncml) by akira: export `SOURCE_DATE_EPOCH`.
  * [#794239](https://bugs.debian.org/794239) on [ zipios++](https://tracker.debian.org/ zipios++) by akira: export `SOURCE_DATE_EPOCH`.
  * [#794247](https://bugs.debian.org/794247) on [whizzytex](https://tracker.debian.org/whizzytex) by Dhole: remove current date from documentation.
  * [#794248](https://bugs.debian.org/794248) on [cortado](https://tracker.debian.org/cortado) by Dhole: allow the build date to be set externally and use time from the latest `debian/changelog` entry.
  * [#794395](https://bugs.debian.org/794395) on [classified-ads](https://tracker.debian.org/classified-ads) by Reiner Herrmann: remove timestamps from PNG images.
  * [#794398](https://bugs.debian.org/794398) on [clhep](https://tracker.debian.org/clhep) by Reiner Herrmann: sort source file list.
  * [#794399](https://bugs.debian.org/794399) on [parsec47](https://tracker.debian.org/parsec47) by Reiner Herrmann: use C locale when sorting source files.
  * [#794400](https://bugs.debian.org/794400) on [tumiki-fighters](https://tracker.debian.org/tumiki-fighters) by Reiner Herrmann: use C locale when sorting source files.

## reproducible.debian.net

Four `armhf` build hosts were provided by Vagrant Cascadian and have been
configured to be used by [jenkins.debian.net](https://jenkins.debian.net/).
Work on including `armhf` builds in the
[reproducible.debian.net](https://reproducible.debian.net/) webpages has
begun. So far the repository comparison page just shows us which `armhf`
binary packages are currently missing in our repo. (h01ger)

The scheduler has been changed to re-schedule more packages from stretch than
sid, as the gcc5 transition has started… This mostly affects build log age.
(h01ger)

A new [depwait
status](https://reproducible.debian.net/unstable/amd64/index_depwait.html) has
been introduced for packages which can't be built because of missing build
dependencies. (Mattia Rizzolo)

## debbindiff development

Finally, on August 31st, Lunar released [debbindiff
27](https://tracker.debian.org/news/701661) containing a [complete overhaul of
the code for the comparison
stage](https://anonscm.debian.org/cgit/reproducible/debbindiff.git/commit/?id=5c02e00001e46706a7bf7c8faad353d17178a4d6).
The new architecture is more versatile and extensible while minimizing code
duplication. [libarchive](http://libarchive.org/) is now used to handle cpio
archives and iso9660 images through the newly packaged [python-
libarchive-c](https://tracker.debian.org/python-libarchive-c). This should
also help support a couple other archive formats in the future. Symlinks and
devices are now properly compared. Text files are compared as Unicode after
being decoded, and encoding differences are reported. Support for Sqlite3 and
Mono/.NET executables has been added. Thanks to Valentin Lorentz, the test
suite should now run on more systems. A [small defiency in
unquashfs](https://bugs.debian.org/794096) has been identified in the process.
A long standing optimization is now performed on Debian package: based on the
content of the `md5sums` control file, we skip comparing files with matching
hashes. This makes debbindiff usable on packages with many files. Fuzzy-
matching is now performed for files in the same container (like a tarball) to
handle renames. Also, for Debian `.changes`, listed files are now compared
without looking the embedded version number. This makes debbindiff a lot more
useful when comparing different versions of the same package.

Based on the rearchitecturing work has been done to allow parallel processing.
The
[branch](https://anonscm.debian.org/cgit/reproducible/debbindiff.git/log/?h=pu/parallel)
now seems to work most of the time. More test needs to be done before it can
be merged.

The current fuzzy-matching algorithm,
[ssdeep](http://ssdeep.sourceforge.net/), has showed disappointing results.
One important use case is being able to properly compare debug symbols. Their
path is made using the [Build
ID](https://fedoraproject.org/wiki/Releases/FeatureBuildId#Unique_build_ID).
As this identifier is made with a checksum of the binary content, finding
things like [CPP
macros](https://reproducible.debian.net/issues/unstable/timestamps_from_cpp_macros_issue.html)
is much easier when a diff of the debug symbols is available. Good news is
that [TLSH](https://github.com/trendmicro/tlsh), another fuzzy-matching
algorithm, has been tested with much better results. A package is [waiting in
NEW](https://ftp-master.debian.org/new/tlsh_3.2.1+20150727-1.html) and the
[code is
ready](https://anonscm.debian.org/cgit/reproducible/debbindiff.git/log/?h=pu/tlsh)
for it to become available.

A follow-up [release 28](https://tracker.debian.org/news/702053) was made on
August 2nd fixing content label used for gzip2, bzip2 and xz files and an
error on text files only differing in their encoding. It also contains a small
code improvement on how comments on `Difference` object are handled.

This is the last release name `debbindiff`. A new name has been chosen to
better reflect that it is not a Debian specific tool. Stay tuned!

## Documentation update

Valentin Lorentz updated the [patch submission
template](https://wiki.debian.org/ReproducibleBuilds/Contribute#How_to_report_bugs)
to suggest to write the kind of issue in the bug subject.

Small progress have been made on the [Reproducible Builds
HOWTO](https://anonscm.debian.org/cgit/reproducible/reproducible-builds-
howto.git/) while preparing the related [CCCamp15
talk](https://events.ccc.de/camp/2015/Fahrplan/events/6657.html).

## Package reviews

235 obsolete
[reviews](https://reproducible.debian.net/unstable/amd64/index_notes.html)
have been removed, 47 added and 113 updated this week.

42 reports for packages failing to build from source have been made by Chris
West (Faux).

New issue added this week:
[haskell_devscripts_locale_substvars](https://reproducible.debian.net/issues/unstable/haskell_devscripts_locale_substvars_issue.html).

## Misc.

Valentin Lorentz wrote a [script to report packages tested as
unreproducible](https://anonscm.debian.org/cgit/reproducible/misc.git/tree/unreproducible-
installed) installed on a system. We encourage everyone to run it on their
systems and give feedback!

