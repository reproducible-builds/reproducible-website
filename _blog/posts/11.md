---
layout: blog
week: 11
published: 2015-07-13 12:23:09
---

> Debian is undertaking a huge effort to develop a reproducible builds system.
> I'd like to thank you for that. This could be Debian's most important
> project, with how badly computer security has been going.

— PerniciousPunk in [Reddit's Ask me anything! to Neil McGovern,
DPL](https://www.reddit.com/r/linux/comments/3ctb6n/im_neil_mcgovern_debian_project_leader_ask_me/csyv4j5).

What happened in the [reproducible
builds](https://wiki.debian.org/ReproducibleBuilds) effort this week:

## Toolchain fixes

More tools are getting patched to use the value of the [SOURCE_DATE_EPOCH
environment
variable](https://wiki.debian.org/ReproducibleBuilds/TimestampsProposal) as
the current time:

  * [libxslt](https://tracker.debian.org/libxslt) in [#791815](https://bugs.debian.org/791815) (Dhole),
  * [texlive-bin](https://tracker.debian.org/texlive-bin) via [upstream](https://www.tug.org/pipermail/pdftex/2015-July/008952.html) ([#792202](https://bugs.debian.org/792202)) (akira),
  * [sphinx](https://tracker.debian.org/sphinx) via [upstream](https://github.com/sphinx-doc/sphinx/pull/1954) (Dmitry Shachnev).

In the [“reproducible” experimental
toolchain](https://wiki.debian.org/ReproducibleBuilds/ExperimentalToolchain)
which have been uploaded:

  * [doxygen](https://tracker.debian.org/doxygen) to [support SOURCE_DATE_EPOCH](https://bugs.debian.org/792201) (akira),
  * [debhelper](https://tracker.debian.org/debhelper) now set `SOURCE_DATE_EPOCH` to the time of the latest `debian/changelog` entry when exporting build flags, patch sent as [#791823](https://bugs.debian.org/791823) (Dhole),
  * [epydoc](https://tracker.debian.org/epydoc) to [support SOURCE_DATE_EPOCH](https://bugs.debian.org/790899) (Reiner Herrmann),
  * `texlive-bin` (akira) and `libxslt` (Dhole) with the aforementioned support for `SOURCE_DATE_EPOCH`.

Johannes Schauer followed up on [making sbuild build path
deterministic](https://bugs.debian.org/778571) with several ideas.

## Packages fixed

The following 311 packages became reproducible due to changes in their build
dependencies : [4ti2](https://tracker.debian.org/4ti2),
[alot](https://tracker.debian.org/alot),
[angband](https://tracker.debian.org/angband), [appstream-
glib](https://tracker.debian.org/appstream-glib),
[argvalidate](https://tracker.debian.org/argvalidate), [armada-
backlight](https://tracker.debian.org/armada-backlight),
[ascii](https://tracker.debian.org/ascii),
[ask](https://tracker.debian.org/ask),
[astroquery](https://tracker.debian.org/astroquery),
[atheist](https://tracker.debian.org/atheist),
[aubio](https://tracker.debian.org/aubio),
[autorevision](https://tracker.debian.org/autorevision), [awesome-
extra](https://tracker.debian.org/awesome-extra),
[bibtool](https://tracker.debian.org/bibtool), [boot-info-
script](https://tracker.debian.org/boot-info-script),
[bpython](https://tracker.debian.org/bpython),
[brian](https://tracker.debian.org/brian), [btrfs-
tools](https://tracker.debian.org/btrfs-tools), [bugs-
everywhere](https://tracker.debian.org/bugs-everywhere),
[capnproto](https://tracker.debian.org/capnproto),
[cbm](https://tracker.debian.org/cbm),
[ccfits](https://tracker.debian.org/ccfits),
[cddlib](https://tracker.debian.org/cddlib),
[cflow](https://tracker.debian.org/cflow),
[cfourcc](https://tracker.debian.org/cfourcc),
[cgit](https://tracker.debian.org/cgit),
[chaussette](https://tracker.debian.org/chaussette), [checkbox-
ng](https://tracker.debian.org/checkbox-ng), [cinnamon-settings-
daemon](https://tracker.debian.org/cinnamon-settings-daemon),
[clfswm](https://tracker.debian.org/clfswm),
[clipper](https://tracker.debian.org/clipper),
[compton](https://tracker.debian.org/compton),
[cppcheck](https://tracker.debian.org/cppcheck),
[crmsh](https://tracker.debian.org/crmsh),
[cupt](https://tracker.debian.org/cupt),
[cutechess](https://tracker.debian.org/cutechess),
[d-itg](https://tracker.debian.org/d-itg), [dahdi-
tools](https://tracker.debian.org/dahdi-tools),
[dapl](https://tracker.debian.org/dapl),
[darnwdl](https://tracker.debian.org/darnwdl),
[dbusada](https://tracker.debian.org/dbusada), [debian-security-
support](https://tracker.debian.org/debian-security-support),
[debomatic](https://tracker.debian.org/debomatic),
[dime](https://tracker.debian.org/dime),
[dipy](https://tracker.debian.org/dipy),
[dnsruby](https://tracker.debian.org/dnsruby),
[doctrine](https://tracker.debian.org/doctrine),
[drmips](https://tracker.debian.org/drmips), [dsc-
statistics](https://tracker.debian.org/dsc-statistics), [dune-
common](https://tracker.debian.org/dune-common), [dune-
istl](https://tracker.debian.org/dune-istl), [dune-
localfunctions](https://tracker.debian.org/dune-localfunctions),
[easytag](https://tracker.debian.org/easytag),
[ent](https://tracker.debian.org/ent), [epr-
api](https://tracker.debian.org/epr-api),
[esajpip](https://tracker.debian.org/esajpip),
[eyed3](https://tracker.debian.org/eyed3),
[fastjet](https://tracker.debian.org/fastjet),
[fatresize](https://tracker.debian.org/fatresize), [fflas-
ffpack](https://tracker.debian.org/fflas-ffpack),
[flann](https://tracker.debian.org/flann),
[flex](https://tracker.debian.org/flex),
[flint](https://tracker.debian.org/flint),
[fltk1.3](https://tracker.debian.org/fltk1.3), [fonts-
dustin](https://tracker.debian.org/fonts-dustin), [fonts-
play](https://tracker.debian.org/fonts-play), [fonts-
uralic](https://tracker.debian.org/fonts-uralic),
[freecontact](https://tracker.debian.org/freecontact),
[freedoom](https://tracker.debian.org/freedoom), [gap-
guava](https://tracker.debian.org/gap-guava), [gap-
scscp](https://tracker.debian.org/gap-scscp),
[genometools](https://tracker.debian.org/genometools),
[geogebra](https://tracker.debian.org/geogebra), [git-
reintegrate](https://tracker.debian.org/git-reintegrate), [git-remote-
bzr](https://tracker.debian.org/git-remote-bzr), [git-remote-
hg](https://tracker.debian.org/git-remote-hg),
[gitmagic](https://tracker.debian.org/gitmagic),
[givaro](https://tracker.debian.org/givaro),
[gnash](https://tracker.debian.org/gnash),
[gocr](https://tracker.debian.org/gocr),
[gorm.app](https://tracker.debian.org/gorm.app),
[gprbuild](https://tracker.debian.org/gprbuild),
[grapefruit](https://tracker.debian.org/grapefruit),
[greed](https://tracker.debian.org/greed),
[gtkspellmm](https://tracker.debian.org/gtkspellmm),
[gummiboot](https://tracker.debian.org/gummiboot),
[gyp](https://tracker.debian.org/gyp), [heat-
cfntools](https://tracker.debian.org/heat-cfntools),
[herold](https://tracker.debian.org/herold),
[htp](https://tracker.debian.org/htp),
[httpfs2](https://tracker.debian.org/httpfs2),
[i3status](https://tracker.debian.org/i3status),
[imagetooth](https://tracker.debian.org/imagetooth),
[imapcopy](https://tracker.debian.org/imapcopy),
[imaprowl](https://tracker.debian.org/imaprowl),
[irker](https://tracker.debian.org/irker),
[jansson](https://tracker.debian.org/jansson),
[jmapviewer](https://tracker.debian.org/jmapviewer), [jsdoc-
toolkit](https://tracker.debian.org/jsdoc-toolkit),
[jwm](https://tracker.debian.org/jwm),
[katarakt](https://tracker.debian.org/katarakt), [khronos-opencl-
man](https://tracker.debian.org/khronos-opencl-man), [khronos-opengl-
man4](https://tracker.debian.org/khronos-opengl-man4), [lastpass-
cli](https://tracker.debian.org/lastpass-cli), [lava-
coordinator](https://tracker.debian.org/lava-coordinator), [lava-
tool](https://tracker.debian.org/lava-tool),
[lavapdu](https://tracker.debian.org/lavapdu),
[letterize](https://tracker.debian.org/letterize),
[lhapdf](https://tracker.debian.org/lhapdf),
[libam7xxx](https://tracker.debian.org/libam7xxx),
[libburn](https://tracker.debian.org/libburn),
[libccrtp](https://tracker.debian.org/libccrtp),
[libclaw](https://tracker.debian.org/libclaw),
[libcommoncpp2](https://tracker.debian.org/libcommoncpp2),
[libdaemon](https://tracker.debian.org/libdaemon), [libdbusmenu-
qt](https://tracker.debian.org/libdbusmenu-qt),
[libdc0](https://tracker.debian.org/libdc0),
[libevhtp](https://tracker.debian.org/libevhtp),
[libexosip2](https://tracker.debian.org/libexosip2),
[libfreenect](https://tracker.debian.org/libfreenect),
[libgwenhywfar](https://tracker.debian.org/libgwenhywfar),
[libhmsbeagle](https://tracker.debian.org/libhmsbeagle),
[libitpp](https://tracker.debian.org/libitpp),
[libldm](https://tracker.debian.org/libldm),
[libmodbus](https://tracker.debian.org/libmodbus),
[libmtp](https://tracker.debian.org/libmtp),
[libmwaw](https://tracker.debian.org/libmwaw),
[libnfo](https://tracker.debian.org/libnfo), [libpam-
abl](https://tracker.debian.org/libpam-abl),
[libphysfs](https://tracker.debian.org/libphysfs),
[libplayer](https://tracker.debian.org/libplayer),
[libqb](https://tracker.debian.org/libqb),
[libsecret](https://tracker.debian.org/libsecret),
[libserial](https://tracker.debian.org/libserial),
[libsidplayfp](https://tracker.debian.org/libsidplayfp),
[libtime-y2038-perl](https://tracker.debian.org/libtime-y2038-perl),
[libxr](https://tracker.debian.org/libxr),
[lift](https://tracker.debian.org/lift),
[linbox](https://tracker.debian.org/linbox),
[linthesia](https://tracker.debian.org/linthesia),
[livestreamer](https://tracker.debian.org/livestreamer),
[lizardfs](https://tracker.debian.org/lizardfs),
[lmdb](https://tracker.debian.org/lmdb),
[log4c](https://tracker.debian.org/log4c),
[logbook](https://tracker.debian.org/logbook),
[lrslib](https://tracker.debian.org/lrslib),
[lvtk](https://tracker.debian.org/lvtk),
[m-tx](https://tracker.debian.org/m-tx), [mailman-
api](https://tracker.debian.org/mailman-api),
[matroxset](https://tracker.debian.org/matroxset),
[miniupnpd](https://tracker.debian.org/miniupnpd),
[mknbi](https://tracker.debian.org/mknbi),
[monkeysign](https://tracker.debian.org/monkeysign),
[mpi4py](https://tracker.debian.org/mpi4py),
[mpmath](https://tracker.debian.org/mpmath),
[mpqc](https://tracker.debian.org/mpqc), [mpris-
remote](https://tracker.debian.org/mpris-remote),
[musicbrainzngs](https://tracker.debian.org/musicbrainzngs), [network-
manager](https://tracker.debian.org/network-manager),
[nifticlib](https://tracker.debian.org/nifticlib),
[obfsproxy](https://tracker.debian.org/obfsproxy),
[ogre-1.9](https://tracker.debian.org/ogre-1.9),
[opal](https://tracker.debian.org/opal),
[openchange](https://tracker.debian.org/openchange),
[opensc](https://tracker.debian.org/opensc), [packaging-
tutorial](https://tracker.debian.org/packaging-tutorial),
[padevchooser](https://tracker.debian.org/padevchooser),
[pajeng](https://tracker.debian.org/pajeng),
[paprefs](https://tracker.debian.org/paprefs),
[pavumeter](https://tracker.debian.org/pavumeter),
[pcl](https://tracker.debian.org/pcl),
[pdmenu](https://tracker.debian.org/pdmenu),
[pepper](https://tracker.debian.org/pepper),
[perroquet](https://tracker.debian.org/perroquet),
[pgrouting](https://tracker.debian.org/pgrouting),
[pixz](https://tracker.debian.org/pixz),
[pngcheck](https://tracker.debian.org/pngcheck),
[po4a](https://tracker.debian.org/po4a),
[powerline](https://tracker.debian.org/powerline),
[probabel](https://tracker.debian.org/probabel), [profitbricks-
client](https://tracker.debian.org/profitbricks-client),
[prosody](https://tracker.debian.org/prosody),
[pstreams](https://tracker.debian.org/pstreams),
[pyacidobasic](https://tracker.debian.org/pyacidobasic),
[pyepr](https://tracker.debian.org/pyepr),
[pymilter](https://tracker.debian.org/pymilter),
[pytest](https://tracker.debian.org/pytest), [python-
amqp](https://tracker.debian.org/python-amqp), [python-
apt](https://tracker.debian.org/python-apt), [python-
carrot](https://tracker.debian.org/python-carrot), [python-
django](https://tracker.debian.org/python-django), [python-
ethtool](https://tracker.debian.org/python-ethtool), [python-
mock](https://tracker.debian.org/python-mock), [python-
odf](https://tracker.debian.org/python-odf), [python-
pathtools](https://tracker.debian.org/python-pathtools), [python-
pskc](https://tracker.debian.org/python-pskc), [python-
psutil](https://tracker.debian.org/python-psutil), [python-
pypump](https://tracker.debian.org/python-pypump), [python-
repoze.tm2](https://tracker.debian.org/python-repoze.tm2), [python-
repoze.what](https://tracker.debian.org/python-repoze.what),
[qdjango](https://tracker.debian.org/qdjango), [qpid-
proton](https://tracker.debian.org/qpid-proton),
[qsapecng](https://tracker.debian.org/qsapecng),
[radare2](https://tracker.debian.org/radare2),
[reclass](https://tracker.debian.org/reclass),
[repsnapper](https://tracker.debian.org/repsnapper), [resource-
agents](https://tracker.debian.org/resource-agents),
[rgain](https://tracker.debian.org/rgain),
[rttool](https://tracker.debian.org/rttool), [ruby-
aggregate](https://tracker.debian.org/ruby-aggregate), [ruby-
albino](https://tracker.debian.org/ruby-albino), [ruby-archive-tar-
minitar](https://tracker.debian.org/ruby-archive-tar-minitar), [ruby-
bcat](https://tracker.debian.org/ruby-bcat), [ruby-
blankslate](https://tracker.debian.org/ruby-blankslate), [ruby-coffee-
script](https://tracker.debian.org/ruby-coffee-script), [ruby-
colored](https://tracker.debian.org/ruby-colored), [ruby-dbd-
mysql](https://tracker.debian.org/ruby-dbd-mysql), [ruby-dbd-
odbc](https://tracker.debian.org/ruby-dbd-odbc), [ruby-dbd-
pg](https://tracker.debian.org/ruby-dbd-pg), [ruby-dbd-
sqlite3](https://tracker.debian.org/ruby-dbd-sqlite3), [ruby-
dbi](https://tracker.debian.org/ruby-dbi), [ruby-dirty-
memoize](https://tracker.debian.org/ruby-dirty-memoize), [ruby-
encryptor](https://tracker.debian.org/ruby-encryptor), [ruby-
erubis](https://tracker.debian.org/ruby-erubis), [ruby-fast-
xs](https://tracker.debian.org/ruby-fast-xs), [ruby-
fusefs](https://tracker.debian.org/ruby-fusefs), [ruby-
gd](https://tracker.debian.org/ruby-gd), [ruby-
git](https://tracker.debian.org/ruby-git), [ruby-
globalhotkeys](https://tracker.debian.org/ruby-globalhotkeys), [ruby-
god](https://tracker.debian.org/ruby-god), [ruby-
hike](https://tracker.debian.org/ruby-hike), [ruby-
hmac](https://tracker.debian.org/ruby-hmac), [ruby-
integration](https://tracker.debian.org/ruby-integration), [ruby-jnunemaker-
matchy](https://tracker.debian.org/ruby-jnunemaker-matchy), [ruby-
memoize](https://tracker.debian.org/ruby-memoize), [ruby-merb-
core](https://tracker.debian.org/ruby-merb-core), [ruby-merb-
haml](https://tracker.debian.org/ruby-merb-haml), [ruby-merb-
helpers](https://tracker.debian.org/ruby-merb-helpers), [ruby-
metaid](https://tracker.debian.org/ruby-metaid), [ruby-
mina](https://tracker.debian.org/ruby-mina), [ruby-net-
irc](https://tracker.debian.org/ruby-net-irc), [ruby-net-
netrc](https://tracker.debian.org/ruby-net-netrc), [ruby-
odbc](https://tracker.debian.org/ruby-odbc), [ruby-
ole](https://tracker.debian.org/ruby-ole), [ruby-
packet](https://tracker.debian.org/ruby-packet), [ruby-
parseconfig](https://tracker.debian.org/ruby-parseconfig), [ruby-
platform](https://tracker.debian.org/ruby-platform), [ruby-
plist](https://tracker.debian.org/ruby-plist), [ruby-
popen4](https://tracker.debian.org/ruby-popen4), [ruby-
rchardet](https://tracker.debian.org/ruby-rchardet), [ruby-
romkan](https://tracker.debian.org/ruby-romkan), [ruby-
ronn](https://tracker.debian.org/ruby-ronn), [ruby-
rubyforge](https://tracker.debian.org/ruby-rubyforge), [ruby-
rubytorrent](https://tracker.debian.org/ruby-rubytorrent), [ruby-
samuel](https://tracker.debian.org/ruby-samuel), [ruby-shoulda-
matchers](https://tracker.debian.org/ruby-shoulda-matchers), [ruby-
sourcify](https://tracker.debian.org/ruby-sourcify), [ruby-test-
spec](https://tracker.debian.org/ruby-test-spec), [ruby-
validatable](https://tracker.debian.org/ruby-validatable), [ruby-
wirble](https://tracker.debian.org/ruby-wirble), [ruby-xml-
simple](https://tracker.debian.org/ruby-xml-simple), [ruby-
zoom](https://tracker.debian.org/ruby-zoom),
[rumor](https://tracker.debian.org/rumor), [rurple-
ng](https://tracker.debian.org/rurple-ng),
[ryu](https://tracker.debian.org/ryu),
[sam2p](https://tracker.debian.org/sam2p), [scikit-
learn](https://tracker.debian.org/scikit-learn),
[serd](https://tracker.debian.org/serd),
[shellex](https://tracker.debian.org/shellex), [shorewall-
doc](https://tracker.debian.org/shorewall-doc),
[shunit2](https://tracker.debian.org/shunit2),
[simbody](https://tracker.debian.org/simbody),
[simplejson](https://tracker.debian.org/simplejson),
[smcroute](https://tracker.debian.org/smcroute),
[soqt](https://tracker.debian.org/soqt),
[sord](https://tracker.debian.org/sord),
[spacezero](https://tracker.debian.org/spacezero), [spamassassin-
heatu](https://tracker.debian.org/spamassassin-heatu),
[spamprobe](https://tracker.debian.org/spamprobe), [sphinxcontrib-
youtube](https://tracker.debian.org/sphinxcontrib-youtube),
[splitpatch](https://tracker.debian.org/splitpatch),
[sratom](https://tracker.debian.org/sratom),
[stompserver](https://tracker.debian.org/stompserver),
[syncevolution](https://tracker.debian.org/syncevolution),
[tgt](https://tracker.debian.org/tgt),
[ticgit](https://tracker.debian.org/ticgit),
[tinyproxy](https://tracker.debian.org/tinyproxy),
[tor](https://tracker.debian.org/tor), [tox](https://tracker.debian.org/tox),
[transmissionrpc](https://tracker.debian.org/transmissionrpc),
[tweeper](https://tracker.debian.org/tweeper),
[udpcast](https://tracker.debian.org/udpcast), [units-
filter](https://tracker.debian.org/units-filter),
[viennacl](https://tracker.debian.org/viennacl),
[visp](https://tracker.debian.org/visp),
[vite](https://tracker.debian.org/vite), [vmfs-
tools](https://tracker.debian.org/vmfs-tools),
[waffle](https://tracker.debian.org/waffle),
[waitress](https://tracker.debian.org/waitress), [wavtool-
pl](https://tracker.debian.org/wavtool-pl),
[webkit2pdf](https://tracker.debian.org/webkit2pdf),
[wfmath](https://tracker.debian.org/wfmath),
[wit](https://tracker.debian.org/wit),
[wreport](https://tracker.debian.org/wreport), [x11proto-
input](https://tracker.debian.org/x11proto-input),
[xbae](https://tracker.debian.org/xbae), [xdg-
utils](https://tracker.debian.org/xdg-utils),
[xdotool](https://tracker.debian.org/xdotool),
[xsystem35](https://tracker.debian.org/xsystem35),
[yapsy](https://tracker.debian.org/yapsy),
[yaz](https://tracker.debian.org/yaz).

Please note that some packages in the above list are _falsely_ reproducible.
In the experimental toolchain, `debhelper` exported `TZ=UTC` and this made
packages capturing the current date (without the time) reproducible in the
current test environment.

The following packages became reproducible after getting fixed:

  * [389-admin](https://tracker.debian.org/389-admin)/1.1.42-1 uploaded by Timo Aaltonen, [original patch](https://bugs.debian.org/791922) by Chris Lamb.
  * [aroarfw](https://tracker.debian.org/aroarfw)/0.1~beta5-2 uploaded by Patrick Matthäi, [original patch](https://bugs.debian.org/779215) by akira.
  * [clfft](https://tracker.debian.org/clfft)/2.4-2 by Ghislain Antony Vaillant.
  * [custom-tab-width](https://tracker.debian.org/custom-tab-width)/1.0.1-3 by Daniel Kahn Gillmor.
  * [debirf](https://tracker.debian.org/debirf)/0.35 uploaded by Daniel Kahn Gillmor, [original patch](https://bugs.debian.org/777183) by Chris Lamb.
  * [enigmail](https://tracker.debian.org/enigmail)/2:1.8.2-3 by Daniel Kahn Gillmor.
  * [flashproxy](https://tracker.debian.org/flashproxy)/1.7-4 by Ximin Luo.
  * [getdns](https://tracker.debian.org/getdns)/0.2.0-2 by Daniel Kahn Gillmor.
  * [glfw3](https://tracker.debian.org/glfw3)/3.1.1-1 by James Cowgill, [reported](https://bugs.debian.org/790139) by akira.
  * [gpgme1.0](https://tracker.debian.org/gpgme1.0)/1.5.5-3 by Daniel Kahn Gillmor.
  * [jzmq](https://tracker.debian.org/jzmq)/3.1.0-4 by Jan Niehusmann.
  * [libcommons-cli-java](https://tracker.debian.org/libcommons-cli-java)/1.3.1-1 by tony mancill.
  * [liblo](https://tracker.debian.org/liblo)/0.28-5 uploaded by Felipe Sateler, [original patch](https://bugs.debian.org/789040) by akira.
  * [libmoe](https://tracker.debian.org/libmoe)/1.5.8-2 uploaded by TANIGUCHI Takaki, [original patch](https://bugs.debian.org/779167) by Chris Lamb.
  * [libnet-interface-perl](https://tracker.debian.org/libnet-interface-perl)/1.012-2 uploaded by gregor herrmann, [original patch](https://bugs.debian.org/790503) by Chris Lamb.
  * [libxmlenc-java](https://tracker.debian.org/libxmlenc-java)/0.52+dfsg-4 by Emmanuel Bourg.
  * [lintian](https://tracker.debian.org/lintian)/2.5.33 by Niels Thykier.
  * [litecoin](https://tracker.debian.org/litecoin)/0.10.2.2-1 uploaded by Dmitry Smirnov, [original patch](https://bugs.debian.org/778984) by Chris Lamb.
  * [node-ws](https://tracker.debian.org/node-ws)/0.7.2+ds1.349b7460-1 by Ximin Luo.
  * [pyevolve](https://tracker.debian.org/pyevolve)/0.6~rc1+svn398+dfsg-7 uploaded by Christian Kastner, [original patch](https://bugs.debian.org/788403) by Juan Picca.
  * [sendmail](https://tracker.debian.org/sendmail)/8.14.9-3 by Andreas Beckmann.
  * [uthash](https://tracker.debian.org/uthash)/1.9.9.1+git20150507-2 by Ilias Tsitsimpis, [report](https://bugs.debian.org/791856) by Jakub Wilk.

Ben Hutchings [upstreamed several patches](http://www.spinics.net/lists/linux-
doc/msg30579.html) to fix Linux reproducibility issues which were quickly
merged.

Some uploads fixed some reproducibility issues but not all of them:

  * [apt-dater](https://tracker.debian.org/apt-dater)/1.0.2-1 uploaded by Patrick Matthäi, [original patch](https://bugs.debian.org/789648) by Dhole, fixed upstream by Thomas Liske.
  * [argyll](https://tracker.debian.org/argyll)/1.7.0+repack-4 by Jörg Frings-Fürst.
  * [grads](https://tracker.debian.org/grads)/2:2.0.2-4 by Alastair McKinstry.
  * [kfreebsd-10](https://tracker.debian.org/kfreebsd-10) by Steven Chamberlain.
  * [mariadb-10.0](https://tracker.debian.org/mariadb-10.0)/10.0.20-2 by Otto Kekäläinen.
  * [xtel](https://tracker.debian.org/xtel)/3.3.0-18 uploaded by Samuel Thibault, [original patch](https://bugs.debian.org/789965) by Dhole.

Uploads that should fix packages not in _main_ :

  * [fglrx-driver](https://tracker.debian.org/fglrx-driver)/1:15.7-1 uploaded by Patrick Matthäi, fixed by Andreas Beckmann.
  * [pycuda](https://tracker.debian.org/pycuda)/2015.1.2-1 uploaded by Tomasz Rybak, patch by Juan Picca.

Patches submitted which have not made their way to the archive yet:

  * [#787675](https://bugs.debian.org/787675) on [ricochet](https://tracker.debian.org/ricochet) by Daniel Kahn Gillmor: use the `debian/changelog` date in the manpage.
  * [#791648](https://bugs.debian.org/791648) on [fish](https://tracker.debian.org/fish) by Chris Lamb: sort header files in documentation.
  * [#791691](https://bugs.debian.org/791691) on [fritzing](https://tracker.debian.org/fritzing) by Chris Lamb: sort the documentation author list using the C locale.
  * [#791834](https://bugs.debian.org/791834) on [bitcoin](https://tracker.debian.org/bitcoin) by Reiner Herrmann: sort sources files using the C locale.
  * [#791845](https://bugs.debian.org/791845) on [yacas](https://tracker.debian.org/yacas) by Reiner Herrmann: sort hints using the C locale.
  * [#791851](https://bugs.debian.org/791851) on [scowl](https://tracker.debian.org/scowl) by Reiner Herrmann: sort dictionary files using the C locale.
  * [#791913](https://bugs.debian.org/791913) on [ceph](https://tracker.debian.org/ceph) by Chris Lamb: sort configuration file names.
  * [#791923](https://bugs.debian.org/791923) on [alpine](https://tracker.debian.org/alpine) by Chris Lamb: use `debian/changelog` date as build date and use `debian` as the builder hostname.
  * [#791960](https://bugs.debian.org/791960) on [leveldb](https://tracker.debian.org/leveldb) by Reiner Herrmann: sort sources files using th e C locale.
  * [#792054](https://bugs.debian.org/792054) on [ben](https://tracker.debian.org/ben) by Reiner Herrmann: use `debian/changelog` date as bui ld date.
  * [#792056](https://bugs.debian.org/792056) on [val-and-rick](https://tracker.debian.org/val-and-rick) by Reiner Herrmann: sort sources by filenames.

## reproducible.debian.net

A new package set has been added for [lua
maintainers](https://reproducible.debian.net/unstable/amd64/pkg_set_maint_lua.html).
(h01ger)

[tracker.debian.org](https://tracker.debian.org/) now only shows
reproducibility issues for _unstable_.

Holger and Mattia worked on [several bugfixes and
enhancements](https://anonscm.debian.org/cgit/qa/jenkins.debian.net.git/log/):
finished initial test setup for NetBSD, rewriting more shell scripts in
Python, saving UDD requests, and more…

## debbindiff development

Reiner Herrmann fixed text comparison of files with different encoding.

## Documentation update

Juan Picca added to the commands needed for a [local test chroot
installation](https://wiki.debian.org/ReproducibleBuilds/ExperimentalToolchain#Usage_example)
of the [locales-all](https://packages.debian.org/locales-all) package.

## Package reviews

286 obsolete
[reviews](https://reproducible.debian.net/unstable/amd64/index_notes.html)
have been removed, 278 added and 243 updated this week.

43 new bugs for packages failing to build from sources have been filled by
Chris West (Faux), Mattia Rizzolo, and h01ger.

The following new issues have been added:
[timestamps_in_manpages_generated_by_ronn](https://reproducible.debian.net/issues/unstable/timestamps_in_manpages_generated_by_ronn_issue.html),
[timestamps_in_documentation_generated_by_org_mode](https://reproducible.debian.net/issues/unstable/timestamps_in_documentation_generated_by_org_mode_issue.html),
and
[timestamps_in_pdf_generated_by_matplotlib](https://reproducible.debian.net/issues/unstable/timestamps_in_pdf_generated_by_matplotlib_issue.html).

## Misc.

Reiner Herrmann has submitted [patches for
OpenWrt](https://lists.openwrt.org/pipermail/openwrt-
devel/2015-July/034281.html).

Chris Lamb cleaned up some code and removed cruft in the [misc.git
repository](https://anonscm.debian.org/cgit/reproducible/misc.git/). Mattia
Rizzolo updated the [prebuilder
script](https://anonscm.debian.org/cgit/reproducible/misc.git/tree/prebuilder)
to match what is currently done on `reproducible.debian.net`.

