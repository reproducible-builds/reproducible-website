---
layout: blog
week: 13
published: 2015-07-26 18:03:47
---

What happened in the [reproducible
builds](https://wiki.debian.org/ReproducibleBuilds) effort this week:

## Toolchain fixes

  * Emmanuel Bourg uploaded [maven-archiver](https://tracker.debian.org/maven-archiver)/2.6-3 which fixed parsing `DEB_CHANGELOG_DATETIME` with non English locales.
  * Emmanuel Bourg uploaded [maven-repo-helper](https://tracker.debian.org/maven-repo-helper)/1.8.12 which always use the same system independent encoding when transforming the pom files.
  * Piotr Ożarowski uploaded [dh-python](https://tracker.debian.org/dh-python)/2.20150719 which makes the order of the generated maintainer scripts deterministic. [Original patch](https://bugs.debian.org/792436) by Chris Lamb.

akira uploaded a new version of [doxygen](https://tracker.debian.org/doxygen)
in the [experimental “reproducible”
repository](https://wiki.debian.org/ReproducibleBuilds/ExprimentalToolchain)
incorporating [upstream
patch](https://github.com/doxygen/doxygen/commit/b31266c1076c6284116f17241d9e8aa048f88e60)
for
[SOURCE_DATE_EPOCH](https://wiki.debian.org/ReproducibleBuilds/TimestampsProposal),
and now producing timezone independent timestamps.

Dhole updated Peter De Wachter's patch on
[ghostscript](https://tracker.debian.org/ghostscript) to use
`SOURCE_DATE_EPOCH` and use UTC as a timezone. A modified package is now being
experimented.

## Packages fixed

The following 14 packages became reproducible due to changes in their build
dependencies: [bino](https://tracker.debian.org/bino),
[cfengine2](https://tracker.debian.org/cfengine2),
[fwknop](https://tracker.debian.org/fwknop), [gnome-
software](https://tracker.debian.org/gnome-software), [jnr-
constants](https://tracker.debian.org/jnr-constants),
[libextractor](https://tracker.debian.org/libextractor),
[libgtop2](https://tracker.debian.org/libgtop2), [maven-compiler-
plugin](https://tracker.debian.org/maven-compiler-plugin), [mk-
configure](https://tracker.debian.org/mk-configure),
[nanoc](https://tracker.debian.org/nanoc), [octave-
splines](https://tracker.debian.org/octave-splines), [octave-
symbolic](https://tracker.debian.org/octave-symbolic),
[riece](https://tracker.debian.org/riece), [vdr-plugin-
infosatepg](https://tracker.debian.org/vdr-plugin-infosatepg).

The following packages became reproducible after getting fixed:

  * [cpl-plugin-fors](https://tracker.debian.org/cpl-plugin-fors)/5.0.11+dfsg-4 uploaded by Ole Streicher, [original patch](https://bugs.debian.org/792933) by Chris Lamb.
  * [cpl-plugin-hawki](https://tracker.debian.org/cpl-plugin-hawki)/1.8.18+dfsg-4 uploaded by Ole Streicher, [original patch](https://bugs.debian.org/792936) by Chris Lamb.
  * [cpl-plugin-kmos](https://tracker.debian.org/cpl-plugin-kmos)/1.3.12+dfsg-4 uploaded by Ole Streicher, [original patch](https://bugs.debian.org/792937) by Chris Lamb.
  * [cpl-plugin-visir](https://tracker.debian.org/cpl-plugin-visir)/3.5.1+dfsg-3 uploaded by Ole Streicher, [original patch](https://bugs.debian.org/792942) by Chris Lamb.
  * [docbook-dsssl](https://tracker.debian.org/docbook-dsssl)/1.79-8 uploaded by Peter Eisentraut, [original patch](https://bugs.debian.org/792763) by Chris Lamb.
  * [git-annex](https://tracker.debian.org/git-annex)/5.20150617 by Joey Hess, [initial report](https://bugs.debian.org/785736) by Daniel Kahn Gillmor.
  * [links2](https://tracker.debian.org/links2)/2.10-1 by Axel Beckert.
  * [linuxlogo](https://tracker.debian.org/linuxlogo)/5.11-8 uploaded by Dariusz Dwornikowski, [original patch](https://bugs.debian.org/792783) by Reiner Herrmann.
  * [maradns](https://tracker.debian.org/maradns)/2.0.11-1 uploaded by Dariusz Dwornikowski, [original patch](https://bugs.debian.org/785535) by Reiner Herrmann.
  * [pkgconf](https://tracker.debian.org/pkgconf)/0.9.12-1 uploaded by Andrew Shadura, [original patch](https://bugs.debian.org/792285) by Juan Picca.
  * [python-odf](https://tracker.debian.org/python-odf)/1.3.1+dfsg-1 uploaded by W. Martin Borgert, [original patch](https://bugs.debian.org/777635) by akira.
  * [qsapecng](https://tracker.debian.org/qsapecng)/2.0.0-6 uploaded by Simone Rossetto, [original patch](https://bugs.debian.org/789428) by akira.
  * [ruby-liquid](https://tracker.debian.org/ruby-liquid)/3.0.4-1 uploaded by Cédric Boutillier, [report](https://bugs.debian.org/788694) by Dylan Thacker-Smith.
  * [ruby-standalone](https://tracker.debian.org/ruby-standalone)/0.5.1 uploaded by Antonio Terceiro, [original patch](https://bugs.debian.org/792525) by Reiner Herrmann.
  * [sahara](https://tracker.debian.org/sahara)/2015.1.0-6 uploaded by Thomas Goirand, [original patch](https://bugs.debian.org/788595) by Juan Picca.
  * [squid3](https://tracker.debian.org/squid3)/3.5.6-1 uploaded by Luigi Gangitano, fixed upstream.
  * [tabble](https://tracker.debian.org/tabble)/0.43-3 uploaded by gregor herrmann, [original patch](https://bugs.debian.org/782222) by Reiner Herrmann.
  * [twine](https://tracker.debian.org/twine)/1.5.0-1 by Zygmunt Krynicki.
  * [visp](https://tracker.debian.org/visp)/2.10.0-4 uploaded by Fabien Spindler, [original patch](https://bugs.debian.org/790074) by akira.

Some uploads fixed some reproducibility issues but not all of them:

  * [dpatch](https://tracker.debian.org/dpatch)/2.0.36 by Gergely Nagy, with a [patch](https://bugs.debian.org/792954) from akira.
  * [fldigi](https://tracker.debian.org/fldigi)/3.22.13-1 by Kamal Mostafa.
  * [fortunes-bg](https://tracker.debian.org/fortunes-bg)/1.2 uploaded by Anton Zinoviev, [original patch](https://bugs.debian.org/792970) by akira.
  * [openjfx](https://tracker.debian.org/openjfx)/8u40-b25-3 by Emmanuel Bourg.
  * [zoneminder](https://tracker.debian.org/zoneminder)/1.28.1-7 by Dmitry Smirnov.
  * [zookeeper](https://tracker.debian.org/zookeeper)/3.4.6-5 by Emmanuel Bourg.

Patches submitted which have not made their way to the archive yet:

  * [#792943](https://bugs.debian.org/792943) on [argus-client](https://tracker.debian.org/argus-client) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792945](https://bugs.debian.org/792945) on [authbind](https://tracker.debian.org/authbind) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792947](https://bugs.debian.org/792947) on [cvs-mailcommit](https://tracker.debian.org/cvs-mailcommit) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792949](https://bugs.debian.org/792949) on [chimera2](https://tracker.debian.org/chimera2) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792950](https://bugs.debian.org/792950) on [ccze](https://tracker.debian.org/ccze) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792951](https://bugs.debian.org/792951) on [dbview](https://tracker.debian.org/dbview) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792952](https://bugs.debian.org/792952) on [dhcpdump](https://tracker.debian.org/dhcpdump) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792953](https://bugs.debian.org/792953) on [dhcping](https://tracker.debian.org/dhcping) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792955](https://bugs.debian.org/792955) on [dput](https://tracker.debian.org/dput) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792958](https://bugs.debian.org/792958) on [dtaus](https://tracker.debian.org/dtaus) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792959](https://bugs.debian.org/792959) on [elida](https://tracker.debian.org/elida) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792961](https://bugs.debian.org/792961) on [enemies-of-carlotta](https://tracker.debian.org/enemies-of-carlotta) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792963](https://bugs.debian.org/792963) on [erc](https://tracker.debian.org/erc) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792965](https://bugs.debian.org/792965) on [fastforward](https://tracker.debian.org/fastforward) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792967](https://bugs.debian.org/792967) on [fgetty](https://tracker.debian.org/fgetty) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792969](https://bugs.debian.org/792969) on [flowscan](https://tracker.debian.org/flowscan) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792971](https://bugs.debian.org/792971) on [junior-doc](https://tracker.debian.org/junior-doc) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792972](https://bugs.debian.org/792972) on [libjama](https://tracker.debian.org/libjama) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792973](https://bugs.debian.org/792973) on [liblip](https://tracker.debian.org/liblip) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792974](https://bugs.debian.org/792974) on [liblockfile](https://tracker.debian.org/liblockfile) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792975](https://bugs.debian.org/792975) on [libmsv](https://tracker.debian.org/libmsv) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792976](https://bugs.debian.org/792976) on [logapp](https://tracker.debian.org/logapp) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792977](https://bugs.debian.org/792977) on [luakit](https://tracker.debian.org/luakit) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792978](https://bugs.debian.org/792978) on [nec](https://tracker.debian.org/nec) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792979](https://bugs.debian.org/792979) on [runit](https://tracker.debian.org/runit) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792980](https://bugs.debian.org/792980) on [tworld](https://tracker.debian.org/tworld) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792981](https://bugs.debian.org/792981) on [wmweather](https://tracker.debian.org/wmweather) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792982](https://bugs.debian.org/792982) on [ftpcopy](https://tracker.debian.org/ftpcopy) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792983](https://bugs.debian.org/792983) on [gerstensaft](https://tracker.debian.org/gerstensaft) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792984](https://bugs.debian.org/792984) on [integrit](https://tracker.debian.org/integrit) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792985](https://bugs.debian.org/792985) on [ipsvd](https://tracker.debian.org/ipsvd) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792986](https://bugs.debian.org/792986) on [uruk](https://tracker.debian.org/uruk) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792987](https://bugs.debian.org/792987) on [jargon](https://tracker.debian.org/jargon) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792988](https://bugs.debian.org/792988) on [xbs](https://tracker.debian.org/xbs) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792989](https://bugs.debian.org/792989) on [freecdb](https://tracker.debian.org/freecdb) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792990](https://bugs.debian.org/792990) on [skalibs](https://tracker.debian.org/skalibs) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792991](https://bugs.debian.org/792991) on [gpsmanshp](https://tracker.debian.org/gpsmanshp) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792993](https://bugs.debian.org/792993) on [cgoban](https://tracker.debian.org/cgoban) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792994](https://bugs.debian.org/792994) on [angband-doc](https://tracker.debian.org/angband-doc) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792995](https://bugs.debian.org/792995) on [abook](https://tracker.debian.org/abook) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792996](https://bugs.debian.org/792996) on [bcron](https://tracker.debian.org/bcron) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792998](https://bugs.debian.org/792998) on [chiark-utils](https://tracker.debian.org/chiark-utils) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#792999](https://bugs.debian.org/792999) on [console-cyrillic](https://tracker.debian.org/console-cyrillic) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#793000](https://bugs.debian.org/793000) on [beav](https://tracker.debian.org/beav) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#793001](https://bugs.debian.org/793001) on [blosxom](https://tracker.debian.org/blosxom) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#793002](https://bugs.debian.org/793002) on [cgilib](https://tracker.debian.org/cgilib) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#793003](https://bugs.debian.org/793003) on [daemontools](https://tracker.debian.org/daemontools) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#793004](https://bugs.debian.org/793004) on [debdelta](https://tracker.debian.org/debdelta) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#793005](https://bugs.debian.org/793005) on [checkpw](https://tracker.debian.org/checkpw) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#793006](https://bugs.debian.org/793006) on [dropbear](https://tracker.debian.org/dropbear) by akira: set the mtimes of all files which are modified during builds to the latest `debian/changelog` entry.
  * [#793126](https://bugs.debian.org/793126) on [torbutton](https://tracker.debian.org/torbutton) by Dhole: set `TZ=UTC` when calling `zip`.
  * [#793127](https://bugs.debian.org/793127) on [pdf.js](https://tracker.debian.org/pdf.js) by Dhole: set `TZ=UTC` when calling `zip`.
  * [#793300](https://bugs.debian.org/793300) on [deejayd](https://tracker.debian.org/deejayd) by Dhole: set `TZ=UTC` when calling `zip`.

## reproducible.debian.net

Packages identified as failing to build from source with no bugs filed and
older than 10 days are scheduled more often now (except in experimental).
(h01ger)

## Package reviews

178 obsolete
[reviews](https://reproducible.debian.net/unstable/amd64/index_notes.html)
have been removed, 59 added and 122 updated this week.

New issue identified this week:
[random_order_in_ruby_rdoc_indices](https://reproducible.debian.net/issues/unstable/random_order_in_ruby_rdoc_indices_issue.html).

18 new bugs for packages failing to build from sources have been reported by
Chris West (Faux), and h01ger.

