---
layout: blog
week: 16
published: 2015-08-16 16:11:33
---

What happened in the [reproducible
builds](https://wiki.debian.org/ReproducibleBuilds) effort this week:

## Toolchain fixes

  * Stéphane Glondu uploaded [dh-ocaml](https://tracker.debian.org/dh-ocaml)/1.0.10 which now generates *.info with a deterministic order. [Original patch](https://bugs.debian.org/779037) by Chris Lamb. Stéphane also uploaded [ocaml](https://tracker.debian.org/ocaml)/4.02.3-1 to experimental which enables `ocamldoc` to build reproducible manpages using a [patch](https://bugs.debian.org/794586) by Valentin Lorentz.
  * Paul Gevers uploaded [pasdoc](https://tracker.debian.org/pasdoc)/0.14.0-1 with upstream changes which should make the generated documentation reproducible by default.
  * Osamu Aoki uploaded [debiandoc-sgml](https://tracker.debian.org/debiandoc-sgml)/1.2.31-1 adding spport for a `DEBIANDOC_DATE` environment variable to override the content of the `<date>` tag.
  * Dmitry Shachnev uploaded [sphinx](https://tracker.debian.org/sphinx)/1.3.1-4 which fixes many reproducibility issues and add support for [SOURCE_DATE_EPOCH](https://wiki.debian.org/ReproducibleBuilds/TimestampsProposal).

Valentin Lorentz sent a [patch](https://bugs.debian.org/795606) for
[ispell](https://tracker.debian.org/ispell) to initialize memory structures
before dumping their content.

In [our experimental
repository](https://wiki.debian.org/ReproducibleBuilds/ExperimentalToolchain),
[qt4-x11](https://tracker.debian.org/qt4-x11) has been rebased on the latest
version (Dhole), as was [doxygen](https://tracker.debian.org/doxygen) (akira).

## Packages fixed

The following packages became reproducible due to changes in their build
dependencies: [backup-manager](https://tracker.debian.org/backup-manager),
[cheese](https://tracker.debian.org/cheese), [coinor-
csdp](https://tracker.debian.org/coinor-csdp), [coinor-
dylp](https://tracker.debian.org/coinor-dylp), [ebook-
speaker](https://tracker.debian.org/ebook-speaker),
[freefem](https://tracker.debian.org/freefem),
[indent](https://tracker.debian.org/indent), [libjbcrypt-
java](https://tracker.debian.org/libjbcrypt-java), [qtquick1-opensource-
src](https://tracker.debian.org/qtquick1-opensource-src), [ruby-coffee-
script](https://tracker.debian.org/ruby-coffee-script), [ruby-
distribution](https://tracker.debian.org/ruby-distribution),
[schroot](https://tracker.debian.org/schroot), [twittering-
mode](https://tracker.debian.org/twittering-mode).

The following packages became reproducible after getting fixed:

  * [abook](https://tracker.debian.org/abook)/0.6.0~pre2-5 by Denis Briand.
  * [ada-reference-manual](https://tracker.debian.org/ada-reference-manual)/1:2012.2-6 by Nicolas Boulenguez.
  * [aegisub](https://tracker.debian.org/aegisub)/3.2.2+dfsg-1 uploaded by Sebastian Reichel, [original patch](https://bugs.debian.org/789728) by Juan Picca.
  * [casablanca](https://tracker.debian.org/casablanca)/2.5.0-1 uploaded by Gianfranco Costamagna, fix by Mattia Rizzolo.
  * [dpatch](https://tracker.debian.org/dpatch)/2.0.37 by Gergely Nagy.
  * [ganeti](https://tracker.debian.org/ganeti)/2.14.1-1~exp1 by Apollon Oikonomopoulos.
  * [gperf](https://tracker.debian.org/gperf)/3.0.4-2 by Hilko Bengen, [report](https://bugs.debian.org/794955) by akira.
  * [htdig](https://tracker.debian.org/htdig)/1:3.2.0b6-14 uploaded by Ralf Treinen, [original patch](https://bugs.debian.org/793648) by Chris Lamb.
  * [icedove](https://tracker.debian.org/icedove)/38.1.0-1 uploaded by Christoph Goehre, [report](https://bugs.debian.org/794456) by Lunar.
  * [kamailio](https://tracker.debian.org/kamailio)/4.3.1-2 by Victor Seva.
  * [leveldb](https://tracker.debian.org/leveldb)/1.18-3 uploaded by Laszlo Boszormenyi, [original patch](https://bugs.debian.org/791960) by Reiner Herrmann.
  * [librostlab-blast](https://tracker.debian.org/librostlab-blast)/1.0.1-5 uploaded by Andreas Tille, [original patch](https://bugs.debian.org/795485) by Chris Lamb.
  * [linux-tools](https://tracker.debian.org/linux-tools)/4.1.4-1 by Ben Hutchings.
  * [nitpic](https://tracker.debian.org/nitpic)/0.1-16 uploaded by Ralf Treinen, patches by Chris Lamb ([#777492](https://bugs.debian.org/777492)) and akira ([#793708](https://bugs.debian.org/793708)).
  * [openscad](https://tracker.debian.org/openscad)/2015.03-1+dfsg-1 uploaded by Christian M. Amsüss, fixed upstream.
  * [pciutils](https://tracker.debian.org/pciutils)/1:3.3.1-1 uploaded by Anibal Monsalve Salazar, [origial patch](https://bugs.debian.org/783144) by Reiner Herrmann.
  * [pyepr](https://tracker.debian.org/pyepr)/0.9.3-1 uploaded by Antonio Valentino, [original patch](https://bugs.debian.org/788246) by Juan Picca.
  * [pytables](https://tracker.debian.org/pytables)/3.2.1-1 by Antonio Valentino.
  * [python-xlib](https://tracker.debian.org/python-xlib)/0.14+20091101-3 by Andrew Shadura, [report](https://bugs.debian.org/795057) by akira.
  * [rrdtool](https://tracker.debian.org/rrdtool)/1.5.4-3 by Jean-Michel Vourgère.
  * [sgmltools-lite](https://tracker.debian.org/sgmltools-lite)/3.0.3.0.cvs.20010909-18 uploaded by Ralf Treinen, patches by Chris Lamb ([#777011](https://bugs.debian.org/777011)) and akira ([#793720](https://bugs.debian.org/793720)).
  * [uhd](https://tracker.debian.org/uhd)/3.8.5-2 by A. Maitland Bottoms.
  * [xfireworks](https://tracker.debian.org/xfireworks)/1.3-10 uploaded by Yukiharu YABUKI, [original patch](https://bugs.debian.org/777402) by Chris Lamb.

Some uploads fixed some reproducibility issues but not all of them:

  * [ben](https://tracker.debian.org/ben)/0.7.1 uploaded by Mehdi Dogguy, [original patch](https://bugs.debian.org/792054) by Reiner Herrmann.
  * [debiandoc-sgml-doc](https://tracker.debian.org/debiandoc-sgml-doc)/1.1.24 uploaded by Osamu Aoki, [initial patch](https://bugs.debian.org/794795) by Dhole.
  * [ifrench-gut](https://tracker.debian.org/ifrench-gut)/1:1.0-31 uploaded by Lionel Elie Mamane, [original patch](https://bugs.debian.org/795581) by Valentin Lorentz.
  * [qdjango](https://tracker.debian.org/qdjango)/0.6.0-1 uploaded by Jeremy Lainé, fixed upstream, [original patch](https://bugs.debian.org/789405) by akira.
  * [spectacle](https://tracker.debian.org/spectacle)/0.25-1 assembled by Philippe Coval, [original patch](https://bugs.debian.org/775996) by Chris Lamb.

Patches submitted which have not made their way to the archive yet:

  * [#795203](https://bugs.debian.org/795203) on [xlsx2csv](https://tracker.debian.org/xlsx2csv) by Dhole: set `PODDATE` to the date of the latest `debian/changelog` entry.
  * [#795392](https://bugs.debian.org/795392) on [blkreplay](https://tracker.debian.org/blkreplay) by Dhole: tell `pod2man` to use the date of the latest `debian/changelog` entry.
  * [#795394](https://bugs.debian.org/795394) on [libitpp](https://tracker.debian.org/libitpp) by Dhole: use `SOURCE_DATE_EPOCH` as source for the manpage date instead of the currentdate.
  * [#795395](https://bugs.debian.org/795395) on [adblock-plus](https://tracker.debian.org/adblock-plus) by Dhole: set `TZ` to `UTC` when using `zip`.
  * [#795438](https://bugs.debian.org/795438) on [wims-extra](https://tracker.debian.org/wims-extra) by Chris Lamb: ask `grep` to cope with non-UTF8 files.
  * [#795441](https://bugs.debian.org/795441) on [aprx](https://tracker.debian.org/aprx) by Chris Lamb: use `SOURCE_DATE_EPOCH` as source for the manpage date instead of the currentdate.
  * [#795462](https://bugs.debian.org/795462) on [ogre-1.9](https://tracker.debian.org/ogre-1.9) by Chris Lamb: removes timestamps from the documentation system.
  * [#795484](https://bugs.debian.org/795484) on [ruby-rmagick](https://tracker.debian.org/ruby-rmagick) by Chris Lamb: patch the bindings to allow the PRNG seed to be set, and set it to a fixed value when generating examples.
  * [#795562](https://bugs.debian.org/795562) on [htp](https://tracker.debian.org/htp) by Chris Lamb: export `TZ=UTC` in `debian/rules`.

akira found [another embedded code copy](https://bugs.debian.org/795056) of
[texi2html](https://tracker.debian.org/texi2html) in
[maxima](https://tracker.debian.org/maxima).

## reproducible.debian.net

Work on testing several architectures has continued. (Mattia/h01ger)

## Package reviews

29 [reviews](https://reproducible.debian.net/unstable/amd64/index_notes.html)
have been removed, 187 added and 34 updated this week.

172 new FTBFS reports were filled, 137 solely by Chris West (Faux).

josch spent time investigating the issue with [fonts in PDF
files](https://reproducible.debian.net/issues/unstable/fonts_in_pdf_files_issue.html).

Chris Lamb documented the [issue affecting documentation generated by
ocamldoc](https://reproducible.debian.net/issues/unstable/timestamps_in_documentation_generated_by_ocamldoc_issue.html).

## Misc.

Lunar presented a general [“Reproducible builds HOWTO”
talk](https://events.ccc.de/camp/2015/Fahrplan/events/6657.html) at the [Chaos
Communication Camp 2015](https://events.ccc.de/camp/2015/) in Germany on
August 13th.
[Recordings](https://media.ccc.de/browse/conferences/camp2015/camp2015-6657-how_to_make_your_software_build_reproducibly.html)
are already available, as well as
[slides](https://reproducible.alioth.debian.org/presentations/2015-08-13-CCCamp15.pdf)
and
[script](https://reproducible.alioth.debian.org/presentations/2015-08-13-CCCamp15-outline.pdf).

h01ger and Lunar also used CCCamp15 as an opportunity to have discussions with
members of several different projects about reproducible builds. Good news
should be coming soon.

