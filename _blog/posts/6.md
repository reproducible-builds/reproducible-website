---
layout: blog
week: 6
published: 2015-06-08 16:58:54
---

What happened about the [reproducible
builds](https://wiki.debian.org/ReproducibleBuilds) effort for this week:

## Presentations

On May 26th,Holger Levsen presented reproducible builds in Debian at CCC
Berlin for the [Datengarten 52](https://berlin.ccc.de/wiki/Datengarten/52).
The presentation was in German and the [slides](http://meetings-
archive.debian.net/pub/debian-meetings/2015/datengarten-ccc-
berlin/2015-05-26-CCCBerlin.pdf) in English. [Audio and video
recordings](http://meetings-archive.debian.net/pub/debian-
meetings/2015/datengarten-ccc-berlin/) are available.

## Toolchain fixes

  * Dmitry Shachnev uploaded [pyqt5](https://tracker.debian.org/pyqt5)/5.4.1+dfsg-3 which makes `pyuic` output imports in stable order. [Original patch](https://bugs.debian.org/787251) by Reiner Herrmann.
  * Lunar uploaded [mozilla-devscripts](https://tracker.debian.org/mozilla-devscripts)/0.41 which uses the UTC timezone when calling `zip` or `unzip`.

Niels Thykier
[fixed](https://anonscm.debian.org/cgit/reproducible/debhelper.git/commit/?h=pu/reproducible_builds&id=66b9e28502bfde32c43dcc80e96d9d33cebea62a)
the experimental support for the automatic creation of debug packages in
[debhelper](https://tracker.debian.org/debhelper) that being tested as part of
the reproducible toolchain.

Lunar added to the “reproducible build” version of
[dpkg](https://tracker.debian.org/dpkg) the normalization of permissions for
files in `control.tar`. The patch has also been
[submitted](https://bugs.debian.org/787980) based on the main branch.

Daniel Kahn Gillmor [proposed a patch](https://bugs.debian.org/787444) to add
support for externally-supplying build date to
[help2man](https://tracker.debian.org/help2man). This sparkled a discussion
about agreeing on a common name for an environment variable to hold the date
that should be used. It seems opinions are converging on using
`SOURCE_DATE_UTC` which would hold a [ISO-8601 formatted date in
UTC](https://en.wikipedia.org/wiki/ISO_8601#UTC)) (e.g.
`2015-06-05T01:08:20Z`). Kudos to Daniel, Brendan O'Dea, Ximin Luo for pushing
this forward.

Lunar proposed a patch to Tar upstream [adding a `--clamp-mtime`
option](https://lists.gnu.org/archive/html/help-tar/2015-06/msg00000.html) as
a generic solution for [timestamp variations in
tarballs](https://wiki.debian.org/ReproducibleBuilds/TimestampsInTarball)
which might also be useful for dpkg. The option changes the behavior of
`--mtime` to only use the time specified if the file mtime is newer than the
given time. So far, upstream is not convinced that it would make a worthwhile
addition to Tar, though.

Daniel Kahn Gillmor [reached out to the libburnia
project](https://lists.gnu.org/archive/html/bug-xorriso/2015-06/msg00006.html)
to ask for help on how to make ISO created with `xorriso` reproducible. We
should reward Thomas Schmitt with a “model upstream” trophy as he went through
a thorough analysis of possible sources of variations and ways to improve the
situation. Most of what is missing with the current version in Debian is
available in the latest upstream version, but [libisoburn in Debian needs
help](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=679265). Daniel
[backported the missing option for version
1.3.2-1.1](https://bugs.debian.org/787793).

akira [submitted a new
issue](https://bugzilla.gnome.org/show_bug.cgi?id=750447) to Doxygen upstream
regarding the [timestamps added to the generated
manpages](https://wiki.debian.org/ReproducibleBuilds/TimestampsInManpagesGeneratedByDoxygen).

## Packages fixed

The following 49 packages became reproducible due to changes in their build
dependencies: [activemq-protobuf](https://tracker.debian.org/activemq-
protobuf), [bnfc](https://tracker.debian.org/bnfc), [bridge-method-
injector](https://tracker.debian.org/bridge-method-injector), [commons-
exec](https://tracker.debian.org/commons-exec), [console-
data](https://tracker.debian.org/console-data),
[djinn](https://tracker.debian.org/djinn), [github-
backup](https://tracker.debian.org/github-backup), [haskell-authenticate-
oauth](https://tracker.debian.org/haskell-authenticate-oauth), [haskell-
authenticate](https://tracker.debian.org/haskell-authenticate), [haskell-
blaze-builder](https://tracker.debian.org/haskell-blaze-builder), [haskell-
blaze-textual](https://tracker.debian.org/haskell-blaze-textual), [haskell-
bloomfilter](https://tracker.debian.org/haskell-bloomfilter), [haskell-
brainfuck](https://tracker.debian.org/haskell-brainfuck), [haskell-hspec-
discover](https://tracker.debian.org/haskell-hspec-discover), [haskell-pretty-
show](https://tracker.debian.org/haskell-pretty-show), [haskell-
unlambda](https://tracker.debian.org/haskell-unlambda),
[haskell-x509-util](https://tracker.debian.org/haskell-x509-util), [haskelldb-
hdbc-odbc](https://tracker.debian.org/haskelldb-hdbc-odbc), [haskelldb-hdbc-
postgresql](https://tracker.debian.org/haskelldb-hdbc-postgresql), [haskelldb-
hdbc-sqlite3](https://tracker.debian.org/haskelldb-hdbc-sqlite3),
[hasktags](https://tracker.debian.org/hasktags),
[hedgewars](https://tracker.debian.org/hedgewars),
[hscolour](https://tracker.debian.org/hscolour), [https-
everywhere](https://tracker.debian.org/https-everywhere), [java-comment-
preprocessor](https://tracker.debian.org/java-comment-preprocessor),
[jffi](https://tracker.debian.org/jffi),
[jgit](https://tracker.debian.org/jgit), [jnr-
ffi](https://tracker.debian.org/jnr-ffi), [jnr-
netdb](https://tracker.debian.org/jnr-netdb),
[jsoup](https://tracker.debian.org/jsoup),
[lhs2tex](https://tracker.debian.org/lhs2tex), [libcolor-calc-
perl](https://tracker.debian.org/libcolor-calc-perl), [libfile-changenotify-
perl](https://tracker.debian.org/libfile-changenotify-perl), [libpdl-io-
hdf5-perl](https://tracker.debian.org/libpdl-io-hdf5-perl), [libsvn-notify-
mirror-perl](https://tracker.debian.org/libsvn-notify-mirror-perl),
[localizer](https://tracker.debian.org/localizer), [maven-
enforcer](https://tracker.debian.org/maven-enforcer),
[pyotherside](https://tracker.debian.org/pyotherside), [python-
xlrd](https://tracker.debian.org/python-xlrd), [python-xstatic-angular-
bootstrap](https://tracker.debian.org/python-xstatic-angular-bootstrap), [rt-
extension-calendar](https://tracker.debian.org/rt-extension-calendar), [ruby-
builder](https://tracker.debian.org/ruby-builder), [ruby-em-
hiredis](https://tracker.debian.org/ruby-em-hiredis), [ruby-
redcloth](https://tracker.debian.org/ruby-redcloth),
[shellcheck](https://tracker.debian.org/shellcheck), [sisu-
plexus](https://tracker.debian.org/sisu-plexus), [tomcat-maven-
plugin](https://tracker.debian.org/tomcat-maven-plugin),
[v4l2loopback](https://tracker.debian.org/v4l2loopback), [vim-
latexsuite](https://tracker.debian.org/vim-latexsuite).

The following packages became reproducible after getting fixed:

  * [afterstep](https://tracker.debian.org/afterstep)/2.2.12-6 uploaded by Robert Luberda, [original patch](https://bugs.debian.org/785774) by Juan Picca.
  * [birdfont](https://tracker.debian.org/birdfont)/2.8.0-2 by Hideki Yamane.
  * [bzr](https://tracker.debian.org/bzr)/2.6.0+bzr6602-1 by Jelmer Vernooij.
  * [cassiopee](https://tracker.debian.org/cassiopee)/1.0.3+dfsg-2 uploaded by Olivier Sallou, [original patch](https://bugs.debian.org/787765) by akira.
  * [dhcp-helper](https://tracker.debian.org/dhcp-helper)/1.1-3 by Simon Kelley.
  * [elog](https://tracker.debian.org/elog)/3.1.0-2-1 by Roger Kalt.
  * [flashcache](https://tracker.debian.org/flashcache)/3.1.3+git20150513-1 uploaded by Liang Guo, [original patch](https://bugs.debian.org/780883) by Reiner Herrmann.
  * [fonts-kiloji](https://tracker.debian.org/fonts-kiloji)/1:2.1.0-21 by Hideki Yamane.
  * [inspircd](https://tracker.debian.org/inspircd)/2.0.20-2 by Guillaume Delacour.
  * [jd](https://tracker.debian.org/jd)/1:2.8.9-150226-3 by Hideki Yamane.
  * [jruby-joni](https://tracker.debian.org/jruby-joni)/2.1.6-2 by Hideki Yamane.
  * [libaqbanking](https://tracker.debian.org/libaqbanking)/5.6.0beta-1 by Micha Lenk.
  * [libencode-hanextra-perl](https://tracker.debian.org/libencode-hanextra-perl)/0.23-4 [fixed](https://bugs.debian.org/787754) and uploaded by Niko Tyni.
  * [libencode-jis2k-perl](https://tracker.debian.org/libencode-jis2k-perl)/0.02-3 by Niko Tyni.
  * [libjide-oss-java](https://tracker.debian.org/libjide-oss-java)/3.6.9+dfsg-1 by Markus Koschany.
  * [librpc-xml-perl](https://tracker.debian.org/librpc-xml-perl)/0.78-3 upload by Niko Tyni, [original patch](https://bugs.debian.org/787206) by Reiner Herrmann.
  * [libterm-readkey-perl](https://tracker.debian.org/libterm-readkey-perl)/2.32-2 by Niko Tyni.
  * [mkgmap-splitter](https://tracker.debian.org/mkgmap-splitter)/0.0.0+svn421-1 by Bas Couwenberg.
  * [mod-authn-webid](https://tracker.debian.org/mod-authn-webid)/0~20110301-3 by Clint Adams, [original patch](https://bugs.debian.org/778199) by Chris Lamb.
  * [ossim](https://tracker.debian.org/ossim)/1.8.16-4 uploaded by Bas Couwenberg, [original patch](https://bugs.debian.org/787790) by Juan Picca.
  * [psfex](https://tracker.debian.org/psfex)/3.17.1+dfsg-2 by Ole Streicher.
  * [socket-wrapper](https://tracker.debian.org/socket-wrapper)/1.1.3-2 uploaded by Laszlo Boszormenyi, [original patch](https://bugs.debian.org/782863) by Jelmer Vernooij.
  * [stiff](https://tracker.debian.org/stiff)/2.4.0-2 by Ole Streicher.
  * [testng](https://tracker.debian.org/testng)/6.9.4-2 by Eugene Zhukov.
  * [wcwidth](https://tracker.debian.org/wcwidth)/0.1.4-2 by Sebastian Ramacher.

Some uploads fixed some reproducibility issues but not all of them:

  * [ant](https://tracker.debian.org/ant)/1.9.5-1 by Emmanuel Bourg.
  * [gcin](https://tracker.debian.org/gcin)/2.8.3+dfsg1-2 by ChangZhuo Chen.
  * [grcompiler](https://tracker.debian.org/grcompiler)/4.2-5 by Hideki Yamane.
  * [python2.7](https://tracker.debian.org/python2.7)/2.7.10-2 uploaded by Matthias Klose, [based on a patch](https://bugs.debian.org/786978) by Lunar.
  * [python3.4](https://tracker.debian.org/python3.4)/3.4.3-7 uploaded by Matthias Klose, [based on a patch](https://bugs.debian.org/786965) by Lunar.
  * [python3.5](https://tracker.debian.org/python3.5)/3.5.0~b2-1 uploaded by Matthias Klose, [based on a patch](https://bugs.debian.org/786959) by Lunar.
  * [wml](https://tracker.debian.org/wml)/2.0.12ds1-9 by Axel Beckert.

Patches submitted which did not make their way to the archive yet:

  * [#787327](https://bugs.debian.org/787327) on [vim](https://tracker.debian.org/vim) by Reiner Herrmann: set a constant user and set `modified-by` option.
  * [#787650](https://bugs.debian.org/787650) on [lush](https://tracker.debian.org/lush) by Daniel Kahn Gillmor: remove `__DATE__` and `__TIME__` macros from source.
  * [#787669](https://bugs.debian.org/787669) on [cloc](https://tracker.debian.org/cloc) by Daniel Kahn Gillmor: use time of latest `debian/changelog` entry in manpage.
  * [#787675](https://bugs.debian.org/787675) on [ricochet](https://tracker.debian.org/ricochet) by Daniel Kahn Gillmor: patch configure to allow an external build date and set it to the time of latest `debian/changelog` entry.
  * [#787804](https://bugs.debian.org/787804) on [clipper](https://tracker.debian.org/clipper) by akira: set `HTML_TIMESTAMP` to `NO` in Doxygen configuration.
  * [#787829](https://bugs.debian.org/787829) on [colobot](https://tracker.debian.org/colobot) by akira: set `HTML_TIMESTAMP` to `NO` in Doxygen configuration.
  * [#787865](https://bugs.debian.org/787865) on [fastjet](https://tracker.debian.org/fastjet) by akira: call Doxygen with `HTML_TIMESTAMP` set to `NO`.
  * [#787916](https://bugs.debian.org/787916) on [cal3d](https://tracker.debian.org/cal3d) by akira: remove `$datetime` from the file `api_footer.html`.
  * [#787918](https://bugs.debian.org/787918) on [dime](https://tracker.debian.org/dime) by akira: remove `$datetime` from the file `footer.html`.

Daniel Kahn Gilmor also started discussions for
[emacs24](https://tracker.debian.org/emacs24\]) and the [unsorted lists in
generated .el files](https://bugs.debian.org/787424), the [recording of a PID
number](https://bugs.debian.org/787651) in
[lush](https://tracker.debian.org/lush), and the [reproducibility of ISO
images](https://bugs.debian.org/787795) in
[grub2](https://tracker.debian.org/grub2).

## reproducible.debian.net

Notifications are now sent when the build environment for a package has
changed between two builds. This is a first step before automatically building
the package once more. (Holger Levsen)

jenkins.debian.net was upgraded to Debian Jessie. (Holger Levsen)

A new [variation](https://reproducible.debian.net/index_stats.html#variation)
is now being tested: `$PATH`. The second build will be done with a
`/i/capture/the/path` added. (Holger Levsen)

Holger Levsen with the help of Alexander Couzens wrote extra job to [test the
reproducibility](https://reproducible.debian.net/coreboot/) of
[coreboot](http://www.coreboot.org/). Thanks James McCoy for helping with
certificate issues.

Mattia Rizollo made some more internal improvements.

## strip-nondeterminism development

Andrew Ayer released [strip-nondeterminism](https://tracker.debian.org/strip-
nondeterminism)/0.008-1. This new version fixes the gzip handler so that it
now [skip adding a predetermined timestamp when there was
none](https://bugs.debian.org/785742).

Holger Levsen sponsored the upload.

## Documentation update

The pages about timestamps in [manpages generated by
Doxygen](https://wiki.debian.org/ReproducibleBuilds/TimestampsInManpagesGeneratedByDoxygen),
[GHC .hi
files](https://wiki.debian.org/ReproducibleBuilds/TimestampInGhcInterfaces),
and [Jar
files](https://wiki.debian.org/ReproducibleBuilds/TimestampsInJarFiles) have
been updated to reflect their status in upstream.

Markus Koschany documented an [easy way to prevent Doxygen to write timestamps
in HTML
output](https://wiki.debian.org/ReproducibleBuilds/TimestampsInDocumentationGeneratedByDoxygen).

## Package reviews

83 obsolete
[reviews](https://reproducible.debian.net/unstable/amd64/index_notes.html)
have been removed, 71 added and 48 updated this week.

## Meetings

A meeting was held on 2015-06-03. [Minutes](http://meetbot.debian.net/debian-
reproducible/2015/debian-reproducible.2015-06-03-19.03.html) and [full
logs](http://meetbot.debian.net/debian-reproducible/2015/debian-
reproducible.2015-06-03-19.03.log.html) are available.

It was agreed to hold such a meeting every two weeks for the time being. The
[time of the next
meeting](https://lists.alioth.debian.org/pipermail/reproducible-builds/Week-
of-Mon-20150601/001687.html) should be announced soon.

