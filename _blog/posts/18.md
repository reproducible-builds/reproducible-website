---
layout: blog
week: 18
published: 2015-09-01 14:51:21
---

What happened in the [reproducible
builds](https://wiki.debian.org/ReproducibleBuilds) effort this week:

## Toolchain fixes

  * Bdale Garbee uploaded [tar](https://tracker.debian.org/tar)/1.28-1 which includes the `--clamp-mtime` option. [Patch](https://bugs.debian.org/790415) by Lunar.

Aurélien Jarno uploaded
[glibc](https://tracker.debian.org/glibc)/2.21-0experimental1 which will fix
the issue were [locales-all did not behave exactly like
locales](https://bugs.debian.org/788352) despite having it in the `Provides`
field.

Lunar rebased the `pu/reproducible_builds` branch for
[dpkg](https://tracker.debian.org/dpkg) on top of the released 1.18.2. This
made visible [an issue with `udeb`s and automatically generated debug
packages](https://bugs.debian.org/797391).

The [summary from the meeting at DebConf15 between ftpmasters, dpkg
mainatainers and reproducible builds
folks](http://lists.alioth.debian.org/pipermail/reproducible-builds/Week-of-
Mon-20150824/003008.html) has been posted to the revelant mailing lists.

## Packages fixed

The following 70 packages became reproducible due to changes in their build
dependencies: [activemq-activeio](https://tracker.debian.org/activemq-
activeio), [async-http-client](https://tracker.debian.org/async-http-client),
[classworlds](https://tracker.debian.org/classworlds),
[clirr](https://tracker.debian.org/clirr), [compress-
lzf](https://tracker.debian.org/compress-lzf),
[dbus-c++](https://tracker.debian.org/dbus-c++), [felix-
bundlerepository](https://tracker.debian.org/felix-bundlerepository), [felix-
framework](https://tracker.debian.org/felix-framework), [felix-gogo-
command](https://tracker.debian.org/felix-gogo-command), [felix-gogo-
runtime](https://tracker.debian.org/felix-gogo-runtime), [felix-gogo-
shell](https://tracker.debian.org/felix-gogo-shell), [felix-
main](https://tracker.debian.org/felix-main), [felix-shell-
tui](https://tracker.debian.org/felix-shell-tui), [felix-
shell](https://tracker.debian.org/felix-shell), [findbugs-
bcel](https://tracker.debian.org/findbugs-bcel),
[gco](https://tracker.debian.org/gco),
[gdebi](https://tracker.debian.org/gdebi),
[gecode](https://tracker.debian.org/gecode), [geronimo-
ejb-3.2-spec](https://tracker.debian.org/geronimo-ejb-3.2-spec), [git-
repair](https://tracker.debian.org/git-repair),
[gmetric4j](https://tracker.debian.org/gmetric4j), [gs-
collections](https://tracker.debian.org/gs-collections),
[hawtbuf](https://tracker.debian.org/hawtbuf),
[hawtdispatch](https://tracker.debian.org/hawtdispatch), [jack-
tools](https://tracker.debian.org/jack-tools), [jackson-dataformat-
cbor](https://tracker.debian.org/jackson-dataformat-cbor), [jackson-
dataformat-yaml](https://tracker.debian.org/jackson-dataformat-yaml),
[jackson-module-jaxb-annotations](https://tracker.debian.org/jackson-module-
jaxb-annotations), [jmxetric](https://tracker.debian.org/jmxetric), [json-
simple](https://tracker.debian.org/json-simple), [kryo-
serializers](https://tracker.debian.org/kryo-serializers),
[lhapdf](https://tracker.debian.org/lhapdf),
[libccrtp](https://tracker.debian.org/libccrtp),
[libclaw](https://tracker.debian.org/libclaw),
[libcommoncpp2](https://tracker.debian.org/libcommoncpp2),
[libftdi1](https://tracker.debian.org/libftdi1), [libjboss-marshalling-
java](https://tracker.debian.org/libjboss-marshalling-java),
[libmimic](https://tracker.debian.org/libmimic),
[libphysfs](https://tracker.debian.org/libphysfs), [libxstream-
java](https://tracker.debian.org/libxstream-java),
[limereg](https://tracker.debian.org/limereg), [maven-debian-
helper](https://tracker.debian.org/maven-debian-helper), [maven-
filtering](https://tracker.debian.org/maven-filtering), [maven-
invoker](https://tracker.debian.org/maven-invoker),
[mochiweb](https://tracker.debian.org/mochiweb), [mongo-java-
driver](https://tracker.debian.org/mongo-java-driver), [mqtt-
client](https://tracker.debian.org/mqtt-client),
[netty-3.9](https://tracker.debian.org/netty-3.9), [openhft-chronicle-
queue](https://tracker.debian.org/openhft-chronicle-queue), [openhft-
compiler](https://tracker.debian.org/openhft-compiler), [openhft-
lang](https://tracker.debian.org/openhft-lang),
[pavucontrol](https://tracker.debian.org/pavucontrol), [plexus-ant-
factory](https://tracker.debian.org/plexus-ant-factory), [plexus-
archiver](https://tracker.debian.org/plexus-archiver), [plexus-bsh-
factory](https://tracker.debian.org/plexus-bsh-factory), [plexus-
cdc](https://tracker.debian.org/plexus-cdc), [plexus-
classworlds2](https://tracker.debian.org/plexus-classworlds2), [plexus-
component-metadata](https://tracker.debian.org/plexus-component-metadata),
[plexus-container-default](https://tracker.debian.org/plexus-container-
default), [plexus-io](https://tracker.debian.org/plexus-io),
[pytone](https://tracker.debian.org/pytone),
[scolasync](https://tracker.debian.org/scolasync), [sisu-
ioc](https://tracker.debian.org/sisu-ioc), [snappy-
java](https://tracker.debian.org/snappy-java),
[spatial4j-0.4](https://tracker.debian.org/spatial4j-0.4),
[tika](https://tracker.debian.org/tika),
[treeline](https://tracker.debian.org/treeline),
[wss4j](https://tracker.debian.org/wss4j),
[xtalk](https://tracker.debian.org/xtalk),
[zshdb](https://tracker.debian.org/zshdb).

The following packages became reproducible after getting fixed:

  * [apr](https://tracker.debian.org/apr)/1.5.2-2 by Stefan Fritsch.
  * [binutils-m68hc1x](https://tracker.debian.org/binutils-m68hc1x)/1:2.18-6 by Santiago Vila.
  * [buxon](https://tracker.debian.org/buxon)/0.0.5-5 uploaded by Santiago Vila, [original patch](https://bugs.debian.org/777296) by Chris Lamb.
  * [cdtool](https://tracker.debian.org/cdtool)/2.1.8-release-3 by Santiago Vila.
  * [check](https://tracker.debian.org/check)/0.10.0-1 by Tobias Frost.
  * [ffe](https://tracker.debian.org/ffe)/0.3.4-2 by Santiago Vila.
  * [flowscan-cuflow](https://tracker.debian.org/flowscan-cuflow)/1.7-9 by Santiago Vila.
  * [gmt](https://tracker.debian.org/gmt)/5.1.2+dfsg1-2 by Bas Couwenberg.
  * [gtkspellmm](https://tracker.debian.org/gtkspellmm)/3.0.3+dfsg-2 by Philip Rinn.
  * [htp](https://tracker.debian.org/htp)/1.19-2 uploaded by Santiago Vila, [original patch](https://bugs.debian.org/795562) by Chris Lamb.
  * [igerman98](https://tracker.debian.org/igerman98)/20131206-6 by Roland Rosenfeld.
  * [intlfonts](https://tracker.debian.org/intlfonts)/1.2.1-9 uploaded by Santiago Vila, [original patch](https://bugs.debian.org/777012) by Chris Lamb.
  * [irda-utils](https://tracker.debian.org/irda-utils)/0.9.18-14 uploaded by Santiago Vila, [original patch](https://bugs.debian.org/777418) by Chris Lamb.
  * [jackd2](https://tracker.debian.org/jackd2)/1.9.10+20150825git1ed50c92~dfsg-1 uploaded by Adrian Knoth, [original patch](https://bugs.debian.org/796807) by Chris Lamb.
  * [jove](https://tracker.debian.org/jove)/4.16.0.73-4 by Cord Beermann.
  * [jquery](https://tracker.debian.org/jquery)/1.11.3+dfsg-1 uploaded by Antonio Terceiro, [original patch](https://bugs.debian.org/782899) by Reiner Herrmann.
  * [libapache2-authcookie-perl](https://tracker.debian.org/libapache2-authcookie-perl)/3.22-3 by Niko Tyni.
  * [libaqbanking](https://tracker.debian.org/libaqbanking)/5.6.1beta-2 [fixed](https://bugs.debian.org/792727) and uploaded by Micha Lenk.
  * [libcitygml](https://tracker.debian.org/libcitygml)/1.4.3-1 by Bas Couwenberg with a fixed new upstream release.
  * [libevhtp](https://tracker.debian.org/libevhtp)/1.2.10-3 by Vincent Bernat.
  * [libgnome2-perl](https://tracker.debian.org/libgnome2-perl)/1.046-2 by Niko Tyni.
  * [libmarc-charset-perl](https://tracker.debian.org/libmarc-charset-perl)/1.35-2 by Niko Tyni.
  * [libtime-y2038-perl](https://tracker.debian.org/libtime-y2038-perl)/20100403-5 by Niko Tyni.
  * [libxray-absorption-perl](https://tracker.debian.org/libxray-absorption-perl)/3.0.1-3 uploaded by gregor herrmann, [original patch](https://bugs.debian.org/796155) by Niko Tyno.
  * [lpc21isp](https://tracker.debian.org/lpc21isp)/1.97-2 by Agustin Henze.
  * [luakit](https://tracker.debian.org/luakit)/2012.09.13-r1-6 by Santiago Vila, also with a [patch](https://bugs.debian.org/792977) from akira.
  * [moarvm](https://tracker.debian.org/moarvm)/2015.07-1 by Daniel Dehennin with a fixed new upstream release.
  * [mosquitto](https://tracker.debian.org/mosquitto)/1.4.3-1 by Roger A. Light.
  * [ngircd](https://tracker.debian.org/ngircd)/22.1-2 by Christoph Biedl.
  * [nn](https://tracker.debian.org/nn)/6.7.3-10 uploaded by Cord Beermann, [original patch](https://bugs.debian.org/776764) by Chris Lamb.
  * [owncloud-client](https://tracker.debian.org/owncloud-client)/2.0.0~rc1+dfsg-1 by Sandro Knauß.
  * [postfix-gld](https://tracker.debian.org/postfix-gld)/1.7-7 uploaded by Santiago Vila, patches for [gzip](https://bugs.debian.org/777504) by Chris Lamb and [mtimes](https://bugs.debian.org/793710) by akira.
  * [pppconfig](https://tracker.debian.org/pppconfig)/2.3.22 by Santiago Vila.
  * [prometheus](https://tracker.debian.org/prometheus)/0.15.1+ds-2 by Martín Ferrari.
  * [python-xlrd](https://tracker.debian.org/python-xlrd)/0.9.4-1 by Vincent Bernat.
  * [recode](https://tracker.debian.org/recode)/3.6-22 by Santiago Vila.
  * [ruby-rmagick](https://tracker.debian.org/ruby-rmagick)/2.15.4-1 by Antonio Terceiro.
  * [scite](https://tracker.debian.org/scite)/3.6.0-1 by Antonio Valentino.
  * [smartlist](https://tracker.debian.org/smartlist)/3.15-25 by Santiago Vila.
  * [tar](https://tracker.debian.org/tar)/1.28-1 uploaded by Bdale Garbee, [original patch](https://bugs.debian.org/774463) by Reiner Herrman.
  * [transmissionrpc](https://tracker.debian.org/transmissionrpc)/0.11-2 uploaded by Vincent Bernat, [original patch](https://bugs.debian.org/788598) by Juan Picca.
  * [uruk](https://tracker.debian.org/uruk)/20150810-1 uploaded by Joost van Baal-Ilić, [original patch](https://bugs.debian.org/XXX) by Lunar.
  * [webassets](https://tracker.debian.org/webassets)/3:0.11-2 uploaded by Agustin Henze, [original patch](https://bugs.debian.org/775135) by Reiner Herrmann.
  * [xfig](https://tracker.debian.org/xfig)/1:3.2.5.c-5 by Roland Rosenfeld.
  * [xfonts-bolkhov](https://tracker.debian.org/xfonts-bolkhov)/1.1.20001007-8 by Santiago Vila.

Some uploads fixed some reproducibility issues but not all of them:

  * [cvs-buildpackage](https://tracker.debian.org/cvs-buildpackage)/5.24 uploaded by Santiago Vila, [original patch](https://bugs.debian.org/777301) by Chris Lamb.
  * [gcc-mingw-w64](https://tracker.debian.org/gcc-mingw-w64)/15.5 by Stephen Kitt.
  * [vtk6](https://tracker.debian.org/vtk6)/6.2.0+dfsg1-4 by Anton Gladky.

Patches submitted which have not made their way to the archive yet:

  * [#797027](https://bugs.debian.org/797027) on [zyne](https://tracker.debian.org/zyne) by Chris Lamb: switch to `pybuild` to get rid of `.pyc` files.
  * [#797180](https://bugs.debian.org/797180) on [python-doit](https://tracker.debian.org/python-doit) by Chris Lamb: sort output when creating completion script for bash and zsh.
  * [#797211](https://bugs.debian.org/797211) on [apt-dater](https://tracker.debian.org/apt-dater) by Chris Lamb: fix implementation of `SOURCE_DATE_EPOCH`.
  * [#797215](https://bugs.debian.org/797215) on [getdns](https://tracker.debian.org/getdns) by Chris Lamb: fix call to `dpkg-parsechangelog` in `debian/rules`.
  * [#797254](https://bugs.debian.org/797254) on [splint](https://tracker.debian.org/splint) by Chris Lamb: support `SOURCE_DATE_EPOCH` for version string.
  * [#797296](https://bugs.debian.org/797296) on [shiro](https://tracker.debian.org/shiro) by Chris Lamb: remove username from build string.
  * [#797408](https://bugs.debian.org/797408) on [splitpatch](https://tracker.debian.org/splitpatch) by Reiner Herrmann: use `SOURCE_DATE_EPOCH` to set manpage date.
  * [#797410](https://bugs.debian.org/797410) on [eigenbase-farrago](https://tracker.debian.org/eigenbase-farrago) by Reiner Herrmann: sets the comment style to `scm-safe` which tells `ResourceGen` that no timestamps should be included.
  * [#797415](https://bugs.debian.org/797415) on [apparmor](https://tracker.debian.org/apparmor) by Reiner Herrmann: sorting with the locale set to C. CAPABILITIES
  * [#797419](https://bugs.debian.org/797419) on [resiprocate](https://tracker.debian.org/resiprocate) by Reiner Herrmann: set the embedded hostname to a static value.
  * [#797427](https://bugs.debian.org/797427) on [jam](https:/tracker.debian.org/jam) by Reiner Herrmann: sorting with the locale set to C.
  * [#797430](https://bugs.debian.org/797430) on [ii-esu](https://tracker.debian.org/ii-esu) by Reiner Herrmann: sort source list using C locale.
  * [#797431](https://bugs.debian.org/797431) on [tatan](https://tracker.debian.org/tatan) by Reiner Herrmann: sort source list using C locale.

Chris Lamb also noticed that binaries shipped with [libsilo-
bin](https://packages.debian.org/libsilo-bin) [did not
work](https://bugs.debian.org/797414).

## Documentation update

Chris Lamb and Ximin Luo assembled a [proper specification for
SOURCE_DATE_EPOCH](https://reproducible-builds.org/specs/source-date-epoch/)
in the hope to convince more upstreams to adopt it. Thanks to Holger it is
published under a non-Debian domain name.

Lunar documented easiest way to solve issues with [file
ordering](https://wiki.debian.org/ReproducibleBuilds/FileOrderInTarballs) and
[timestamps](https://wiki.debian.org/ReproducibleBuilds/TimestampsInTarball)
in tarballs that came with [tar](https://tracker.debian.org/tar)/1.28-1.

Some
[examples](https://wiki.debian.org/ReproducibleBuilds/TimestampsProposal#Examples)
on how to use `SOURCE_DATE_EPOCH` have been improved to support systems
without GNU `date`.

## reproducible.debian.net

`armhf` is [finally being
tested](https://reproducible.debian.net/unstable/index_suite_armhf_stats.html),
which also means the remote building of Debian packages finally works! This
paves the way to perform the tests on even more architectures and doing
variations on CPU and date. Some packages even produce the same binary
`Arch:all` packages on different architectures
([1](https://reproducible.debian.net/buildinfo/unstable/amd64/ssh-import-
id_3.21-1_amd64.buildinfo),
[2](https://reproducible.debian.net/buildinfo/unstable/armhf/ssh-import-
id_3.21-1_armhf.buildinfo)). (h01ger)

[Tests for FreeBSD](https://reproducible.debian.net/freebsd/) are finally
running. (h01ger)

As it seems the gcc5 transition has cooled off, we schedule sid more often
than testing again on `amd64`. (h01ger)

[disorderfs](https://tracker.debian.org/disorderfs) has been built and
installed on all build nodes (`amd64` and `armhf`). One issue related to
permissions for root and unpriviliged users needs to be solved before
`disorderfs` can be used on
[reproducible.debian.net](https://reproducible.debian.net/). (h01ger)

## strip-nondeterminism

Version 0.011-1 has been released on August 29th. The new version updates
`dh_strip_nondeterminism` to match recent changes in `debhelper`. (Andrew
Ayer)

## disorderfs

[disorderfs](https://tracker.debian.org/disorderfs), the new FUSE filesystem
to ease testing of filesystem-related variations, is now almost ready to be
used. Version 0.2.0 adds support for extended attributes. Since then Andrew
Ayer also added support to reverse directory entries instead of shuffling
them, and arbitrary padding to the number of blocks used by files.

## Package reviews

142 [reviews](https://reproducible.debian.net/unstable/amd64/index_notes.html)
have been removed, 48 added and 259 updated this week.

Santiago Vila renamed the `not_using_dh_builddeb` issue into
[varying_mtimes_in_data_tar_gz_or_control_tar_gz](https://reproducible.debian.net/issues/unstable/varying_mtimes_in_data_tar_gz_or_control_tar_gz_issue.html)
to align better with other tag names.

New issue identified this week:
[random_order_in_python_doit_completion](https://reproducible.debian.net/issues/unstable/random_order_in_python_doit_completion_issue.html).

37 FTBFS issues have been reported by Chris West (Faux) and Chris Lamb.

## Misc.

h01ger gave a talk at [FrOSCon](https://www.froscon.de/) on August 23rd.
[Recordings](https://media.ccc.de/browse/conferences/froscon/2015/froscon2015-1614-the_long_road_to_reproducible_builds.html)
are already online.

_These reports are being reviewed and enhanced every week by many people
hanging out on`#debian-reproducible`. Huge thanks!_

