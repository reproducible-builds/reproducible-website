---
layout: blog
week: 12
published: 2015-07-26 17:41:53
---

What happened in the [reproducible
builds](https://wiki.debian.org/ReproducibleBuilds) effort this week:

## Toolchain fixes

Eric Dorlan uploaded
[automake-1.15](https://tracker.debian.org/automake-1.15)/1:1.15-2 which makes
the output of `mdate-sh` deterministic. [Original
patch](https://bugs.debian.org/782345) by Reiner Herrmann.

Kenneth J. Pronovici uploaded
[epydoc](https://tracker.debian.org/epydoc)/3.0.1+dfsg-8 which now honors
[SOURCE_DATE_EPOCH](https://wiki.debian.org/ReproducibleBuilds/TimestampsProposal).
[Original patch](https://bugs.debian.org/790899) by Reiner Herrmann.

Chris Lamb [submitted a patch](https://bugs.debian.org/792436) to [dh-
python](https://tracker.debian.org/dh-python) to make the order of the
generated maintainer scripts deterministic. Chris also [offered a
fix](https://bugs.debian.org/792491) for a source of non-determinism in [dpkg-
shlibdeps](https://manpage.debian.org/dpkg-shlibdeps) when packages have
alternative dependencies.

Dhole [provided a patch](https://bugs.debian.org/792687) to add support for
`SOURCE_DATE_EPOCH` to [gettext](https://tracker.debian.org/gettext).

## Packages fixed

The following 78 packages became reproducible in our setup due to changes in
their build dependencies: [chemical-mime-
data](https://tracker.debian.org/chemical-mime-data), [clojure-
contrib](https://tracker.debian.org/clojure-contrib), [cobertura-maven-
plugin](https://tracker.debian.org/cobertura-maven-plugin),
[cpm](https://tracker.debian.org/cpm),
[davical](https://tracker.debian.org/davical), [debian-security-
support](https://tracker.debian.org/debian-security-support),
[dfc](https://tracker.debian.org/dfc),
[diction](https://tracker.debian.org/diction),
[dvdwizard](https://tracker.debian.org/dvdwizard),
[galternatives](https://tracker.debian.org/galternatives), [gentlyweb-
utils](https://tracker.debian.org/gentlyweb-utils),
[gifticlib](https://tracker.debian.org/gifticlib),
[gmtkbabel](https://tracker.debian.org/gmtkbabel), [gnuplot-
mode](https://tracker.debian.org/gnuplot-mode),
[gplanarity](https://tracker.debian.org/gplanarity),
[gpodder](https://tracker.debian.org/gpodder), [gtg-
trace](https://tracker.debian.org/gtg-trace),
[gyoto](https://tracker.debian.org/gyoto),
[highlight.js](https://tracker.debian.org/highlight.js),
[htp](https://tracker.debian.org/htp), [ibus-
table](https://tracker.debian.org/ibus-table),
[impressive](https://tracker.debian.org/impressive),
[jags](https://tracker.debian.org/jags), [jansi-
native](https://tracker.debian.org/jansi-native), [jnr-
constants](https://tracker.debian.org/jnr-constants),
[jthread](https://tracker.debian.org/jthread),
[jwm](https://tracker.debian.org/jwm), [khronos-
api](https://tracker.debian.org/khronos-api), [latex-coffee-
stains](https://tracker.debian.org/latex-coffee-stains), [latex-
make](https://tracker.debian.org/latex-make),
[latex2rtf](https://tracker.debian.org/latex2rtf),
[latexdiff](https://tracker.debian.org/latexdiff),
[libcrcutil](https://tracker.debian.org/libcrcutil),
[libdc0](https://tracker.debian.org/libdc0),
[libdc1394-22](https://tracker.debian.org/libdc1394-22),
[libidn2-0](https://tracker.debian.org/libidn2-0),
[libint](https://tracker.debian.org/libint), [libjava-jdbc-
clojure](https://tracker.debian.org/libjava-jdbc-clojure), [libkryo-
java](https://tracker.debian.org/libkryo-java), [libphone-ui-
shr](https://tracker.debian.org/libphone-ui-shr), [libpicocontainer-
java](https://tracker.debian.org/libpicocontainer-java),
[libraw1394](https://tracker.debian.org/libraw1394), [librostlab-
blast](https://tracker.debian.org/librostlab-blast),
[librostlab](https://tracker.debian.org/librostlab),
[libshevek](https://tracker.debian.org/libshevek),
[libstxxl](https://tracker.debian.org/libstxxl), [libtools-logging-
clojure](https://tracker.debian.org/libtools-logging-clojure), [libtools-
macro-clojure](https://tracker.debian.org/libtools-macro-clojure),
[litl](https://tracker.debian.org/litl),
[londonlaw](https://tracker.debian.org/londonlaw),
[ltsp](https://tracker.debian.org/ltsp),
[macsyfinder](https://tracker.debian.org/macsyfinder),
[mapnik](https://tracker.debian.org/mapnik), [maven-compiler-
plugin](https://tracker.debian.org/maven-compiler-plugin),
[mc](https://tracker.debian.org/mc),
[microdc2](https://tracker.debian.org/microdc2),
[miniupnpd](https://tracker.debian.org/miniupnpd),
[monajat](https://tracker.debian.org/monajat),
[navit](https://tracker.debian.org/navit),
[pdmenu](https://tracker.debian.org/pdmenu),
[pirl](https://tracker.debian.org/pirl),
[plm](https://tracker.debian.org/plm), [scikit-
learn](https://tracker.debian.org/scikit-learn), [snp-
sites](https://tracker.debian.org/snp-sites), [sra-
sdk](https://tracker.debian.org/sra-sdk),
[sunpinyin](https://tracker.debian.org/sunpinyin),
[tilda](https://tracker.debian.org/tilda), [vdr-plugin-
dvd](https://tracker.debian.org/vdr-plugin-dvd), [vdr-plugin-
epgsearch](https://tracker.debian.org/vdr-plugin-epgsearch), [vdr-plugin-
remote](https://tracker.debian.org/vdr-plugin-remote), [vdr-plugin-
spider](https://tracker.debian.org/vdr-plugin-spider), [vdr-plugin-
streamdev](https://tracker.debian.org/vdr-plugin-streamdev), [vdr-plugin-
sudoku](https://tracker.debian.org/vdr-plugin-sudoku), [vdr-plugin-
xineliboutput](https://tracker.debian.org/vdr-plugin-xineliboutput),
[veromix](https://tracker.debian.org/veromix),
[voxbo](https://tracker.debian.org/voxbo),
[xaos](https://tracker.debian.org/xaos),
[xbae](https://tracker.debian.org/xbae).

The following packages became reproducible after getting fixed:

  * [analog](https://tracker.debian.org/analog)/2:6.0-21 uploaded by Andreas Beckmann, [original patch](https://bugs.debian.org/788752) by Dhole.
  * [base-passwd](https://tracker.debian.org/base-passwd)/3.5.38 uploaded by Colin Watson, [original patch](https://bugs.debian.org/792520) by Juan Picca.
  * [debconf](https://tracker.debian.org/debconf)/1.5.57 uploaded by Colin Watson, [original patch](https://bugs.debian.org/783255) by Lunar.
  * [ipband](https://tracker.debian.org/ipband)/0.8.1-4 by Mats Erik Andersson.
  * [kfreebsd-10](https://tracker.debian.org/kfreebsd-10)/10.1~svn274115-7 by Steven Chamberlain.
  * [libcommons-cli-java](https://tracker.debian.org/libcommons-cli-java)/1.3.1-1 by tony mancill.
  * [libpsl](https://tracker.debian.org/libpsl)/0.7.1-1 by Daniel Kahn Gillmor.
  * [maven-archiver](https://tracker.debian.org/maven-archiver)/2.6-3 by Emmanuel Bourg.
  * [mtink](https://tracker.debian.org/mtink)/1.0.16-9 by Graham Inggs.
  * [ocamlweb](https://tracker.debian.org/ocamlweb)/1.39-2 uploaded by Mehdi Dogguy, [original patch](https://bugs.debian.org/776643) by Chris Lamb.
  * [rbdoom3bfg](https://tracker.debian.org/rbdoom3bfg)/1.0.3+repack1+git20150625-1 by Tobias Frost.
  * [spatialite-tools](https://tracker.debian.org/spatialite-tools)/4.2.1~rc1-2 by Bas Couwenberg.
  * [task](https://tracker.debian.org/task)/2.4.4+dfsg-1 by Sebastien Badia.

Some uploads fixed some reproducibility issues but not all of them:

  * [bullet](https://tracker.debian.org/bullet)/2.83.4+dfsg-1 by Markus Koschany.
  * [cdo](https://tracker.debian.org/cdo)/1.6.6+dfsg.1-2 by Alastair McKinstry.
  * [fish](https://tracker.debian.org/fish)/2.2.0-1 uploaded by Tristan Seligmann, [original patch](https://bugs.debin.org/791648) by Chris Lamb.
  * [sympy](https://tracker.debian.org/sympy)/0.7.6-3 by Sergey B Kirpichev.
  * [xtables-addons](https://tracker.debian.org/xtables-addons)/2.7-1 uploaded by Dmitry Smirnov, [original patch](https://bugs.debian.org/776879) by Reiner Herrmann.

Patches submitted which have not made their way to the archive yet:

  * [#792178](https://bugs.debian.org/792178) on [gunroar](https://tracker.debian.org/gunroar) by Reiner Herrmann: use C locale when sorting source files.
  * [#792181](https://bugs.debian.org/792181) on [tth](https://tracker.debian.org/tth) by Reiner Herrmann: remove timestamps from generated HTML files.
  * [#792285](https://bugs.debian.org/792285) on [pkgconf](https://tracker.debian.org/pkgconf) by Juan Picca: set `LC_ALL=C` when running `sort`.
  * [#792319](https://bugs.debian.org/792319) on [jsmath-fonts](https://tracker.debian.org/jsmath-fonts) by Chris Lamb: set `TZ=UTC` when calling `unzip`.
  * [#792424](https://bugs.debian.org/792424) on [swh-plugins](https://tracker.debian.org/swh-plugins) by Chris Lamb: sort inputs in `Makefile`.
  * [#792525](https://bugs.debian.org/792525) on [ruby-standalone](https://tracker.debian.org/ruby-standalone) by Reiner Herrmann: use UTC and C locale when formatting the manpage date for the documentation.
  * [#792528](https://bugs.debian.org/792528) on [dict-foldoc](https://tracker.debian.org/dict-foldoc) by Reiner Herrmann: use C locale when formatting the date for the documentation.
  * [#792529](https://bugs.debian.org/792529) on [tomatoes](https://tracker.debian.org/tomatoes) by Reiner Herrmann: use date from `debian/changelog` in version string.
  * [#792593](https://bugs.debian.org/792593) on [lives](https://tracker.debian.org/lives) by Dhole: process a Perl hash in stable order.
  * [#792596](https://bugs.debian.org/792596) on [jsmath](https://tracker.debian.org/jsmath) by Dhole: set `TZ=UTC` when calling `unzip`.
  * [#792597](https://bugs.debian.org/792597) on [jsmath-fonts-sprite](https://tracker.debian.org/jsmath-fonts-sprite) by Dhole: set `TZ=UTC` when calling `unzip`.
  * [#792598](https://bugs.debian.org/792598) on [libreoffice-canzeley-client](https://tracker.debian.org/libreoffice-canzeley-client) by Dhole: set `TZ=UTC` when calling `unzip`.
  * [#792599](https://bugs.debian.org/792599) on [openthesaurus](https://tracker.debian.org/openthesaurus) by Dhole: set `TZ=UTC` when calling `unzip`.
  * [#792602](https://bugs.debian.org/792602) on [fonts-stix](https://tracker.debian.org/fonts-stix) by Dhole: set `TZ=UTC` when calling `unzip`.
  * [#792667](https://bugs.debian.org/792667) on [jack-audio-connection-kit](https://tracker.debian.org/jack-audio-connection-kit) by use date from `debian/changelog` in manpages.
  * [#792668](https://bugs.debian.org/792668) on [pyhoca-gui](https://tracker.debian.org/pyhoca-gui) by remove date from package version number.
  * [#792671](https://bugs.debian.org/792671) on [apertium-dbus](https://tracker.debian.org/apertium-dbus) by remove `*.pyo` and `*.pyc` from binary package.
  * [#792673](https://bugs.debian.org/792673) on [bup](https://tracker.debian.org/bup) by use date from `debian/changelog` when generating version strings.
  * [#792684](https://bugs.debian.org/792684) on [cain](https://tracker.debian.org/cain) by Chris Lamb: ensure stable permissions when creating source tarball.
  * [#792709](https://bugs.debian.org/792709) on [dict-jargon](https://tracker.debian.org/dict-jargon) by Dhole: set timestamp in archive using the latest entry of `debian/changelog`.
  * [#792727](https://bugs.debian.org/792727) on [libaqbanking](https://tracker.debian.org/libaqbanking) by Micha Lenk (upstream): sort source files in documentation.
  * [#792763](https://bugs.debian.org/792763) on [docbook-dsssl](https://tracker.debian.org/docbook-dsssl) by Chris Lamb: sort input files when creating changelog.
  * [#792770](https://bugs.debian.org/792770) on [lynx-cur](https://tracker.debian.org/lynx-cur) by Reiner Herrmann: use C locale when sorting configuration files.
  * [#792771](https://bugs.debian.org/792771) on [mu-cade](https://tracker.debian.org/mu-cade) by Reiner Herrmann: use C locale when sorting source files.
  * [#792772](https://bugs.debian.org/792772) on [titanion](https://tracker.debian.org/titanion) by Reiner Herrmann: use C locale when sorting source files.
  * [#792783](https://bugs.debian.org/792783) on [linuxlogo](https://tracker.debian.org/linuxlogo) by Reiner Herrmann: use C locale when sorting source files.
  * [#792821](https://bugs.debian.org/792821) on [pkg-config](https://tracker.debian.org/pkg-config) by Juan Picca: use C locale when sorting source files.
  * [#792828](https://bugs.debian.org/792828) on [tiger](https://tracker.debian.org/tiger) by Daniel Kahn Gillmor: use C locale when listing soure files.

## reproducible.debian.net

The statistics on the [main page of
reproducible.debian.net](https://reproducible.debian.net/reproducible.html)
are now updated every five minutes. A random unreviewed package is suggested
in the “look at a package” form on every build. (h01ger)

A [new package
set](https://reproducible.debian.net/unstable/amd64/pkg_set_cii-census.html)
based new on the [Core Internet Infrastructure
census](https://github.com/linuxfoundation/cii-census) has been added.
(h01ger)

Testing of FreeBSD has started, though no results yet. More [details have been
posted](http://lists.freebsd.org/pipermail/freebsd-
hackers/2015-July/047997.html) to the `freebsd-hackers` mailing list. The
build is run on a new virtual machine running FreeBSD 10.1 with 3 cores and 6
GB of RAM, also sponsored by [Profitbricks](https://www.profitbricks.co.uk/).

## strip-nondeterminism development

Andrew Ayer released version 0.009 of [strip-
nondeterminism](https://packages.debian.org/strip-nondeterminism). The new
version will strip locales from Javadoc, include the name of files causing
errors, and [ignore unhandled (but rare) zip64
archives](https://bugs.debian.org/791574).

## debbindiff development

Lunar continued its major refactoring to enhance code reuse and pave the way
to fuzzy-matching and parallel processing. Most file comparators have now been
converted to the new class hierarchy.

In order to support for archive formats, work has started on packaging [Python
bindings for libarchive](https://github.com/Changaco/python-libarchive-c).
While getting support for more archive formats with a common interface is very
nice, [libarchive](http://www.libarchive.org/) is a stream oriented library
and might have bad performance with how debbindiff currently works. Time will
tell if better solutions need to be found.

## Documentation update

Lunar started a [Reproducible builds
HOWTO](https://anonscm.debian.org/cgit/reproducible/reproducible-builds-
howto.git) intended to explain the different aspects of making software build
reproducibly to the different audiences that might have to get involved like
software authors, producers of binary packages, and distributors.

## Package reviews

17 obsolete
[reviews](https://reproducible.debian.net/unstable/amd64/index_notes.html)
have been removed, 212 added and 46 updated this week.

15 new bugs for packages failing to build from sources have been reported by
Chris West (Faux), and Mattia Rizzolo.

## Presentations

Lunar presented Debian efforts and some recipes on making software build
reproducibly at [Libre Software Meeting
2015](https://2015.rmll.info/reproducible-builds-in-debian-and-
everywhere?lang=en).
[Slides](https://reproducible.alioth.debian.org/presentations/2015-07-06-LSM2015.pdf)
and a [video
recording](https://rmll.ubicast.tv/permalink/v1253b3db528fcf0ch3p/) are
available.

## Misc.

h01ger, dkg, and Lunar attended a [Core Infrastructure
Initiative](https://www.coreinfrastructure.org/) meeting. The progress and
tools mode for the Debian efforts were shown. Several discussions also helped
getting a better understanding of the needs of other free software projects
regarding reproducible builds. The idea of a global append only log, similar
to the logs used for [Certificate
Transparency](https://en.wikipedia.org/wiki/Certificate_Transparency), came up
on multiple occasions. Using such append only logs for keeping records of
sources and build results has gotten the name “Binary Transparency Logs”. They
would at least help identifying a compromised software signing key. Whether
the benefits in using such logs justify the costs need more research.

