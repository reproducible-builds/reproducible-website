---
layout: blog
week: 45
published: 2016-03-10 21:48:20
---

What happened in the [reproducible
builds](https://wiki.debian.org/ReproducibleBuilds) effort between February
28th and March 5th:

## Toolchain fixes

  * Antonio Terceiro uploaded [gem2deb](https://tracker.debian.org/gem2deb)/0.27 that forces generated _gemspecs_ to use the date from `debian/changelog`.
  * Antonio Terceiro uploaded [gem2deb](https://tracker.debian.org/gem2deb)/0.28 that forces generated _gemspecs_ to have their contains file lists sorted.
  * Robert Luberda uploaded [ispell](https://tracker.debian.org/ispell)/3.4.00-5 which make builds of hashes reproducible.
  * Cédric Boutillier uploaded [ruby-ronn](https://tracker.debian.org/ruby-ronn)/0.7.3-4 which will make the output locale agnostic. [Original patch](https://bugs.debian.org/807201) by Chris Lamb.
  * Markus Koschany uploaded [spring](https://tracker.debian.org/spring)/101.0+dfsg-1. Fixed by Alexandre Detiste.

Ximin Luo [resubmitted](https://savannah.gnu.org/patch/?8925) the patch adding
the `--clamp-mtime` option to Tar on Savannah's bug tracker.

Lunar
[rebased](https://anonscm.debian.org/cgit/reproducible/dpkg.git/log/?h=pu/reproducible_builds)
our experimental [dpkg](https://tracker.debian.org/dpkg) on top of the current
master branch. Changes in the test infrastructure [are
required](https://lists.alioth.debian.org/pipermail/reproducible-builds/Week-
of-Mon-20160229/004950.html) before uploading a new version to [our
experimental
repository](https://wiki.debian.org/ReproducibleBuilds/ExperimentalToolchain).

Reiner Herrmann rebased our custom [texlive-
bin](https://tracker.debian.org/texlive-bin) against the latest uploaded
version.

## Packages fixed

The following 77 packages have become reproducible due to changes in their
build dependencies: [asciidoctor](https://tracker.debian.org/asciidoctor),
[atig](https://tracker.debian.org/atig), [fuel-
astute](https://tracker.debian.org/fuel-astute),
[jekyll](https://tracker.debian.org/jekyll), [libphone-ui-
shr](https://tracker.debian.org/libphone-ui-shr),
[linkchecker](https://tracker.debian.org/linkchecker), [maven-plugin-
testing](https://tracker.debian.org/maven-plugin-testing), [node-
iscroll](https://tracker.debian.org/node-iscroll), [origami-
pdf](https://tracker.debian.org/origami-pdf), [plexus-
digest](https://tracker.debian.org/plexus-digest),
[pry](https://tracker.debian.org/pry), [python-
avro](https://tracker.debian.org/python-avro), [python-
odf](https://tracker.debian.org/python-odf),
[rails](https://tracker.debian.org/rails), [ruby-actionpack-xml-
parser](https://tracker.debian.org/ruby-actionpack-xml-parser), [ruby-active-
model-serializers](https://tracker.debian.org/ruby-active-model-serializers),
[ruby-activerecord-session-store](https://tracker.debian.org/ruby-
activerecord-session-store), [ruby-api-
pagination](https://tracker.debian.org/ruby-api-pagination), [ruby-
babosa](https://tracker.debian.org/ruby-babosa), [ruby-
carrierwave](https://tracker.debian.org/ruby-carrierwave), [ruby-classifier-
reborn](https://tracker.debian.org/ruby-classifier-reborn), [ruby-
compass](https://tracker.debian.org/ruby-compass), [ruby-
concurrent](https://tracker.debian.org/ruby-concurrent), [ruby-
configurate](https://tracker.debian.org/ruby-configurate), [ruby-
crack](https://tracker.debian.org/ruby-crack), [ruby-css-
parser](https://tracker.debian.org/ruby-css-parser), [ruby-cucumber-
rails](https://tracker.debian.org/ruby-cucumber-rails), [ruby-
delorean](https://tracker.debian.org/ruby-delorean), [ruby-
encryptor](https://tracker.debian.org/ruby-encryptor), [ruby-
fakeweb](https://tracker.debian.org/ruby-fakeweb), [ruby-
flexmock](https://tracker.debian.org/ruby-flexmock), [ruby-fog-
vsphere](https://tracker.debian.org/ruby-fog-vsphere), [ruby-
gemojione](https://tracker.debian.org/ruby-gemojione), [ruby-
git](https://tracker.debian.org/ruby-git), [ruby-
grack](https://tracker.debian.org/ruby-grack), [ruby-
htmlentities](https://tracker.debian.org/ruby-htmlentities), [ruby-jekyll-
feed](https://tracker.debian.org/ruby-jekyll-feed), [ruby-json-
schema](https://tracker.debian.org/ruby-json-schema), [ruby-
listen](https://tracker.debian.org/ruby-listen), [ruby-
markerb](https://tracker.debian.org/ruby-markerb), [ruby-
mathml](https://tracker.debian.org/ruby-mathml), [ruby-mini-
magick](https://tracker.debian.org/ruby-mini-magick), [ruby-net-
telnet](https://tracker.debian.org/ruby-net-telnet), [ruby-omniauth-azure-
oauth2](https://tracker.debian.org/ruby-omniauth-azure-oauth2), [ruby-
omniauth-saml](https://tracker.debian.org/ruby-omniauth-saml), [ruby-
org](https://tracker.debian.org/ruby-org), [ruby-
origin](https://tracker.debian.org/ruby-origin), [ruby-
prawn](https://tracker.debian.org/ruby-prawn), [ruby-
pygments.rb](https://tracker.debian.org/ruby-pygments.rb), [ruby-
raemon](https://tracker.debian.org/ruby-raemon), [ruby-rails-deprecated-
sanitizer](https://tracker.debian.org/ruby-rails-deprecated-sanitizer), [ruby-
raindrops](https://tracker.debian.org/ruby-raindrops), [ruby-
rbpdf](https://tracker.debian.org/ruby-rbpdf), [ruby-
rbvmomi](https://tracker.debian.org/ruby-rbvmomi), [ruby-
recaptcha](https://tracker.debian.org/ruby-recaptcha), [ruby-
ref](https://tracker.debian.org/ruby-ref), [ruby-
responders](https://tracker.debian.org/ruby-responders), [ruby-
rjb](https://tracker.debian.org/ruby-rjb), [ruby-rspec-
rails](https://tracker.debian.org/ruby-rspec-rails), [ruby-
rspec](https://tracker.debian.org/ruby-rspec), [ruby-rufus-
scheduler](https://tracker.debian.org/ruby-rufus-scheduler), [ruby-sass-
rails](https://tracker.debian.org/ruby-sass-rails), [ruby-
sass](https://tracker.debian.org/ruby-sass), [ruby-sentry-
raven](https://tracker.debian.org/ruby-sentry-raven), [ruby-sequel-
pg](https://tracker.debian.org/ruby-sequel-pg), [ruby-
sequel](https://tracker.debian.org/ruby-sequel), [ruby-
settingslogic](https://tracker.debian.org/ruby-settingslogic), [ruby-shoulda-
matchers](https://tracker.debian.org/ruby-shoulda-matchers), [ruby-slack-
notifier](https://tracker.debian.org/ruby-slack-notifier), [ruby-
symboltable](https://tracker.debian.org/ruby-symboltable), [ruby-
timers](https://tracker.debian.org/ruby-timers), [ruby-
zip](https://tracker.debian.org/ruby-zip),
[ticgit](https://tracker.debian.org/ticgit),
[tmuxinator](https://tracker.debian.org/tmuxinator),
[vagrant](https://tracker.debian.org/vagrant),
[wagon](https://tracker.debian.org/wagon),
[yard](https://tracker.debian.org/yard).

The following packages became reproducible after getting fixed:

  * [air-quality-sensor](https://tracker.debian.org/air-quality-sensor)/0.1.4-1 uploaded by Benedikt Wildenhain, fixed upstream, [original patch](https://bugs.debian.org/809025) by Chris Lamb.
  * [device3dfx](https://tracker.debian.org/device3dfx)/2013.08.08-4 by Guillem Jover.
  * [fldigi](https://tracker.debian.org/fldigi)/3.23.08-1 by Kamal Mostafa.
  * [fltk1.1](https://tracker.debian.org/fltk1.1)/1.1.10-22 by Aaron M. Ucko.
  * [freeimage](https://tracker.debian.org/freeimage)/3.17.0+ds1-2 by Ghislain Antony Vaillant.
  * [gimagereader](https://tracker.debian.org/gimagereader)/3.1.2+git368fa8f-2 by Philip Rinn.
  * [ginkgocadx](https://tracker.debian.org/ginkgocadx)/3.7.5-1 by Gert Wollny, [fixed upstream](https://github.com/gerddie/ginkgocadx/commit/67785f641f4581b3c3586288fb52491f8589ccdc).
  * [jadetex](https://tracker.debian.org/jadetex)/3.13-17 by Norbert Preining.
  * [opensips](https://tracker.debian.org/opensips)/2.1.2-1 by Razvan Crainea.
  * [ruby-sqlite3](https://tracker.debian.org/ruby-sqlite3)/1.3.11-2 uploaded by Cédric Boutillier, [original patch](https://bugs.debian.org/782884) by Lunar.
  * [runawk](https://tracker.debian.org/runawk)/1.6.0-2 uploaded by Andrew Shadura, [patch](https://bugs.debian.org/816206) by Reiner Herrmann.
  * [systraq](https://tracker.debian.org/systraq)/20160303-1 by Joost van Baal-Ilić.

Some uploads fixed some reproducibility issues, but not all of them:

  * [auto-multiple-choice](https://tracker.debian.org/auto-multiple-choice)/1.2.1-4 by Georges Khaznadar.
  * [avfs](https://tracker.debian.org/avfs)/1.0.3-1 uploaded by Michael Meskes, [original patch](https://bugs.debian.org/810259) by Chris Lamb.
  * [console-setup](https://tracker.debian.org/console-setup)/1.138 uploaded by Anton Zinoviev, [original patch](https://bugs.debian.org/775531) by Reiner Herrmann.
  * [gromacs](https://tracker.debian.org/gromacs)/5.1.2-1 by Nicholas Breen.
  * [mrrescue](https://tracker.debian.org/mrrescue)/1.02c-2 by Alexandre Detiste.
  * [usb-modeswitch-data](https://tracker.debian.org/usb-modeswitch-data)/20160112-2 by Didier Raboud.

Patches submitted which have not made their way to the archive yet:

  * [#816209](https://bugs.debian.org/816209) on [elog](https://tracker.debian.org/elog) by Reiner Herrmann: use `printf` instead of `echo` which is shell-independent.
  * [#816214](https://bugs.debian.org/816214) on [python-pip](https://tracker.debian.org/python-pip) by Reiner Herrmann: removes timestamp from generated Python scripts.
  * [#816230](https://bugs.debian.org/816230) on [rows](https://tracker.debian.org/rows) by Reiner Herrmann: tell grep to always treat the input as text.
  * [#816232](https://bugs.debian.org/816232) on [eficas](https://tracker.debian.org/eficas) by Reiner Herrmann: use `printf` instead of `echo` which is shell-independent.

Florent Daigniere and bancfc reported that [linux-
grsec](https://tracker.debian.org/linux-grsec) was currently built with
`GRKERNSEC_RANDSTRUCT` which will prevent reproducible builds with the current
packaging.

## tests.reproducible-builds.org

[pbuilder](https://tracker.debian.org/pbuilder) has been updated to the last
version to be able to support `Build-Depends-Arch` and `Build-Conflicts-Arch`.
(Mattia Rizzolo, h01ger)

New package sets have been added for [Subgraph
OS](https://subgraph.com/sgos/), which is based on Debian Stretch:
[packages](https://tests.reproducible-
builds.org/testing/amd64/pkg_set_subgraph_OS.html) and [build
dependencies](https://tests.reproducible-
builds.org/testing/amd64/pkg_set_subgraph_OS_build-depends.html). (h01ger)

Two new `armhf` build nodes have been added (thanks Vagrant Cascadian) and
integrated in our Jenkins setup with 8 new `armhf` builder jobs. (h01ger)

## strip-nondeterminism development

[strip-nondeterminism](https://tracker.debian.org/strip-nondeterminism)
version 0.016-1 was [released](https://tracker.debian.org/news/751262) on
Sunday 28th. It will now normalize the `POT-Creation-Date` field in GNU
Gettext `.mo` files. (Reiner Herrmann) Several improvements to the packages
metadata have also been made. (h01ger, Ben Finney)

## Package reviews

185 [reviews](https://reproducible.debian.net/unstable/amd64/index_notes.html)
have been removed, 91 added and 33 updated in the previous week.

New issue:
[fileorder_in_gemspec_files_list](https://reproducible.debian.net/issues/unstable/fileorder_in_gemspec_files_list_issue.html).

43 FTBFS bugs were reported by Chris Lamb, Martin Michlmayr, and gregor
herrmann.

## Misc.

After merging the [patch from Dhiru
Kholia](https://bugzilla.redhat.com/show_bug.cgi?id=1288713) adding support
for `SOURCE_DATE_EPOCH` in [rpm](http://rpm.org/), Florian Festi [opened a
discussion](http://lists.rpm.org/pipermail/rpm-
ecosystem/2016-March/000308.html) on the [rpm-ecosystem mailing
list](http://lists.rpm.org/mailman/listinfo/rpm-ecosystem) about reproducible
builds.

On March 4th, Lunar gave an [overview of the general reproducible builds
effort](https://internetfreedomfestival.org/wiki/index.php/Reproducible_Builds)
at the [Internet Freedom Festival](https://internetfreedomfestival.org/) in
Valencia.

