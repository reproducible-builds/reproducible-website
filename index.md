---
layout: default
title: Home
title_head: Reproducible Builds &mdash; a set of software development practices that create an independently-verifiable path from source to binary code
order: 10
permalink: /
---

<div class="hero mb-4">
    <div class="container text-center">
        <div class="col-sm-8 offset-sm-2">
            <a href="{{ "/" | relative_url }}">
                <img class="mb-4 img-fluid" src="{{ "/assets/images/logo-text-white.png" | relative_url }}" alt="Reproducible Builds" />
            </a>
            <p class="lead mt-5 pt-5 pb-5">
                <strong>Reproducible builds</strong> are a set of software development
                practices that create an independently-verifiable path from source
                to binary&nbsp;code.
                <small class="d-none d-sm-inline">
                    (<a href="{{ "/docs/definition/" | relative_url }}">Find out more</a>)
                </small>
            </p>
        </div>
    </div>
</div>

<div class="row justify-content-center">
  <h2 class="text-center mb-4">Why Reproducible Builds Matter</h2>
  <p class="mb-4"><strong>In short: </strong>Reproducible Builds provide certainty that software is genuine and has not been tampered with.</p>
  <div class="row justify-content-center">
    {% for card in site.data.reasons-for-rb.cards %}
    <div class="col-lg-4 col-md-6 col-sm-12 mb-4 d-flex">
      <div class="card h-100 shadow-sm">
        <div class="card-body">
          <div
            class="d-flex justify-content-center align-items-center p-3 rounded-circle mx-auto tinted-icon"
          >
            {{ card.emoji }}
          </div>
          <h3 class="card-title mt-3">{{ card.title }}</h3>
          <p class="card-text text-left">{{ card.description | markdownify }}</p>
        </div>
      </div>
    </div>
    {% endfor %}
  </div>
</div>

<div  class="row justify-content-center">
  <h2 class="text-center mb-5">Reproducible Builds and You</h2>
  <div class="row justify-content-start offset-boxes">
    {% for card in site.data.user-stories.cards %}
    <div class="col-lg-6 col-md-6 col-sm-12 mb-4 d-flex">
      <div class="card h-100 shadow-sm">
        <div class="card-body">
          <h3 class="card-title mt-0">{{ card.title }}</h3>
          <p class="card-text text-left">{{ card.description | markdownify }}</p>
          <p class="card-text text-left"><strong>{{ card.cta | markdownify }}</strong></p>
        </div>
      </div>
    </div>
    {% endfor %}
  </div>
  <a href="/docs/which-problems-do-reproducible-builds-solve/" class="big-cta card">
    <div class="card-body">
        <div class="cta-text"><strong>Protect developers, safeguard privacy, and ensure trust in software.</strong>Discover how Reproducible Builds help you defend against threats and empower secure collaboration.</div>
        <div class="point"></div>
    </div>
  </a>
</div>

<div class="container my-5">
    <div class="row">
        <!-- How section -->
        <div class="col-md-8">
            <h2 class="mb-4">How does it work?</h2>
            <p>
                First, the <strong>build system</strong> needs to be made entirely deterministic:
                transforming a given source must always create the same result. For example,
                the current date and time must not be recorded and output always has to be
                written in the same order.
            </p>
            <p>
                Second, the set of tools used to perform the build and more generally the
                <strong>build environment</strong> should either be recorded or pre-defined.
            </p>
            <p>
                Third, users should be given a way to recreate a close enough build
                environment, perform the build process, and <strong>validate</strong> that the output matches
                the original build.
            </p>
            <a href="{{ '/docs' | relative_url }}">Learn more about how to make your software build reproducibly…</a>
        </div>
        <!-- Sidebar: Recent Reports and News -->
        <div class="col-md-4">
            <div class="p-4 bg-light rounded">
                <h3 class="mb-3">Recent Monthly Reports</h3>
                <ul class="list-unstyled mb-4">
                    {% assign reports = site.reports | sort: 'year, month' | where: 'draft', 'false' | reverse %}
                    {% for x in reports limit: 3 %}
                    <li class="mb-2">
                        <span class="text-muted">{{ x.date | date: "%b %-d, %Y" }}</span>:
                        <a href="{{ x.url | relative_url }}">{{ x.title }}</a>
                    </li>
                    {% endfor %}
                </ul>
                <a href="{{ "/news/" | relative_url }}" class="btn btn-outline-primary btn-sm">See all reports</a>
            </div>
            <div class="p-4 bg-light rounded mt-4">
                <h3 class="mb-3">Recent News</h3>
                <ul class="list-unstyled mb-4">
                    {% assign posts = site.posts | where: 'draft', 'false' %}
                    {% for x in posts limit: 3 %}
                    <li class="mb-2">
                        <span class="text-muted">{{ x.date | date: "%b %-d, %Y" }}</span>:
                        <a href="{{ x.url | relative_url }}">{{ x.title }}</a>
                    </li>
                    {% endfor %}
                </ul>
                <a href="{{ "/news/" | relative_url }}" class="btn btn-outline-primary btn-sm">See all news</a>
            </div>
        </div>
    </div>
</div>

{% assign sponsors = site.data.sponsors.platinum | sort: 'name' %}
{% if sponsors.size != 0 %}

## Sponsors

We are proud to be [sponsored by]({{ "/sponsors/" | relative_url }}):

<div class="row bg-light p-md-4 p-sm-2 pt-5 pb-5">
    {% for x in sponsors %}
    <div class="col-xs-12 col-sm-6 mb-6 mx-auto">
        <div class="card h-100 text-center justify-content-center">
            <a href="{{ x.url }}" name="{{ x.name }}">
                <img class="p-5 w-100 sponsor-img-platinum" src="{{ x.logo | prepend: "/assets/images/sponsors/" | relative_url }}" alt="{{ x.name }}">
            </a>
        </div>
    </div>
    {% endfor %}
</div>
{% else %}
{% endif %}
