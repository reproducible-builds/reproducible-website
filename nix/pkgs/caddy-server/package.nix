{
  lib,
  writeShellApplication,
  substituteAll,
  caddy,
  reproducible-website,
  ...
}:
let
  caddyFile = substituteAll {
    src = ./Caddyfile;
    webroot = reproducible-website;
  };
in
writeShellApplication {
  name = "caddy-server";
  runtimeInputs = [ caddy ];
  text = ''
    ${lib.getExe caddy} run --adapter caddyfile --config ${caddyFile}
  '';
}
