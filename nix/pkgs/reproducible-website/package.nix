{
  stdenvNoCC,
  lib,
  gitMinimal,
  runtimeShell,
  python3,
  perl536Packages,
  which,
  ruby,
  jekyll,
}:

let
  jekyll' = jekyll.override ({ withOptionalDependencies = true; });
  ruby' = (ruby.withPackages (ps: [ jekyll' ]));
  python3' = python3.withPackages (p: [ p.pyyaml ]);
in
stdenvNoCC.mkDerivation {
  name = "reproducible-website";
  src = ./../../../.;

  nativeBuildInputs = [
    gitMinimal
    perl536Packages.Po4a
    which
    python3'
    ruby'
  ];

  # This step should not be mandatory, the script contributors.py
  # should not get the contributors from git.
  # Without this preConfigure phase, the script will fail.
  preConfigure = ''
    git init
    git config user.email "you@example.com"
    git config user.name "Nix build"
    git commit -am "empty" --allow-empty
  '';

  preBuild = ''
    patchShebangs ./bin
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p $out
    cp -ar _site/* $out/

    runHook postInstall
  '';
}
