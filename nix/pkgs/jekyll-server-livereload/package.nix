{
  lib,
  writeShellApplication,
  jekyll-server,
  ...
}:
writeShellApplication {
  name = "jekyll-server-livereload";
  runtimeInputs = [ jekyll-server ];
  text = ''
    ${lib.getExe jekyll-server} --watch --livereload --incremental "$@"
  '';
}
