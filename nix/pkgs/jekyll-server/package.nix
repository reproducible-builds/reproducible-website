{
  lib,
  writeShellApplication,
  jekyll,
  ...
}:
let
  jekyll' = jekyll.override { withOptionalDependencies = true; };
in
writeShellApplication {
  name = "jekyll-server";
  runtimeInputs = [ jekyll' ];
  text = ''
    ${lib.getExe jekyll'} serve --host "$@"
  '';
}
