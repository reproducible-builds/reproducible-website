{ ... }:
{
  perSystem =
    { config, ... }:
    {
      apps = {
        # Runs a Caddy server to serve the static site
        # This is the default app, ready for production
        # This is not meant to be used during development phase
        default = {
          type = "app";
          program = config.packages.caddy-server;
        };

        # Runs the Jekyll server only
        # For development phase
        jekyll-serve = {
          type = "app";
          program = config.packages.jekyll-server;
        };

        # Runs the Jekyll server with live reload
        # For development phase
        jekyll-serve-livereload = {
          type = "app";
          program = config.packages.jekyll-server-livereload;
        };
      };
    };
}
