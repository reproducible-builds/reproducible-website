{ ... }:
{
  perSystem =
    { config, pkgs, ... }:
    {
      devShells = {
        # A default shell with all the necessary tools to work on the website
        # Use `nix develop` to use it
        default = pkgs.mkShell {
          inputsFrom = [ config.packages.reproducible-website ];

          packages = [
            pkgs.pandoc
            pkgs.gnumake
            pkgs.perl536Packages.Po4a
            pkgs.which
          ];
        };
      };
    };
}
