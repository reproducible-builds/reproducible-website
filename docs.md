---
layout: default
title: Docs
permalink: /docs/
order: 25
redirect_from: /docs/.
---

# Documentation

Getting reproducible builds for your software or distribution might be easier than you think.

However, it might require small changes to your build system and a strategy on
how to enable others to recreate an environment in which the builds can be
reproduced.


{% for section in site.data.docs %}
  <h2>{{ section.title }}</h2>

  <ul class="list-unstyled">
  {% for item in section.docs %}
    {% assign item_url = item | prepend: "/docs/" | append:"/" %}
    {% for p in site.docs %}
      {% if p.url == item_url %}
      <li><a href="{{ p.url | relative_url }}">{{ p.title }}</a></li>
      {% endif %}
    {% endfor %}
  {% endfor %}
  </ul>
{% endfor %}

<h2>Specifications</h2>

<ul class="list-unstyled">
  <li>
    <a href="{{ "/specs/source-date-epoch/" | absolute_url }}"><tt>SOURCE_DATE_EPOCH</tt></a>
  </li>
  <li>
    <a href="{{ "/specs/build-path-prefix-map/" | absolute_url }}"><tt>BUILD_PATH_PREFIX_MAP</tt> (WIP)</a>
  </li>
</ul>
